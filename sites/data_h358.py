"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

"""
from __future__ import annotations
from buildingenergy.solar import SolarModel, SolarSystem, RectangularMask, InvertedMask
from buildingenergy.parameters import ParameterSet
from buildingenergy.data import VariableSet, ParameterizedVariableSet, DataProvider, Bindings
from buildingenergy.control import SignalGenerator


class H358ParameterSet(ParameterSet):

    def __init__(self):
        super().__init__()
        self('body_metabolism', 100, (50, 150, 10))
        self('office:volume', 56, (30, 80, 5))
        self('office:heater_power_per_delta_surface_temperature', 50, (10, 100, 5))
        self('office-outdoor:Q_0', 15/3600, (0/3600, 2000/3600, 10/3600))
        self('office-outdoor:Q_window', 50/3600, (0/3600, 2000/3600, 10/3600))
        self('office-outdoor:Q_door', 0/3600, (0/3600, 2000/3600, 10/3600))
        self('office-corridor:Q_0', 15/3600, (0/3600, 200/3600, 10/3600))
        self('office-corridor:Q_window', 0/3600, (0/3600, 2000/3600, 10/3600))
        self('office-corridor:Q_door', 50/3600, (0/3600, 2000/3600, 10/3600))
        self('office-downstairs:slab_surface_correction', 1, (1, 3, .1))
        self('office-outdoor:psi_bridge', 0.5 * 0.99, (0.0 * 0.99, 0.5 * 5, 0.1))
        self('office-outdoor:foam_thickness', 34e-3, (10e-3, 50e-3, 10e-3))
        self('CO2_occupant_breath_production', 2, (0, 25, 1))
        self('office-outdoor:solar_factor', 0.4, (.1, .85, .1))
        self('downstairs:Temperature', 20)
        self('outdoor:CCO2', 400, (250, 650, 50))
        self('office-outdoor:Rfactor', 1, (.5, 2, .1))
        self('office-corridor:Rfactor', 1, (.5, 2, .1))
        self('office-downstairs:Rfactor', 1, (.5, 2, .1))
        self('office-outdoor:Cfactor', 1, (.1, 10, .1))
        self('office-corridor:Cfactor', 1, (.1, 10, .1))
        self('office-downstairs:Cfactor', 1, (.1, 10, .1))
        self('office:Vfactor', 1, (.5, 1.5, .1))


class H358Data(VariableSet):

    def __init__(self, starting_stringdate: str = None, ending_stringdate: str = None, number_of_levels: int = 2, verbose: bool = False):  # type: ignore
        super().__init__(csv_measurement_filename='h358data_2015-2016.csv', json_openweather_filename='Grenoble-INP1990.json', starting_stringdate=starting_stringdate, ending_stringdate=ending_stringdate, albedo=.1, pollution=0.1, deleted_variables=('Tyanis', 'zetaW7', 'zetaW9', 'wind_speed', 'wind_direction_in_deg', 'feels_like', 'occupancy', 'humidity', 'pressure', 'temp_min', 'temp_max', 'description', 'power_heater', 'temperature_2m', 'relativehumidity_2m', 'dewpoint_2m', 'apparent_temperature', 'weathercode', 'pressure_msl', 'surface_pressure', 'cloudcover', 'cloudcover_low', 'cloudcover_mid', 'cloudcover_high', 'et0_fao_evapotranspiration', 'vapor_pressure_deficit', 'windspeed_10m', 'windspeed_100m', 'winddirection_10m', 'winddirection_100m', 'windgusts_10m', 'soil_temperature_0_to_7cm', 'soil_temperature_7_to_28cm', 'soil_temperature_28_to_100cm', 'soil_temperature_100_to_255cm', 'soil_moisture_0_to_7cm', 'soil_moisture_7_to_28cm', 'soil_moisture_28_to_100cm', 'soil_moisture_100_to_255cm', 'is_day', 'shortwave_radiation', 'direct_radiation', 'diffuse_radiation', 'direct_normal_irradiance'), number_of_levels=number_of_levels, verbose=verbose)  # noqa  'precipitation', 'rain', 'snowfall',

        # creation of the solar mask
        window_mask = InvertedMask(RectangularMask((-86, 60), (20, 68)))
        solar_model = SolarModel(self.weather_data)
        solar_system = SolarSystem(solar_model)
        solar_system.add_collector('main', surface=2, exposure_in_deg=-13, slope_in_deg=90, solar_factor=0.85, collector_mask=window_mask)
        solar_gains_with_mask, _ = solar_system.solar_gains_in_Wh()
        self.add_external_variable('Psun_window', solar_gains_with_mask)

        # build invariant variables
        detected_motions: list[int] = [int(d > 1) for d in self('detected_motions')]
        power_stephane: list[float] = self('power_stephane')
        power_khadija: list[float] = self('power_khadija')
        power_audrey: list[float] = self('power_audrey')
        power_stagiaire: list[float] = self('power_stagiaire')

        occupancy: list[int] = [max(detected_motions[k], int(power_stephane[k] > 17) + int(power_khadija[k] > 17) + int(power_stagiaire[k] > 17) + int(power_audrey[k] > 17)) for k in range(len(self))]
        presence: list[int] = [int(occupancy[k] > 0) for k in range(len(self))]
        self.add_external_variable('occupancy', occupancy)
        self.add_external_variable('presence', presence)

        # create a series of setpoint values (useful or not, depending on a control temperature is declared or not)
        setpoint_generator = SignalGenerator(base_value=21, datetimes=self('datetime'))
        setpoint_generator.heater_on((15, 3), (15, 10))
        setpoint_generator.daily([0, 1, 2, 3, 4], 17, [0, 6, 12, 14, 18])
        setpoint_generator.daily([0, 1, 2, 3, 4], 13, [0, 5, 22])
        setpoint_generator.daily([5, 6], 13, [0, 24])
        setpoint_generator.long_absence(5, self('presence'))
        setpoint_generator.capping(5, .7, self('window_opening'))
        self.add_external_variable('Tsetpoint', setpoint_generator())


class H358ParameterizedDataSet(ParameterizedVariableSet):

    def __init__(self, dp: DataProvider):
        super().__init__(dp)
        self.add('Pmetabolism', lambda k: dp('body_metabolism') * dp('occupancy', k), 'occupancy', 'body_metabolism')
        self.add('Pheater', lambda k: dp('office:heater_power_per_delta_surface_temperature') * (dp('dT_heat', k) if dp('dT_heat', k) > 1 else 0), 'office:heater_power_per_delta_surface_temperature', 'dT_heat')
        self.add('Pheat_total', lambda k: dp('office:heater_power_per_delta_surface_temperature') * dp('dT_heat', k) + dp('total_electric_power') + dp('occupancy', k) * dp('body_metabolism') + dp('office-outdoor:solar_factor') * dp('Psun_window', k), 'office:heater_power_per_delta_surface_temperature', 'dT_heat', 'total_electric_power', 'occupancy', 'body_metabolism', 'office-outdoor:solar_factor', 'Psun_window')
        self.add('CO2production', lambda k: dp('CO2_occupant_breath_production') * dp('occupancy', k), 'CO2_occupant_breath_production', 'occupancy')
        self.add('Q(office,outdoor)', lambda k: dp('office-outdoor:Q_0') + dp('office-outdoor:Q_window') * dp('window_opening', k) + dp('office-outdoor:Q_door') * dp('door_opening', k), 'Qoutdoor0', 'Qoutdoor_window', 'window_opening', 'office-outdoor:Q_door', 'door_opening')
        self.add('Q(corridor,office)', lambda k: dp('office-corridor:Q_0') + dp('office-corridor:Q_window') * dp('window_opening', k) + dp('office-corridor:Q_door') * dp('door_opening', k), 'office-corridor:Q_0', 'Qoutdoor_window', 'office-corridor:Q_window', 'office-corridor:Q_door', 'door_opening')


h358_bindings = Bindings()
h358_bindings.add_synonyms('TZcorridor', 'Tcorridor')
h358_bindings.add_synonyms('TZdownstairs', 'downstairs:Temperature')
h358_bindings.add_synonyms('TZoutdoor', 'weather_temperature')
h358_bindings.add_synonyms('PZoffice', 'Pheat_total')
h358_bindings.add_synonyms('CCO2_corridor', 'corridor_CO2_concentration')
h358_bindings.add_synonyms('PCO2_office', 'CO2production')
h358_bindings.add_synonyms('CCO2_outdoor', 'outdoor:CCO2')

param = H358ParameterSet()
h358_data_provider = DataProvider(param, H358Data(starting_stringdate='15/02/2015', ending_stringdate='15/02/2016', number_of_levels=3, verbose=True), h358_bindings)
H358ParameterizedDataSet(h358_data_provider)

if __name__ == '__main__':
    h358_data_provider.plot()
    print(h358_data_provider('datetime'))
    print(h358_data_provider)
    print(h358_data_provider('Tcorridor', 3))
    print(h358_data_provider('TZcorridor', 3))
    print(h358_data_provider('TZcorridor'))
    print(h358_data_provider('Pmetabolism', 3))
    print(h358_data_provider('Pmetabolism'))
    print(h358_data_provider('Q(corridor,office)', 30))
    print(h358_data_provider.fingerprint(3))
    print(h358_data_provider.fingerprint(32))
    param('Qcorridor0', 15/3600)
    print(h358_data_provider.fingerprint(32))
    param('Rfactor_office_outdoor', .5)
    print(h358_data_provider.fingerprint(32))
