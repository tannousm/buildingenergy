"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
import sites.data_h358
import buildingenergy.linreg


training_data = sites.data_h358.H358Data(None, None)

######## tuning parameter zone #######
offset: bool = True
minimum_input_delay: int = 0
input_names: tuple[str] = ('weather_temperature', 'window_opening', 'Tcorridor', 'door_opening', 'total_electric_power', 'Psun_window', 'dT_heat', 'occupancy')
inputs_maximum_delays: tuple[int] = 10  # (2, 0, 0, 0, 2, 1, 1)
output_name: str = 'Toffice_reference'
ouput_maximum_delay: int = 10
######################################

buildingenergy.linreg.sliding_arx_estimation(output_name,input_names, training_data, offset=offset, minimum_input_delay=minimum_input_delay, inputs_maximum_delays=inputs_maximum_delays, ouput_maximum_delay=ouput_maximum_delay)

print('Results have been saved as a markdown document in the "results/sliding" folder')