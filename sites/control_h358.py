"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
import sites.data_h358
import sites.model_h358
from buildingenergy.control import Controller, SignalGenerator, ControlRunner


class H358Controller(Controller):
    
    def __init__(self, datetimes, **data) -> None:
        super().___init__(datetimes, **data)
        # self.create_control('window_opening')
        # self.create_control('door_opening')
        # self.create_control('temperature_setpoint', 'heating_power', (0, 2000))
        
    def generate_timed_setpoints(self) -> dict[str, list[float]]:
        datetimes = h358_data('datetime')
        timed_setpoints: dict[str, list[float]] = dict()
        summer_period_start: tuple[int, int] = (15, 4)
        summer_period_stop: tuple[int, int] = (15, 10)
        
        long_absence_signal: SignalGenerator = SignalGenerator(1, datetimes)
        long_absence_signal.long_absence(1, h358_data('presence'))
        timed_setpoints['long_absence'] = long_absence_signal()
        
        heater_on_signal: SignalGenerator= SignalGenerator(1, datetimes)
        heater_on_signal.heater_on(summer_period_start, summer_period_stop)
        timed_setpoints['heater_on'] = heater_on_signal()
        
        temperature_setpoint_signal: SignalGenerator= SignalGenerator(21, datetimes)
        temperature_setpoint_signal.heater_on(summer_period_start, summer_period_stop)
        temperature_setpoint_signal.daily((0,1,2,3,4,5,6), 15, [0, 5])
        temperature_setpoint_signal.daily((0,1,2,3,4), 19, [9, 17])
        timed_setpoints['temperature_setpoint'] = temperature_setpoint_signal()
        
        day_time_signal: SignalGenerator= SignalGenerator(1, datetimes)
        day_time_signal.daily((0,1,2,3,4,5,6), 0, [0, 5])
        timed_setpoints['day_time'] = day_time_signal()
        
        window_opening_setpoints: list[float] = [1000*(1 - timed_setpoints['long_absence'][i]) for i in range(len(h358_data))]
        timed_setpoints['window_opening'] = window_opening_setpoints
        return timed_setpoints
        
    def control_value_domains(self, k: int, context_data: dict[str, float]) -> dict[str,list[float]]:
        value_domains: dict[str,list[float]] = dict()
        
        opening_context: bool = context_data['presence'] == 0 
        match opening_context:
            case True:  # absence
                value_domains['window_opening'] = [0]
                value_domains['door_opening'] = [0]
            case False:  # presence
                value_domains['window_opening'] = [0, 1]
                value_domains['door_opening'] = [0, 1]
            
        heater_context: tuple[bool] = context_data['heater_on'] > 0, context_data['long_absence'] > 0, context_data['day_time']>0
        match heater_context:
            case [False, long_absence, daytime]:  # summer
                pass
            case [True, True, daytime]:  # heater_on and long_absence
                value_domains['temperature_setpoint'] = [5]
            case [True, False, False]:  # heater_on and night
                value_domains['temperature_setpoint'] = [16]
            case [True, False, True]:  # heater_on and day
                value_domains['temperature_setpoint'] = [15, 16, 18, 19, 20, 21, 22, 23, 26]
        return value_domains

    def control_algorithm(self, k: int, value_domains: dict[str, list[float]], context_data: dict[str, float], dependent_control_port_names: dict[str, str]) -> dict[str, float]:
        context: tuple[float] = (context_data['heater_on'], context_data['long_absence'], context_data['presence'], context_data['CCO2_office'], context_data['TZoffice_temperature'], context_data['outdoor_temperature'])
        
        match context:
            case [heater_on, 1, presence, indoor_CO2, indoor_temperature, outdoor_temperature]:
                controls: dict[str: float] = {'window_opening': 0, 'door_opening': 0}
            case [heater_on, 0, 1, indoor_CO2, indoor_temperature, outdoor_temperature] if indoor_CO2 > 4000:
                controls: dict[str: float] = {'window_opening': 1, 'door_opening': 1}
            case [1, 0, 1, indoor_CO2, indoor_temperature, outdoor_temperature] if indoor_CO2 > 1500 and indoor_temperature >= 19 and outdoor_temperature >= indoor_temperature - 5:
                controls: dict[str: float] = {'window_opening': 1, 'door_opening': 1}
            case [1, 0, 1, indoor_CO2, indoor_temperature, outdoor_temperature] if indoor_CO2 > 1500 and indoor_temperature >= 19 and outdoor_temperature < indoor_temperature - 5:
                controls: dict[str: float] = {'window_opening': 0, 'door_opening': 1}
            case _ :
                controls: dict[str: float] = {'window_opening': 0, 'door_opening': 0}
        
        if context_data['heater_on']:
            controls['temperature_setpoint'] = context_data['temperature_setpoint']
        else: 
            controls['temperature_setpoint'] = None
        return controls
        
    
if __name__ == '__main__':
    h358_data: sites.data_h358.H358Data = sites.data_h358.H358Data('15/02/2015', '15/02/2016', number_of_levels=3)
    parameters: sites.model_h358.H358ParameterSet = sites.model_h358.H358ParameterSet()
    parameters('heater_power_per_delta_surface_temperature', 0)
    parameterized_data: sites.data_h358.H358ParameterizedDataSet = sites.data_h358.H358ParameterizedDataSet(parameters)
    h358_data.register_parameterized_dataset(parameterized_data)
    model: sites.H358Model = sites.model_h358.H358Model(order=4, parameters=parameters, data=h358_data, parameterized_data=parameterized_data, model_data_bindings=sites.model_h358.model_data_bindings())

    h358_controller: H358Controller = H358Controller(h358_data)
    print(h358_controller)
    print(h358_controller.typed_controls)
    control_runner: ControlRunner = ControlRunner(model, h358_controller)
    
    regular_outputs_values, regular_inputs_values, airflows_values, variables_in_fingerprint_values = control_runner.run()
    for variable_name in regular_outputs_values:
        h358_data.add_external_variable(variable_name, regular_outputs_values[variable_name])
    for variable_name in regular_inputs_values:
        h358_data.add_external_variable(variable_name, regular_inputs_values[variable_name])
    for variable_name in airflows_values:
        h358_data.add_external_variable(variable_name, airflows_values[variable_name])
    for variable_name in variables_in_fingerprint_values:
        h358_data.add_external_variable(variable_name, variables_in_fingerprint_values[variable_name])
        
    h358_data.plot()
    
    #print('->', h358_controller.controls(30))
    
    # for k in range(len(data)):
    #     direct_controls, indirect_controls = h358_controller.controls(k)  #, temperature_setpoint=16)
    #     print(str(k), ' > ', direct_controls, ', ', indirect_controls)
    #     if k % 100 == 0:
    #         input('Enter to continue >')
    
# class H358runner(ModelHandler):

#     def __init__(self) -> None:
#         super().__init__((('TZcorridor', 'Tcorridor'), ('TZdownstairs', 'Tdownstairs'), ('TZoutdoor', 'weather_temperature'), ('PZoffice', [
#             'total_electric_power', 'Psun_window', 'Pmetabolism', 'Pheater']), ('CCO2_corridor', 'corridor_CO2_concentration'), ('CCO2_outdoor', 'outdoorCCO2'), ('PCO2_office', 'CO2production'), ('Qoutdoor', 'Qoutdoor'), ('Qcorridor', 'Qcorridor'), ('TZoffice', 'Toffice_reference'), ('CCO2_office', 'office_CO2_concentration'), ('CCO2_corridor', 'corridor_CO2_concentration')))
        
#     def make_parameter_set(self, data) -> ParameterSet:
#         parameter_set = ParameterSet(resolution=10)
        
#         parameter_set = ParameterSet(resolution=10)
#         parameter_set('body_metabolism', 100, 50, 200)
#         parameter_set('heater_power_per_delta_surface_temperature', 50, 10, 200)
#         parameter_set('Qoutdoor0', 50/3600, 0/3600, 200/3600)
#         parameter_set('Qoutdoor_window', 50/3600, 2/3600, 2000/3600)
#         parameter_set('Qoutdoor_door', 50/3600, 2/3600, 2000/3600)
#         parameter_set('Qcorridor0', 50/3600, 0/3600, 200/3600)
#         parameter_set('Qcorridor_window', 50/3600, 2/3600, 2000/3600)
#         parameter_set('Qcorridor_door', 50/3600, 2/3600, 2000/3600)
#         parameter_set('slab_effective_thickness', 12e-2, 3e-2, 20e-2)
#         parameter_set('room_volume', 52, 45, 70)
#         parameter_set('psi_bridge', 0.5 * 0.99, 0.0 * 0.99, 0.5 * 5)
#         parameter_set('foam_thickness', 34e-3, 10e-3, 50e-3)
#         parameter_set('CO2_occupant_breath_production', 13, 7, 25)
#         parameter_set('solar_factor', 0.4, .1, .85)
#         parameter_set('Pmetabolism', model_h358.Pmetabolim(data, parameter_set), 0, 1000)
#         parameter_set('Pheater', model_h358.Pheater(data, parameter_set), 0, 2000)
#         parameter_set('Pheat', model_h358.Pheat(data, parameter_set), 0, 5000)
#         parameter_set('CO2production', model_h358.CO2production(data, parameter_set), 0, 50)
#         parameter_set('Qoutdoor', model_h358.Qoutdoor(data, parameter_set), 2/3600, 2000/3600)
#         parameter_set('Qcorridor', model_h358.Qcorridor(data, parameter_set), 2/3600, 2000/3600)
#         return parameter_set

#     def make_data(self) -> DataSet:
#         return data_h358.H358Data('15/02/2015', '15/02/2016') # '16/02/2015', '23/02/2015'

#     # def make_parameterized_data(self, data: Data, parameter_set: ParameterSet):
#     #     Pmetabolim(data, parameter_set)

#     def make_model(self, parameter_set: ParameterSet) -> model_h358.Model:
#         return model_h358.Model(order=4, parameters=parameter_set)
        
        
# def main():
#     runner = H358runner()
#     start_time = time.time()
#     runner.simulate()
    
#     #runner.memorize('_sim', inputs=False)
#     #runner.fit(learning_ratio=2/3., suffix='*', maxiter=30, popsize=20, tol=1e-3, workers=0)
#     #runner.simulate()
#     runner.memorize('_sim', inputs=False)
#     # runner.sensitivity(number_of_trajectories=10, number_of_levels=4)
#     duration = (time.time() - start_time) 
#     print('Operation time: %f seconds' % duration)
#     runner.plot()

    # print('- Loading data')
    # start = time.time()
    # data: sites.data_h358.H358Data = sites.data_h358.H358Data('15/02/2015', '15/02/2016', number_of_levels=3)
    # print(data)
    # parameters: sites.model_h358H358ParameterSet = sites.model_h358.H358ParameterSet()
    # parameters('heater_power_per_delta_surface_temperature', 0)
    # parameterized_data: sites.data_h358.H358ParameterizedDataSet = sites.data_h358.H358ParameterizedDataSet(parameters)
    # data.register_parameterized_dataset(parameterized_data)
    # print('%i secs' % (time.time() - start))
    
    # print('- Model generation')
    # start = time.time()
    # model: sites.H358Model = sites.model_h358.H358Model(order=4, parameters=parameters, data=data, parameterized_data=parameterized_data, model_data_bindings=sites.model_h358.h358_model_data_bindings())
    # print('%i secs' % (time.time() - start))
    # print(model)


    # controller: Controller = sites.control_h358.H358Controller(data('datetime'))
    # controller.add_input_control('window_opening', controller.opening_control_options)
    # controller.add_input_control('door_opening', controller.opening_control_options)
    
    # # controller.add_step_output_control('TZoffice', controller.temperature_control_options, 'PZoffice', controller.power_control_options, 21)
    
    # runner: ControlRunner = ControlRunner(model, controller)
    
    # print('\n______ Simulation ______')
    # simulation_start = time.time()
    # runner.run()
    # print('%i secs' % (time.time() - start))
    # runner.plot()

# if __name__ == '__main__':
#     main()

#     # yappi -f pstat -o profiling.out h358runner.py
#     # snakeviz profiling.out
