"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
import matplotlib.pylab
import buildingenergy.openweather
import buildingenergy.solar

site_weather_data = buildingenergy.openweather.WeatherJsonReader('engins.json', from_stringdate = '01/01/2019', to_stringdate = '31/12/2019',  albedo=.1, location='Engins', latitude=45.181909, longitude=5.619965).site_weather_data
print('weather variables:', site_weather_data.variable_names)  
solar_model = buildingenergy.solar.SolarModel(site_weather_data=site_weather_data)
solar_model.plot_heliodor(2015, 'heliodon')
solar_model.plot_solar_cardinal_irradiations()
matplotlib.pylab.figure()
phis1 = solar_model.irradiances(slope_deg=90+35, exposure_deg=0)
print('energy PV:', sum(phis1['total'])*150*.13/1000,'kWh')
phis2 = solar_model.irradiances(slope_deg=90+35, exposure_deg=-20)
print('energy PV:', sum(phis2['total'])*150*.13/1000, 'kWh')
matplotlib.pylab.plot(solar_model.site_weather_data.get('datetime'), phis2['total'])
matplotlib.pylab.plot(solar_model.site_weather_data.get('datetime'), phis1['total'])
matplotlib.pylab.legend(('0','-20'))
matplotlib.pyplot.figure()
matplotlib.pylab.plot(solar_model.site_weather_data.get('datetime'), solar_model.site_weather_data.get('temperature'))
solar_model.plot_angles()
solar_model.try_export()
matplotlib.pylab.show()
