from __future__ import annotations
from math import pi
from buildingenergy.thermal import ThermalNetwork, CAUSALITY
import buildingenergy.openweather
import buildingenergy.solar
import matplotlib.pyplot as plt
import buildingenergy.thermal
import buildingenergy.physics
import buildingenergy.parameters
import buildingenergy.building


#on prend les matriaux dont on a besoin
library: buildingenergy.physics.Library = buildingenergy.physics.Library()
library.store('wood', 'thermal', 277)
library.store('air', 'thermal', 259)
library.store('insulation', 'thermal', 278)
library.store('foam','thermal',260)

buildingenergy.building.Building.library = library


###on ecrit les interfaces entre les espaces 

#e1 = espace entre le mur exterieur gauche de la ruche et le premier cadre
height = 34e-2
depth = 50e-2
width = 42.8e-2
external_wood_thickness = 2.4e-2
internal_wall_thickness = 2.1e-2
external_insulation_foam = 1e-2
external_insulation_wood = 2e-2
number_of_rooms = 11
insulation = False


weather_file_name: str = 'grenoble1979-2022.json'
weather_year: int = 2021
altitude: float = 212
albedo = 0.1
location: str = 'Grenoble'
open_weather_map_json_reader = buildingenergy.openweather.WeatherJsonReader(weather_file_name, from_stringdate='1/01/%i' % weather_year, to_stringdate='1/02/%i' % (weather_year),  albedo=albedo, pollution=0.1, location=location)
site_weather_data = open_weather_map_json_reader.site_weather_data
##solar_model = buildingenergy.solar.SolarModel(site_weather_data) pour avoir le rayonnement solaire
Touts = site_weather_data.get('temperature')

T_queen = 35
swarm_location = 4
swarm_radius = 10e-2
swarm_equivalent_foam_thickness = 5e-2

###############################
def beehive_compute(insulation):
    external_insulation = external_insulation_foam + external_insulation_wood
    effective_height = height-2*(external_wood_thickness+external_insulation)
    effective_depth = depth-2*(external_wood_thickness+external_insulation)
    effective_room_width = (width-2*(external_wood_thickness+external_insulation)-(number_of_rooms-1)*internal_wall_thickness)/(number_of_rooms-1)

    surface_side = effective_height * effective_depth
    surface_room_front = effective_room_width * effective_height
    surface_room_top = effective_room_width * effective_depth

    beehive = buildingenergy.building.Building(*['e%i'%(i) for i in range(number_of_rooms)], 'queen') 

    # left room
    left_room_vertical = beehive.side('outdoor', 'e0', buildingenergy.building.SideType.WALL,surface_side+2*surface_room_front)
    left_room_vertical.add_layer('wood', external_wood_thickness)
    if insulation:
        left_room_vertical.add_layer('foam', external_insulation_foam)
        left_room_vertical.add_layer('wood', external_insulation_wood)

    left_room_horizontal = beehive.side('outdoor', 'e0', buildingenergy.building.SideType.ROOF,2*surface_room_top)
    left_room_horizontal.add_layer('wood', external_wood_thickness)
    if insulation:
        left_room_horizontal.add_layer('foam', external_insulation_foam)
        left_room_horizontal.add_layer('wood', external_insulation_wood)

    # right room
    right_room_vertical = beehive.side('outdoor', 'e%i'%(number_of_rooms-1), buildingenergy.building.SideType.WALL,surface_side+2*surface_room_front)
    right_room_vertical.add_layer('wood', external_wood_thickness)
    if insulation:
        right_room_vertical.add_layer('foam', external_insulation_foam)
        right_room_vertical.add_layer('wood', external_insulation_wood)

    right_room_horizontal = beehive.side('outdoor', 'e%i'%(number_of_rooms-1), buildingenergy.building.SideType.ROOF,2*surface_room_top)
    right_room_horizontal.add_layer('wood', external_wood_thickness)
    if insulation:
        right_room_horizontal.add_layer('foam', external_insulation_foam)
        right_room_horizontal.add_layer('wood', external_insulation_wood)

    # internal rooms
    for i in range(number_of_rooms-1):
        internal_room_vertical = beehive.side('outdoor', 'e%i'%i, buildingenergy.building.SideType.WALL,2*surface_room_front)
        internal_room_vertical.add_layer('wood', external_wood_thickness)
        if insulation:
            internal_room_vertical.add_layer('foam', external_insulation_foam)
            internal_room_vertical.add_layer('wood', external_insulation_wood)

        internal_room_horizontal = beehive.side('outdoor', 'e%i'%i, buildingenergy.building.SideType.ROOF,2*surface_room_top)
        internal_room_horizontal.add_layer('wood', external_wood_thickness)
        if insulation:
            internal_room_horizontal.add_layer('foam', external_insulation_foam)
            internal_room_horizontal.add_layer('wood', external_insulation_wood)

    for i in range(number_of_rooms-1):
        internal_wall_room = beehive.side('e%i'%i, 'e%i'%(i+1), buildingenergy.building.SideType.WALL,surface_side)
        internal_wall_room.add_layer('wood', internal_wall_thickness)
        
    swarm = beehive.side('e%i'%swarm_location, 'queen', buildingenergy.building.SideType.WALL, 4*pi*(swarm_radius)**2)
    swarm.add_layer('foam', swarm_equivalent_foam_thickness)  # this is an equivalent thickness
    
    print(beehive)
    
    net = ThermalNetwork()
    net.T('Tout', CAUSALITY.IN) #je rentre les temperatures connues 
    net.T('Tqueen', CAUSALITY.OUT)
    
    for i in range(number_of_rooms):
        net.T('T%i'%i, causality=CAUSALITY.OUT)
        net.R(fromT='Tout', toT='T%i'%i, name='Rout%i'%i, value=beehive.get_interface_thermal_resistance('e%i'%i, 'outdoor'))

    for i in range(number_of_rooms-1):
        net.R(fromT='T%i'%i, toT='T%i'%(i+1), name='R%i_%i'%(i,i+1), value=beehive.get_interface_thermal_resistance('e%i'%i, 'e%i'%(i+1)))
        
    net.R(fromT='T%i'%swarm_location, toT='Tqueen', name='Rswarm', value=beehive.get_interface_thermal_resistance('e%i'%swarm_location, 'queen'))

    
    net.HEAT(T='Tqueen', name='Pessain') 
    
    state_model = net.state_model()
    print(state_model)

    D = state_model.D

    
    P_swarm = list()
    T_queen_verif = list()
    Ti = {'T%i'%i:[] for i in range(number_of_rooms)}
    for Tout in Touts:
        P_swarm.append((T_queen - D[0,0]*Tout) / D[0,1])
        Y = D[:,0] * Tout + D[:,1] * P_swarm[-1]
        T_queen_verif.append(Y[0,0])
        for i in range(number_of_rooms):
            Ti['T%i'%i].append(Y[i+1,0])
    return P_swarm, Ti, T_queen_verif

P_swarm_base, Ti_base, T_queen_base = beehive_compute(False) 
P_swarm_insulation, Ti_insulation, T_queen_insulation = beehive_compute(True)

plt.subplot(311)
plt.plot(site_weather_data.get('datetime'), Touts, label='Tout')
plt.plot(site_weather_data.get('datetime'), T_queen_base, label='Tqueen')
for i in range(number_of_rooms):
    plt.plot(site_weather_data.get('datetime'), Ti_base['T%i'%i], label='T%i'%i)
plt.legend()
plt.ylabel('base')
plt.subplot(312)
plt.plot(site_weather_data.get('datetime'), Touts, label='Tout')
plt.plot(site_weather_data.get('datetime'), T_queen_insulation, label='Tqueen')
for i in range(number_of_rooms):
    plt.plot(site_weather_data.get('datetime'), Ti_insulation['T%i'%i], label='T%i'%i)
plt.legend()
plt.ylabel('insulation')
plt.subplot(313)
plt.plot(site_weather_data.get('datetime'), P_swarm_base, label='P_swarm_base')
plt.plot(site_weather_data.get('datetime'), P_swarm_insulation, label='P_swarm_insulation')
plt.legend()
plt.show()