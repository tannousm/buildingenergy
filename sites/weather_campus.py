"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
import time
import buildingenergy.lambdahouse

# Use the library to load a physical property related to a sheet name from the from the 'propertiesDB.xlsx' file, and a row number.
# parameter1: short name used to refer to a material or a component
# parameter2: sheet name in the xlsx file where the property is
# parameter3: row in the sheet of the file containing the property loaded for local usage
# For instance, to load the thermal properties of the material 'Expanded Polystyrene – IN31', sheet 'thermal', row 170

buildingenergy.physics.library.store('polystyrene', 'thermal', 170)  # the argument 1 is the local name that should be used in the wall compositions
buildingenergy.physics.library.store('straw', 'thermal', 261)  # the argument 1 is the local name that should be used in the wall compositions


class MyConfiguration(buildingenergy.lambdahouse.Configuration):

    def __init__(self, weather_file: str, weather_year: int, location: str = None, albedo: float = 0.1, pollution: float = 0.1, latitude: float = None, longitude: float = None, skyline: list[tuple[float, float]] = None) -> None:
        super().__init__(weather_file, weather_year, location, albedo, pollution, latitude, longitude, skyline)
        
        self.section('house')
        self(total_living_surface=25)
        self(height_per_floor=2.5)
        self(shape_factor=1, parametric=[.5, .75, 1, 1.25, 1.5, 1.75, 2])
        self(number_of_floors=1, parametric=[1,2])
        self(wall_composition_in_out=[('plaster', 13e-3), ('straw', 30e-2), ('concrete', 20e-2)])
        self(roof_composition_in_out=[('plaster', 30e-3), ('polystyrene', 9e-2), ('concrete', 13e-2)])
        self(glass_composition_in_out=[('glass', 4e-3), ('air', 6e-3), ('glass', 4e-3)])
        self(ground_composition_in_out=[('concrete', 13e-2), ('polystyrene', 30e-2), ('gravels', 20e-2)])
        self.insulation: str = 'polystyrene'  # be careful: no space in the material used for insulation
        self(polystyrene=20e-2, parametric=[0, 5e-2, 10e-2, 15e-2, 20e-2, 25e-2, 30e-2, 35e-2, 40e-2])
        self.section('windows')
        self(offset_exposure=0, parametric=[offset_exposure for offset_exposure in range(-45, 45, 5)])
        self(glazing={'north': 0.1, 'west': 0.1, 'east': 0.1, 'south': 0.1}, parametric=[0, .2, .4, .6, .8])
        self(solar_factor=0.4, parametric=[.1, .2, .3, .4, .5, .6, .7, .8, .9])
        self(south_solar_protection_angle=20, parametric=[10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60])
        
        self.section('HVAC and photovoltaic (PV) systems')
        self(heating_setpoint=21, parametric=[18, 19, 20, 22, 23])
        self(delta_temperature_absence_mode=3, parametric=[0, 1, 2, 3, 4])
        self(cooling_setpoint=26, parametric=[23, 24, 25, 27, 28, 29])
        self(winter_hvac_trigger_temperature=16)
        self(summer_hvac_trigger_temperature=24)
        self(hvac_hour_delay_for_trigger=24)
        self(inertia_level=15, parametric=[3, 6, 9, 12, 15, 18, 21, 24])
        self(hvac_COP=3)
        self(PV_surface=12, parametric=[1, 2, 4, 6, 9, 14, 18])
        self(final_to_primary_energy_coefficient=2.54)
        self(air_renewal_presence=1, parametric=[.5, 1, 2, 3])  # in vol/h
        self(air_renewal_absence=.1)
        self(ventilation_heat_recovery_efficiency=0.8, parametric=[0, .25, .5, .7, .9])
        self(PV_efficiency=0.20)
        
        self.section('inhabitants')
        self(occupancy_schema={(1, 2, 3, 4, 5,): {(18, 7): 0, (7, 18): 5}, (6, 7): {(0, 24): 2}})  # days of weeks (1=Monday,...), period (start. hour, end. hour) : avg occupancy
        self(average_occupancy_electric_gain=50)
        self(average_occupancy_metabolic_gain=100)
        self(average_permanent_electric_gain=200)
        self(air_renewal_overheat_threshold=26)
        self(air_renewal_overheat=5)

        
if __name__ == '__main__':
    # weather_file = 'Grenoble-INP1990'
    # weather_year = 2019
    # location = None
    # latitude, longitude = 45.194278717041016, 5.731629848480225
    
    weather_file = 'campus_transition'
    weather_year = 2022
    location = 'Forges'
    weather_year = 2019
    latitude, longitude = 48.419742, 2.962580

    configuration = MyConfiguration(weather_file=weather_file, weather_year=weather_year, location=location, latitude=latitude, longitude=longitude)
    tini = time.time()
    experiment = buildingenergy.lambdahouse.Experiment(configuration, on_screen=False)
    analysis = buildingenergy.lambdahouse.Analysis(experiment)  # 1
    analysis.climate(experiment)  # 2
    analysis.evolution(experiment)  # 3
    analysis.solar(experiment)  # 4
    analysis.house(experiment)  # 5
    analysis.neutrality(experiment)  # 6
    experiment.close()  #7
    print('duration: %i minutes' % (int(round((time.time() - tini) / 60))))
