"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
import buildingenergy.openweather
import matplotlib.pylab as plt
from math import floor, ceil, pi
from pandas.plotting import register_matplotlib_converters

import xarray as xr
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm

register_matplotlib_converters()
#site_weather_data = buildingenergy.openweather.OpenWeatherMapJsonReader('grenoble_weather2015-2019.json', '1/01/2019', '1/01/2020',location="Grenoble", albedo=.1).site_weather_data
site_weather_data = buildingenergy.openweather.WeatherJsonReader('refuge-des-bans.json', '1/01/2019', '1/01/2020', location="Refuge des Bans").site_weather_data
# site_weather_data = buildingenergy.openweather.OpenWeatherMapJsonReader('refuge-des-bans.json', from_stringdate = None, to_stringdate = None, location="Refuge des Bans", albedo=.1).site_weather_data
# solar_model = buildingenergy.solar.SolarModel(site_weather_data=site_weather_data)
# solar_model.try_export()
print(site_weather_data.from_stringdate, '>', site_weather_data.to_stringdate)
site_weather_data.day_degrees()
fig, ax = plt.subplots()
plt.plot(site_weather_data.get('stringdate'), site_weather_data.get('temperature'))
ax.set_title('temperature')
ax.axis('tight')
fig, ax = plt.subplots()
plt.plot(site_weather_data.get('stringdate'), site_weather_data.get('cloudiness'))
ax.set_title('cloudiness')
ax.axis('tight')
fig, ax = plt.subplots()
plt.plot(site_weather_data.get('stringdate'), site_weather_data.get('humidity'))
ax.set_title('humidity')
ax.axis('tight')
fig, ax = plt.subplots()
plt.plot(site_weather_data.get('stringdate'), site_weather_data.get('wind_speed'))
ax.set_title('wind_speed')
ax.axis('tight')

plt.show()