"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

ARX model for H358 office.
"""
from __future__ import annotations
import buildingenergy.linreg
import sites.data_h358
import sites.model_h358
import buildingenergy.data

#training_data = sites.data_h358.H358Data(starting_stringdate = '12/10/2015', ending_stringdate = '31/12/2015')  # starting_stringdate = '20/02/2015', ending_stringdate = '1/03/2015')

training_data_provider = sites.data_h358.make_h358_data_provider('12/10/2015', ending_stringdate = '31/12/2015')

validation_data_provider = sites.data_h358.make_h358_data_provider()

######## tuning parameter zone #######
offset: bool = True
minimum_input_delay: int = 0
input_names: tuple[str] = ('weather_temperature', 'window_opening', 'Tcorridor', 'door_opening', 'total_electric_power', 'Psun_window', 'dT_heat', 'occupancy')
inputs_maximum_delays = 10 # (2, 0, 0, 0, 2, 1, 1)  # int: same delay for each input of tuple[int], specific delay for each input
output_name: str = 'Toffice_reference'
ouput_maximum_delay = 10
######################################

print(training_data_provider)

buildingenergy.linreg.arx_estimation(output_name, input_names, training_data_provider, validation_data_provider, offset=offset, minimum_input_delay=minimum_input_delay, inputs_maximum_delays=inputs_maximum_delays, ouput_maximum_delay=ouput_maximum_delay)

print('Results have been saved as a markdown document in the "results/linreg" folder')


