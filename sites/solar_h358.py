"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
import matplotlib.pyplot as plt
import buildingenergy.solar
import buildingenergy.openweather
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()
site_weather_data = buildingenergy.openweather.WeatherJsonReader('grenoble1979-2022.json', from_stringdate = "1/01/2019", to_stringdate = "31/12/2019",  location='Grenoble').site_weather_data
solar_model = buildingenergy.solar.SolarModel(site_weather_data)

officeH358 = buildingenergy.solar.SolarSystem(solar_model)
officeH358.add_collector('main', surface=2, exposure_in_deg=-13, slope_in_deg=90, solar_factor=0.85, collector_mask=buildingenergy.solar.InvertedMask(buildingenergy.solar.RectangularMask((-86, 60), (20, 68))))
officeH358.generate_xls('officeH358', heat_temperature_reference=21, cool_temperature_reference=26)
solar_gains_with_mask, _ = officeH358.solar_gains_in_Wh()
print('total_solar_gain with mask in kWh:', sum(solar_gains_with_mask)/1000)

officeH358_nomask = buildingenergy.solar.SolarSystem(solar_model)
officeH358_nomask.add_collector('main', surface=2, exposure_in_deg=-13, slope_in_deg=90, solar_factor=0.85, collector_mask=None)
solar_gains_without_mask, _ = officeH358_nomask.solar_gains_in_Wh()
print('total_solar_gain without mask in kWh:', sum(solar_gains_without_mask)/1000)
fig, ax = plt.subplots()
plt.plot(officeH358_nomask.datetimes, solar_gains_without_mask)
plt.plot(officeH358.datetimes, solar_gains_with_mask)
ax.set_title('solar gain in Wh')
ax.legend(('no mask', 'mask'))
ax.axis('tight')
ax.grid()


#register_matplotlib_converters()
site_weather_data = buildingenergy.openweather.WeatherJsonReader('grenoble1979-2022.json', from_stringdate = '01/01/2015', to_stringdate = '01/01/2020', location='Grenoble', albedo=.1).site_weather_data
#solar_model = buildingenergy.solar.SolarModel(site_weather_data=site_weather_data)
solar_model.plot_heliodor(2015, 'heliodon')
solar_model.plot_solar_cardinal_irradiations()
phis1 = solar_model.irradiances(slope_deg=20, exposure_deg=buildingenergy.solar.DIRECTION['SOUTH'])
print('energy PV:', sum(phis1['total'])*21*.13/1000,'kWh')
phis2 = solar_model.irradiances(slope_deg=20, exposure_deg=buildingenergy.solar.DIRECTION['WEST'])
print('energy PV:', sum(phis2['total'])*21*.13/1000, 'kWh')
#solar_model.try_export()
plt.figure()
plt.plot(solar_model.site_weather_data.get('datetime'), phis2['total'])
plt.plot(solar_model.site_weather_data.get('datetime'), phis1['total'])
plt.legend(('sud','ouest'))
solar_model.plot_angles()
plt.show()