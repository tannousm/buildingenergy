"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

It is an instance of Model for the office H358
"""
from __future__ import annotations
import matplotlib.pyplot as plt
import buildingenergy.model
import buildingenergy.building 
import buildingenergy.thermal
import buildingenergy.data
import buildingenergy.solar 
import buildingenergy.parameters
import buildingenergy.openweather
import buildingenergy.physics

buildingenergy.physics.library.store('concrete', 'thermal', 269)
buildingenergy.physics.library.store('glass', 'thermal', 267)
buildingenergy.physics.library.store('foam', 'thermal', 260)
buildingenergy.physics.library.store('air', 'thermal', 259)
buildingenergy.physics.library.store('plaster', 'thermal', 265)

class Model(buildingenergy.model.Model):
    
    def __init__(self, parameters: buildingenergy.parameters.ParameterSet, data: buildingenergy.data.VariableSet, order: int=None) -> None:
        super().__init__(parameters=parameters, data=data, model_data_bindings={ ('PZindoor','Pheat'), ('CCO2_outdoor', 'CCO2_outdoor'), ('TZoutdoor', 'weather_temperature'), ('PCO2_indoor', 'PCO2')}, order=order)
        
    def create_zones(self) -> list[str]:
        return ['indoor']

    def create_interfaces(self, parameter_set: buildingenergy.parameters.ParameterSet):
                
        side = 10.
        height = 2.5
        wall_surface = 4 * side * height
        windows_surface = wall_surface * 0.1
        floor_surface = side**2
        self.house_volume = floor_surface * height

        if parameter_set('wall_composition') == 0:  #no insulation
            self.make_wall('indoor', 'outdoor',  wall_surface - windows_surface, ('concrete', 0.3))
            self.make_roof('indoor', 'outdoor',  floor_surface, ('concrete', 0.3))
        if parameter_set('wall_composition') == 1:  #no insulation, multi_layer concrete
            self.make_wall('indoor', 'outdoor',  wall_surface - windows_surface, ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05))
            self.make_roof('indoor', 'outdoor',  floor_surface, ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05))
        if parameter_set('wall_composition') == 2:  #no insulation, multi_layer concrete
            self.make_wall('indoor', 'outdoor',  wall_surface - windows_surface, ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02), ('concrete', 0.02))
            self.make_roof('indoor', 'outdoor',  floor_surface, ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05), ('concrete', 0.05))
        elif parameter_set('wall_composition') == 3:  #internal insulation
            self.make_wall('indoor', 'outdoor',  wall_surface - windows_surface, ('foam', 0.2), ('concrete', 0.3))
            self.make_roof('indoor', 'outdoor',  floor_surface, ('foam', 0.2), ('concrete', 0.3))
        elif parameter_set('wall_composition') == 4:  #external insulation
            self.make_wall('indoor', 'outdoor',  wall_surface - windows_surface, ('concrete', 0.3), ('foam', 0.2))
            self.make_roof('indoor', 'outdoor',  floor_surface, ('concrete', 0.3), ('foam', 0.2))
        elif parameter_set('wall_composition') == 5:  #plaster inside plus external insulation
            self.make_wall('indoor', 'outdoor',  wall_surface - windows_surface, ('foam', 0.013), ('concrete', 0.3), ('foam', 0.2))
            self.make_roof('indoor', 'outdoor',  floor_surface, ('plaster', 0.013), ('concrete', 0.3), ('foam', 0.2))
            self.make_glazing('indoor', 'outdoor', windows_surface, ('glass', 4e-3), ('air', 12e-3), ('glass', 4e-3))
        
    def define_simulated_zone_and_volume(self, parameter_set) -> tuple[str, float]:
        return 'indoor', self.house_volume

    def connect_zone_airflows(self) -> list[tuple[str, str]]:
        return [('outdoor', 'indoor'), ('indoor', 'outdoor')]
        
    def airflow_values(self, parameter_set) -> dict[str, float]:
        return {'Qoutdoor': 2*100/3600}
    
    def make_parameterized_data(self, parameters: buildingenergy.parameters.ParameterSet) -> buildingenergy.data.ParameterizedVariableSet:
        
        parameterized_data: buildingenergy.data.ParameterizedVariableSet = buildingenergy.data.ParameterizedVariableSet(parameters)
            
        def Pheat(parameters: buildingenergy.parameters.ParameterSet, data: buildingenergy.data.VariableSet) -> list[float]:
            Psun_windows: list[float] = data('Psun_windows')
            return [parameters('solar_factor') * Psun_windows[k] for k in range(len(data))]
        parameterized_data.add('Pheat', Pheat, 'Psun_windows')
        
        return parameterized_data


class LambdaData(buildingenergy.data.VariableSet):
    
    def __init__(self, starting_stringdate: str, ending_stringdate: str) -> None:
        super().__init__(csv_measurement_filename=None, json_openweather_filename='grenoble1979-2022.json', starting_stringdate=starting_stringdate, ending_stringdate=ending_stringdate, sea_level_in_meter=290, albedo=.1, pollution=0.1, location='Grenoble', deleted_variables=('wind_speed', 'wind_direction_in_deg', 'feels_like', 'humidity', 'pressure', 'temp_min', 'temp_max', 'description'))
        
        window_surface: float = 10
        solar_system = buildingenergy.solar.SolarSystem(self.site_weather_data)
        solar_system.add_collector('south', window_surface/4, 0, 90, 0.4, buildingenergy.solar.RectangularMask((-90 , 90), (0, 90)))
        solar_system.add_collector('east', window_surface/4, -90, 90, 0.4, buildingenergy.solar.RectangularMask((-180 , 0), (0, 90)))
        solar_system.add_collector('west', window_surface/4, 90, 90, 0.4, buildingenergy.solar.RectangularMask((0 , 180), (0, 90)))
        solar_system.add_collector('north', window_surface/4, 180, 90, 0.4, buildingenergy.solar.RectangularMask((90 , 270), (0, 90)))
        Psun, _ = solar_system.solar_gains_in_Wh()
        self.add_external_variable('Psun_windows', Psun)
        
        
if __name__ == '__main__':
    
    data: LambdaData = LambdaData(starting_stringdate='1/4/2019', ending_stringdate='1/7/2019')
    print(data)
    site_weather_data = data.site_weather_data
     
    print('Model generation')
    
    parameters: buildingenergy.parameters.ParameterSet = buildingenergy.parameters.ParameterSet()
    parameters('wall_composition', 1)
    parameters('Qoutdoor', 2*100/3600)
    parameters('PCO2', 0)
    parameters('CCO2_outdoor', 400)
    parameters('solar_factor', 0.4, (.1, .85,.1))
    
    model = Model(parameters=parameters, data=data, order=None)
    
    simulated_outputs_series = model.pre_simulate()
    for output_name in simulated_outputs_series:
        model.data.add_external_variable(output_name, simulated_outputs_series[output_name])
            
    model.data.plot()