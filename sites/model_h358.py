"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0/

It is an instance of Model for the office H358
"""
from __future__ import annotations
import time
import buildingenergy.data
import buildingenergy.building 
import buildingenergy.thermal
import buildingenergy.model 
import buildingenergy.physics
import buildingenergy.parameters
import sites.data_h358
import sites.building_h358

print('Loading data')
start: float = time.time()
training_data_provider = sites.data_h358.DataProvider(sites.data_h358.H358ParameterSet(),  sites.data_h358.H358Data(starting_stringdate='15/02/2015', ending_stringdate='15/02/2016', number_of_levels=3, verbose=True),  sites.data_h358.h358_bindings)
sites.data_h358.H358ParameterizedDataSet(training_data_provider)
print('\nduration: %i secondes' % round(time.time() - start))


def main():
    print('Simulation')
    start: float = time.time()
    varying_state_model = buildingenergy.model.VaryingStateModel(training_data_provider, sites.building_h358.h358_building, verbose=True)
    variable_simulated_values = varying_state_model.simulate()
    print('\nduration: %i secondes' % round(time.time() - start))

    for variable_name in variable_simulated_values:
        training_data_provider.add_external_variable(variable_name + '_SIM', variable_simulated_values[variable_name])
        
    # training_data_provider.plot()


if __name__ == '__main__':
    main()

# python3.11 -m cProfile -o model_h358.prof sites/model_h358.py
# snakeviz model_h358.prof
#
# pip3.11 install line_profiler  
# python3.11 -m kernprof -l -v sites/model_h358.py
