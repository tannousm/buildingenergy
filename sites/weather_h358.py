"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
import buildingenergy.openweather
import buildingenergy.solar
import matplotlib.pylab as plt
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()
site_weather_data = buildingenergy.openweather.WeatherJsonReader('grenoble1979-2022.json', '1/01/2019', '1/01/2020', location='Grenoble', albedo=.1).site_weather_data
site_weather_data2 = buildingenergy.openweather.WeatherJsonReader('Grenoble-INP1990.json', '1/01/2019', '1/01/2020', albedo=.1).site_weather_data

print(site_weather_data)
print(site_weather_data2)

solar_model = buildingenergy.solar.SolarModel(site_weather_data2)


fig, ax = plt.subplots()
plt.plot(site_weather_data.get('datetime'), site_weather_data2.get('direct_radiation'), label='horizontal')  # total
# plt.plot(site_weather_data.get('datetime'), site_weather_data2.get('diffuse_radiation'), label='diffuse_radiation')  # diffuse
# plt.plot(site_weather_data.get('datetime'), site_weather_data2.get('direct_normal_irradiance'), label='perpendicular to the sun beams')  # direct
exposure = -90  # - 90 east, 90 west, 0 south, 180 north (clockwise with South as reference)
slope = 0  # 0 facing the sky, 90 facing the exposure
irradiances = solar_model.irradiances(exposure_deg=exposure, slope_deg=slope, mask=buildingenergy.solar.RectangularMask(minmax_azimuths_deg=(-90+exposure,90+exposure), minmax_altitudes_deg=(-90+slope, 90+slope)))

print('openmeteo: %f' % (sum(site_weather_data2.get('direct_radiation'))/1000))
print('calculus: %f' % (sum(irradiances['total'])/1000))

# plt.plot(site_weather_data2.get('datetime'), irradiances['direct'], label='model_direct')  # direct
# plt.plot(site_weather_data2.get('datetime'), irradiances['diffuse'], label='model_diffuse')  # diffuse
# plt.plot(site_weather_data2.get('datetime'), irradiances['reflected'], label='model_reflected')
plt.plot(site_weather_data2.get('datetime'), irradiances['total'], label='model_total')  # total
# plt.plot(site_weather_data2.get('datetime'), irradiances['normal'], label='model_normal')

ax.set_title('irradiances')
plt.legend()
ax.axis('tight')
# fig, ax = plt.subplots()
# plt.plot(site_weather_data2.get('datetime'), solar_model.sun_altitudes_deg, label='altitude')
# plt.plot(site_weather_data2.get('datetime'), solar_model.sun_azimuths_deg, label = 'azimuth')
# ax.set_title('angles')
# plt.legend()
# ax.axis('tight')
plt.show()