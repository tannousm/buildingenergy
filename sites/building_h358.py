"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
from buildingenergy.building import SideType, Building, BlockWallSide, LayeredWallSide
from sites.data_h358 import h358_data_provider as dp
import sites.data_h358
import time

# Construction of the wall sides and specification of the maximum order of the resulting state model but also tune the decomposition of layers into sublayer for a more precise simulation based on the depth penetration of a sine wave
h358_building = Building('office', 'corridor', 'downstairs', data_provider=dp, periodic_depth_seconds=60, state_model_order_max=3)

# construction of the wall sides between the office and the corridor

door_surface: float = 80e-2 * 200e-2
door: LayeredWallSide = h358_building.layered_wall_side('office', 'corridor', SideType.DOOR, door_surface)
door.add_layer('wood', 5e-3)
door.add_layer('air', 15e-3)
door.add_layer('wood', 5e-3)

glass_surface: float = 100e-2 * 100e-2
glass: LayeredWallSide = h358_building.layered_wall_side('office', 'corridor', SideType.GLAZING, glass_surface)
glass.add_layer('glass', 4e-3)
internal_wall_thickness: float = 13e-3 + 34e-3 + 13e-3
cupboard_corridor_surface: float = (185e-2 + internal_wall_thickness + 34e-2 + 20e-3) * 2.5
corridor_wall_surface: float = (408e-2 + 406e-2 + internal_wall_thickness) * 2.5 - door_surface - glass_surface - cupboard_corridor_surface

cupboard: LayeredWallSide = h358_building.layered_wall_side('office', 'corridor', SideType.WALL, cupboard_corridor_surface)
cupboard.add_layer('plaster', 13e-3)
cupboard.add_layer('foam', 34e-3)
cupboard.add_layer('plaster', 13e-3)
cupboard.add_layer('air', 50e-2 - 20e-3)
cupboard.add_layer('wood', 20e-3)

plain_corridor_wall: LayeredWallSide = h358_building.layered_wall_side('office', 'corridor', SideType.WALL, corridor_wall_surface)
plain_corridor_wall.add_layer('plaster', 13e-3)
plain_corridor_wall.add_layer('foam', 34e-3)
plain_corridor_wall.add_layer('plaster', 13e-3)

# construction of the wall sides between outdoor wall

west_glass_surface: float = 2 * 130e-2 * 52e-2 + 27e-2 * 52e-2 + 72e-2 * 52e-2
east_glass_surface: float = 36e-2 * 56e-2
windows_surface: float = west_glass_surface + east_glass_surface
no_cavity_surface: float = (685e-2 - 315e-2 - 60e-2) * 2.5 - east_glass_surface
cavity_surface: float = 315e-2 * 2.5 - west_glass_surface

windows: LayeredWallSide = h358_building.layered_wall_side('office', 'outdoor', SideType.WALL, windows_surface)
windows.add_layer('glass', 4e-3)
windows.add_layer('air', 12e-3)
windows.add_layer('glass', 4e-3)

plain_wall: LayeredWallSide = h358_building.layered_wall_side('office', 'outdoor', SideType.WALL, no_cavity_surface)
plain_wall.add_layer('concrete', 30e-2)

cavity_wall: LayeredWallSide = h358_building.layered_wall_side('office', 'outdoor', SideType.WALL, cavity_surface)
cavity_wall.add_layer('concrete', 30e-2)
cavity_wall.add_layer('air', 34e-2)
cavity_wall.add_layer('wood', 20e-3)

bridge: BlockWallSide = h358_building.block_wall_side('office', 'outdoor', SideType.BRIDGE, 0.5 * 0.99 * 685e-2)  # thermal bridge obtained from ThBAT booklet 5, 3.1.1.2, 22B)

# construction of the slab

slab_effective_thickness = 11.9e-2
slab_surface: float = (309e-2 + 20e-3 + 34e-2) * (406e-2 + internal_wall_thickness) + 408e-2 * (273e-2 - 60e-2) - 315e-2 * (34e-2 + 20e-3) - (185e-3 + internal_wall_thickness) * 50e-2
slab: LayeredWallSide = h358_building.layered_wall_side('office', 'downstairs', SideType.WALL, slab_surface)
slab.add_layer('concrete', slab_effective_thickness)
slab.add_layer('air', 20e-2)
slab.add_layer('polystyrene', 7e-3)

# construction of the airflows between zones

h358_building.simulate_zone('office')
h358_building.connect_airflow('office', 'corridor', dp('corridor-office:Q_0'))
h358_building.connect_airflow('office', 'outdoor', dp('office-outdoor:Q_0'))

state_model = h358_building.make_state_model({'Q(corridor,office)':  dp('office-corridor:Q_0'), 'Q(office,outdoor)': dp('office-outdoor:Q_0')}, reset_reduction=False)

if __name__ == '__main__':

    # display the characteristics of the resulting building
    print('Building characteristics:')
    print(h358_building)

    print('\n State model characteristics:')
    print(state_model)

    # load data and plot results
    print('Loading data')

    start: float = time.time()
    state_model.simulate(sites.data_h358.h358_data_provider)
    print('\nmodel simulation duration: %f secondes' % (time.time() - start))

    sites.data_h358.h358_data_provider.plot()
