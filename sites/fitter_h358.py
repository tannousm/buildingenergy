from __future__ import annotations
import numpy
import numpy.linalg
import time
import SALib.analyze.morris
import buildingenergy.data
import buildingenergy.model
import buildingenergy.parameters
import buildingenergy.statemodel
import buildingenergy.model
import sites.data_h358
import buildingenergy.fitter

print('Loading data')
parameters = sites.data_h358.H358ParameterSet()
training_data_provider =  sites.data_h358.DataProvider(parameters, sites.data_h358.H358Data(starting_stringdate='1/03/2015', ending_stringdate='1/05/2015', number_of_levels=3, verbose=True),  sites.data_h358.h358_bindings)
sites.data_h358.H358ParameterizedDataSet(training_data_provider)

validation_data_provider =  sites.data_h358.DataProvider(parameters, sites.data_h358.H358Data(starting_stringdate='15/02/2015', ending_stringdate='15/02/2016', number_of_levels=3, verbose=True),  sites.data_h358.h358_bindings)
sites.data_h358.H358ParameterizedDataSet(validation_data_provider)

print('Model generation')
varying_state_model = buildingenergy.model.VaryingStateModel(training_data_provider, sites.building_h358.h358_building, verbose=True)

fitter: buildingenergy.fitter.ModelFitter = buildingenergy.fitter.ModelFitter(varying_state_model)

# fitter.sensitivity(2)

print('- Model fitting')
start: float = time.time()
best_parameters, best_outputs_simulated_data, best_error = fitter.fit(5, validation_data_provider)
print(best_parameters)
print('levels=(', ', '.join(['%i' % level for level in parameters.adjustable_parameter_levels]), ')')
for simulated_output_name in best_outputs_simulated_data:
    validation_data_provider.add_external_variable(simulated_output_name+'_simulation', best_outputs_simulated_data[simulated_output_name])
validation_data_provider.plot()

print('%i secs' % (time.time() - start))
