"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

It is an instance of Model for the office H358
"""
from __future__ import annotations
import matplotlib.pyplot as plt
import buildingenergy.building 
import buildingenergy.thermal 
import buildingenergy.parameters
import buildingenergy.physics

buildingenergy.physics.library.store('concrete', 'thermal', 269)
buildingenergy.physics.library.store('glass', 'thermal', 267)
buildingenergy.physics.library.store('foam', 'thermal', 260)
buildingenergy.physics.library.store('air', 'thermal', 259)


ventilated_site = buildingenergy.building.Building('indoor')
side: float = 10.
height: float = 2.5
wall_surface: float = 4 * side * height
windows_surface: float = wall_surface * 0.1
floor_surface: float = side**2
house_volume: float = floor_surface * height

wall = ventilated_site.side('indoor', 'outdoor', buildingenergy.building.SideType.WALL, wall_surface - windows_surface)
wall.add_layer('foam', 0.2)
wall.add_layer('concrete', 0.3)

roof = ventilated_site.side('indoor', 'outdoor', buildingenergy.building.SideType.ROOF, floor_surface)
roof.add_layer('foam', 0.2)
roof.add_layer('concrete', 0.3)

glazing = ventilated_site.side('indoor', 'outdoor', buildingenergy.building.SideType.GLAZING, windows_surface)
glazing.add_layer('glass', 4e-3)
glazing.add_layer('air', 12e-3)
glazing.add_layer('glass', 4e-3)

ventilated_site.simulated_zone('indoor', house_volume)
ventilated_site.connect_airflow('outdoor', 'indoor', 1)
ventilated_site.connect_airflow('indoor', 'outdoor', 1)

ventilated_site.propagate_airflows_in_thermal_network()
print(ventilated_site)
print('global_input_airflows:', ','.join(ventilated_site.propagated_airflow_names))

parameter_set = buildingenergy.parameters.ParameterSet()
parameter_set('Qoutdoor', 12/3600)

state_model = ventilated_site.generate_state_model(propagated_airflow_values=parameter_set)
print(state_model)

# ventilated_site.draw_thermal_net()
# ventilated_site.draw_airflow_net()
# plt.show()