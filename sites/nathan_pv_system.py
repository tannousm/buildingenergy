from __future__ import annotations

import numpy
import time
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt
from matplotlib import cm
import buildingenergy
import buildingenergy.timemg
import buildingenergy.solar
import buildingenergy.openweather
# %matplotlib inline

def main():
    ground_surface_in_m2: float = 50  # in m2
    PV_panel_peak_power: float = .25
    panel_height_in_m: float = 1.2  # in m
    PV_inverter_efficiency: float = 0.95 
    temperature_coefficient: float = 0.0035
    array_width: float = 10  # in m
    skyline_azimuths_altitudes_in_deg: tuple[float, float] = ((-180, 0), (180, 0))

    open_weather_map_json_reader = buildingenergy.openweather.WeatherJsonReader('campus_transition_alpespace.json', from_stringdate='1/01/2022', to_stringdate='1/01/2023',  albedo=0.1, pollution=0.1, location="Campus de la Transition")
    site_weather_data = open_weather_map_json_reader.site_weather_data
    solar_model = buildingenergy.solar.SolarModel(site_weather_data) 

    pv_system: buildingenergy.solar.PVsystem = buildingenergy.solar.PVsystem(solar_model, surfacePV_in_m2=50, array_width=10, panel_height_in_m=1.2, efficiency=.5, temperature_coefficient=0.0035)
    
    datetimes = numpy.array(pv_system.datetimes)
    fig = make_subplots(rows=1, cols=1, shared_xaxes=True)
    
    exposure_in_deg = 90  # 0° means directed to the South, 90° to the West,...
    slope_in_deg = 45  # in degrees: 0 = face the sky, 90° = face to the south
    distance_between_arrays_in_m = 2

    PV_efficiency: float = PV_panel_peak_power * PV_inverter_efficiency
    pv_system: buildingenergy.solar.PVsystem = buildingenergy.solar.PVsystem(solar_model, surfacePV_in_m2=ground_surface_in_m2, array_width=array_width, panel_height_in_m=panel_height_in_m, efficiency=PV_efficiency, temperature_coefficient=temperature_coefficient)
    
    datetimes = numpy.array(pv_system.datetimes)
    fig = make_subplots(rows=1, cols=1, shared_xaxes=True)

    productions_in_kWh: list[float] = pv_system.productions_in_kWh(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, buildingenergy.solar.MOUNT_TYPE.FLAT) 
    pv_system_productions_in_kWh = numpy.array(productions_in_kWh)
    fig.add_trace(go.Scatter(x=datetimes, y=pv_system_productions_in_kWh, name='flat mount PV production in kWh', line_shape='hv'), row=1, col=1)
    pv_system.print_month_hour_productions(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, buildingenergy.solar.MOUNT_TYPE.FLAT, productions_in_kWh)

    # productions_in_kWh: list[float] =                                                                                                                                               c  c           pv_system.productions_in_kWh(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, buildingenergy.solar.MOUNT_TYPE.SAW) 
    pv_system_productions_in_kWh = numpy.array(productions_in_kWh)
    fig.add_trace(go.Scatter(x=datetimes, y=pv_system_productions_in_kWh, name='saw mount PV production in kWh', line_shape='hv'), row=1, col=1)
    pv_system.print_month_hour_productions(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, buildingenergy.solar.MOUNT_TYPE.SAW, productions_in_kWh)

    productions_in_kWh: list[float] = pv_system.productions_in_kWh(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, buildingenergy.solar.MOUNT_TYPE.ARROW) 
    pv_system_productions_in_kWh = numpy.array(productions_in_kWh)
    fig.add_trace(go.Scatter(x=datetimes, y=pv_system_productions_in_kWh, name='arrow mount PV production in kWh', line_shape='hv'), row=1, col=1)
    pv_system.print_month_hour_productions(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, buildingenergy.solar.MOUNT_TYPE.ARROW, productions_in_kWh)
    
    fig.update_layout(title="exposure: %f°, slope: %f°, distance: %f"%(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m), xaxis_title="date & tim (each hour)", yaxis_title="PV electricity production in kWh")
    fig.show()
    
    
    print(pv_system)

    mount_type = buildingenergy.solar.MOUNT_TYPE.ARROW
    distances_between_arrays_in_m: list[float] = [i/100 for i in (10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 175, 200)]
    panel_slopes_in_deg: list[float] = [i for i in range(5, 90, 5)]
    exposure_in_deg = 90
    
    t0 = time.time()
    productions_in_kWh, productions_in_kWh_per_pv_surf = buildingenergy.solar.pv_productions_distances_slopes(pv_system, exposure_in_deg, panel_slopes_in_deg, distances_between_arrays_in_m, mount_type)
    print('pv_productions_distances_slopes:', (time.time() - t0)/60, 'min')
    
    X, Y = numpy.meshgrid(panel_slopes_in_deg, distances_between_arrays_in_m)
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.plot_surface(X, Y, productions_in_kWh, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    ax.set_xlabel('panel slope (deg)')
    ax.set_ylabel('distance between panels (m)')
    ax.set_zlabel('production (kWh)')
    ax.set_title('exposure: %f°, mount type: %s' % (exposure_in_deg, mount_type))
    
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.plot_surface(X, Y, productions_in_kWh_per_pv_surf, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    ax.set_xlabel('panel slope (deg)')
    ax.set_ylabel('distance between panels (m)')
    ax.set_zlabel('production per m2 of PV (kWh/m2)')
    ax.set_title('exposure: %f°, mount type: %s' % (exposure_in_deg, mount_type))
    
    exposures_in_deg: list[float] = list(range(-90, 90, 5))
    slopes_in_deg: list[float] = list(range(0, 90, 5))
    distance_between_arrays_in_m: float = 1.2
    mount_type: buildingenergy.solar.MOUNT_TYPE = buildingenergy.solar.MOUNT_TYPE.ARROW

        
    t0 = time.time()
    pv_productions = buildingenergy.solar.pv_productions_angles(pv_system, exposures_in_deg, slopes_in_deg, distance_between_arrays_in_m, mount_type) 
    print('pv_productions_distances_slopes:', (time.time() - t0)/60, 'min')
     
    X, Y = numpy.meshgrid(exposures_in_deg, slopes_in_deg)
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.plot_surface(X, Y, pv_productions, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    ax.set_xlabel('exposure (deg)')
    ax.set_ylabel('slope (deg)')
    ax.set_zlabel('production (kWh)')
    ax.set_title('distance: %fm, mount type: %s' % (distance_between_arrays_in_m, mount_type))
    
if __name__ == '__main__':
    main()
    plt.show()
    # python3.11 -m cProfile -o nathan.prof nathan_pv_system.py
    # snakeviz nathan.prof
    #
    # pip3.11 install line_profiler  
    # python3.11 -m kernprof -l -v nathan.py