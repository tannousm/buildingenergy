"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
import buildingenergy.thermal
import buildingenergy.physics
import buildingenergy.parameters
import buildingenergy.building
import matplotlib.pyplot as plt


h358_office_site = buildingenergy.building.Building('office', 'corridor', 'downstairs')


# corridor wall
door_surface = 80e-2 * 200e-2
door = h358_office_site.side('office', 'corridor', buildingenergy.building.SideType.DOOR, door_surface)
door.add_layer('wood', 5e-3)
door.add_layer('air', 15e-3)
door.add_layer('wood', 5e-3)

glass_surface = 100e-2 * 100e-2
glass: buildingenergy.building.LayeredWallSide = h358_office_site.side('office', 'corridor', buildingenergy.building.SideType.GLAZING, glass_surface)
glass.add_layer('glass', 4e-3)

internal_wall_thickness = 13e-3 + 34e-3 + 13e-3
cupboard_corridor_surface: float = (185e-2 + internal_wall_thickness + 34e-2 + 20e-3) * 2.5
corridor_wall_surface: float = (408e-2 + 406e-2 + internal_wall_thickness) * 2.5 - door_surface - glass_surface - cupboard_corridor_surface

cupboard: buildingenergy.building.LayeredWallSide = h358_office_site.side('office', 'corridor', buildingenergy.building.SideType.WALL, cupboard_corridor_surface)
cupboard.add_layer('plaster', 13e-3)
cupboard.add_layer('foam', 34e-3)
cupboard.add_layer('plaster', 13e-3)
cupboard.add_layer('air', 50e-2 - 20e-3)
cupboard.add_layer('wood', 20e-3)

plain_corridor_wall: buildingenergy.building.LayeredWallSide = h358_office_site.side('office', 'corridor', buildingenergy.building.SideType.WALL, corridor_wall_surface)
plain_corridor_wall.add_layer('plaster', 13e-3)
plain_corridor_wall.add_layer('foam', 34e-3)
plain_corridor_wall.add_layer('plaster', 13e-3)

# outdoor wall
west_glass_surface = 2 * 130e-2 * 52e-2 + 27e-2 * 52e-2 + 72e-2 * 52e-2
east_glass_surface = 36e-2 * 56e-2
windows_surface = west_glass_surface + east_glass_surface
nocavity_surface = (685e-2 - 315e-2 - 60e-2) * 2.5 - east_glass_surface
cavity_surface = 315e-2 * 2.5 - west_glass_surface

windows: buildingenergy.building.LayeredWallSide = h358_office_site.side('office', 'outdoor', buildingenergy.building.SideType.WALL, windows_surface)
windows.add_layer('glass', 4e-3)
windows.add_layer('air', 12e-3)
windows.add_layer('glass', 4e-3)

plain_wall: buildingenergy.building.LayeredWallSide = h358_office_site.side('office', 'outdoor', buildingenergy.building.SideType.WALL, nocavity_surface)
plain_wall.add_layer('concrete', 30e-2)

cavity_wall: buildingenergy.building.LayeredWallSide = h358_office_site.side('office', 'outdoor', buildingenergy.building.SideType.WALL, cavity_surface)
cavity_wall.add_layer('concrete', 30e-2)
cavity_wall.add_layer('air', 34e-2)
cavity_wall.add_layer('wood', 20e-3)

# slab
slab_effective_thickness = 11.9e-2
slab_surface = (309e-2 + 20e-3 + 34e-2) * (406e-2 + internal_wall_thickness) + 408e-2 * (273e-2 - 60e-2) - 315e-2 * (34e-2 + 20e-3) - (185e-3 + internal_wall_thickness) * 50e-2
slab = h358_office_site.side('office', 'downstairs', buildingenergy.building.SideType.WALL, slab_surface)
slab.add_layer('concrete', slab_effective_thickness)
bridge = h358_office_site.add_component_interface('office', 'outdoor', buildingenergy.building.SideType.BRIDGE, 0.5 * 0.99, 685e-2)  # ThBAT booklet 5, 3.1.1.2, 22B)


net = buildingenergy.thermal.ThermalNetwork()
net.T('Tcor', buildingenergy.thermal.CAUSALITY.IN)
net.T('Tout', buildingenergy.thermal.CAUSALITY.IN)
net.T('Tin', buildingenergy.thermal.CAUSALITY.OUT)
net.T('Tslab')

net.R('Tcor','Tin', 'Rcor', value=h358_office_site.get_interface_thermal_resistance('corridor', 'office'))
net.C('Tslab', 'Ci', value=h358_office_site.get_first_facet_interface_thermal_capacitance('downstairs', 'office')[0])
net.R('Tslab', 'Tin', 'Ri', value=h358_office_site.get_interface_thermal_resistance('downstairs', 'office')/2)
net.HEAT('Tin', 'Pin')
net.R('Tin', 'Tout', 'Rout', value=h358_office_site.get_interface_thermal_resistance('office', 'outdoor'))

print(h358_office_site)
state_model = net.state_model()
print(state_model)
net.draw()
plt.show()

# A [[-9.730851245614724e-06]]
# B [[4.3509562457146814e-06, 5.379894999900045e-06]], [[1.318970797523004e-07]]
# C [[0.6578142355374829]]
# D [[0.15300154647352113, 0.18918421798899615]], [[0.004638165965773461]]
