from __future__ import annotations
import datetime
import random
import buildingenergy.solar
import buildingenergy.openweather
import ecommunity.irise
import ecommunity.simulator


class CommunityMember(ecommunity.simulator.SynchronizedMember):

    def __init__(self, member: ecommunity.irise.House, datetimes: list[datetime.datetime], group_name: str, randomize_ratio: float=.2, averager_depth_in_hours: int=3):
        super().__init__(member, datetimes, group_name, randomize_ratio, averager_depth_in_hours)

    def day_interactions(self, the_date: datetime.date, day_hour_indices: list[int], interaction: int, init: bool):
        pass
    
    def hour_interactions(self, the_datetime: datetime.datetime, hour_index: int,  interaction: int, init: bool):
        pass

class NoSignalCommunityManager(ecommunity.simulator.Manager):

    def __init__(self, pv_plant: buildingenergy.solar.PVplant, number_of_day_interactions: int, number_of_hour_interactions: int, no_alert_threshold, randomize_ratio: int=.2, averager_depth_in_hours: int=3) -> None:
        super().__init__(pv_plant, number_of_day_interactions, number_of_hour_interactions, no_alert_threshold=no_alert_threshold, randomize_ratio=randomize_ratio, averager_depth_in_hours=averager_depth_in_hours)
        
        for member in ecommunity.irise.IRISE(zipcode_pattern='381%').get_members():
            member_agent = CommunityMember(member, self.datetimes, 'ecom')
            self.register_synchronized_member(member_agent)


    def day_interactions(self, the_date: datetime.date, day_hour_indices: list[int], interaction: int, init: bool) -> None:
        pass

    def hour_interactions(self, the_datetime: datetime.datetime, hour_index: int,  interaction: int, init: bool) -> None:
        pass

    def finalize(self) -> None:
        pass


if __name__ == '__main__':
    site_weather_data: buildingenergy.openweather.SiteWeatherData = buildingenergy.openweather.WeatherJsonReader('grenoble1979-2022.json', from_stringdate='1/01/2021', to_stringdate='1/01/2022', altitude=330, albedo=0.1, pollution=0.1, location="Grenoble").site_weather_data

    pv_plant_properties = buildingenergy.solar.PVplantProperties()
    pv_plant_properties.skyline_azimuths_altitudes_in_deg: list[tuple[float, float]] = ([(-180.0,13.8), (-170.0,18.9), (-145.1,9.8), (-120.0,18.3), (-96.1,17.3), (-60.8,6.2), (-14.0,2.6), (-8.4,5.6), (0.8,2.6), (21.6,5.5), (38.1,14.6), (49.4,8.9), (60.1,11.3), (87.4,10.4), (99.3,12.0), (142.1,2.6), (157.8,4.0), (175.1,17.1), (180.0,15.9)])
    pv_plant_properties.surfacePV_in_m2: float = 16
    pv_plant_properties.panel_height_in_m: float = 1.2
    pv_plant_properties.efficiency = 0.2 * 0.95 
    pv_plant_properties.temperature_coefficient: float = 0.0035
    pv_plant_properties.array_width: float = 4  # in m
    pv_plant_properties.exposure_in_deg=0  # TO BE ADJUSTED IF NEEDED
    pv_plant_properties.slope_in_deg=0  # TO BE ADJUSTED IF NEEDED
    pv_plant_properties.distance_between_arrays_in_m = 1.2  # TO BE ADJUSTED IF NEEDED
    pv_plant_properties.mount_type: buildingenergy.solar.MOUNT_TYPE = buildingenergy.solar.MOUNT_TYPE.FLAT  # TO BE ADJUSTED IF NEEDED
    
    pv_plant: buildingenergy.solar.PVplant = buildingenergy.solar.PVplant(buildingenergy.solar.SolarModel(site_weather_data), pv_plant_properties)
    
    manager = NoSignalCommunityManager(pv_plant, number_of_day_interactions=1, number_of_hour_interactions=0, no_alert_threshold=1)
    manager.run()
