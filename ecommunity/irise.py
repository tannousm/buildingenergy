"""1h-sampled consumption data extraction for 100 Houses from the IRISE database: it includes all the electric appliances

Author: stephane.ploix@grenoble-inp.fr
"""
from __future__ import annotations

import sqlalchemy
import numpy
import datetime
import configparser
import logging
import matplotlib.pyplot as plt
import pandas
import scipy.stats
from buildingenergy import timemg
import buildingenergy.openweather
import ecommunity.indicators
from buildingenergy.solar import MOUNT_TYPE 

logging.basicConfig(level=logging.INFO)


class IRISE:
    """House data extractor from the IRISE SQLite3 database: it contains all the electric consumptions measured for each of the 100 houses involved in the REMODECE/IRISE project during the beginning of 1998 till 2000. Because the weather data are going to be adapted to our context i.e. the data are shifted in order for the day of weeks to be preserved and to be as close. The administrative time changes carried you twice a year are taken into account."""

    def __init__(self, zipcode_pattern:str='38%') -> None:
        """Initialize the house data extractor by getting connected to the database, extracting all the data and closing the connection.

        :param simulator: the simulator that is going to use the IRISE database
        :type from_datestring: scheduler.IRISEsimulator
        """
        config = configparser.ConfigParser()
        config.read('setup.ini')
        filename = config['folders']['data'] + config['databases']['irise']
        database = sqlalchemy.create_engine('sqlite:///'+filename)
        database_connection = database.connect()
        self._irise_houses = dict()
        query = database_connection.execute(sqlalchemy.text("SELECT ID, ZIPcode, Location FROM HOUSE  WHERE ZIPcode LIKE '%s';" % zipcode_pattern))
        rows = query.cursor.fetchall()
        number_of_houses = len(rows)
        for id, zip_code, location in rows:
            self._irise_houses[id] = House(database_connection, id, zip_code, location, 1/number_of_houses)
        database_connection.invalidate()
        database.dispose()
        
    def get_house(self, id: int)-> House:
        return self._irise_houses[id]
    
    def get_members(self) -> list[House]:
        return list(self._irise_houses.values())
        
    def __str__(self) -> str:
        string: str = ''
        for house_id in self._irise_houses:
            string += '- %s\n' % self._irise_houses[house_id].__str__()
        return string

class House:
    """A house generated from the IRISE SQLite3 database."""

    def __init__(self, database_connection, house_id: int, zip_code: str, location: str, share: float=0):
        """Initialize a house

        :param database_connection: SQLalchemy database connection
        :type database_connection: SQLalchemy
        :param id: id of the house
        :type id: int
        :param zip_code: zip code of the house
        :type zip_code: str
        :param location: location of the house
        :type location: str
        """
        self.id: int = house_id
        self.zip_code = zip_code
        self.name: str = 'house:id=%i@%s-%s' % (house_id, zip_code, location)
        self.location: str = location
        self._id_appliances: dict[int, Appliance] = dict()
        self.share: float = share
        
        query = database_connection.execute(sqlalchemy.text("SELECT ID, Name FROM Appliance WHERE HouseIDREF=%i;" % house_id))
        for appliance_id, name in query.cursor.fetchall():
            self._id_appliances[appliance_id] = Appliance(database_connection, appliance_id, house_id, name)
            if self._id_appliances[appliance_id].name == 'site_consumption':
                self.site_consumption_id: int = appliance_id
                
    def id_appliances(self) -> dict[int, Appliance]:
        return self._id_appliances
    
    def get_appliance(self, id_appliance: int) -> Appliance:
        return self._id_appliances[id_appliance]
    
    @property
    def datetimes(self):
        return self._id_appliances[self.site_consumption_id].datetimes

    def consumptions(self, datetimes: list[datetime.datetime]=None):
        """Return the site consumption of the house.

        :return: site consumption (detailed consumptions are also available)
        :rtype: list[float]
        """
        return self._id_appliances[self.site_consumption_id].consumptions_in_kWh(datetimes)

    def __str__(self) -> str:
        """Return a description of the house

        :return: text description
        :rtype: str
        """
        string: str = 'House %s located at %s %s with appliances:\n' % (self.id, self.zip_code, self.location)
        for appliance_id in self._id_appliances:
            string += '\t%i: %s\n' % (appliance_id, self._id_appliances[appliance_id].__str__())
        return string


class Appliance:
    """An appliance related to a house."""

    def __init__(self, database_connection, appliance_id: int, house_id: int, name: str):
        """Initialize the appliance.

        :param database_connection: sqlite3 database connection
        :type database_connection: SQLalchemy
        :param id: identifier of the appliance in a house
        :type id: int
        :param house_id: identifier of the house containing the appliance
        :type house_id: int
        :param name: name of the appliance
        :type name: str
        """
        self.name: str = name.split(" (")[0]
        self.name = self.name.replace(" ", "_")
        self.name = self.name.lower()
        self.appliance_id = appliance_id
        irise_datetimes = list()
        similarity = list()
        irise_weekdays = list()
        irise_values = list()

        request = sqlalchemy.text("SELECT EpochTime, Value FROM Consumption WHERE HouseIDREF=%i AND ApplianceIDREF=%i ORDER BY EpochTime ASC;" % (house_id, appliance_id))
        query = database_connection.execute(request)
        epochtime_values = query.cursor.fetchall()
        irise_datetimes_10min: list[int] = [timemg.epochtimems_to_datetime(epochtime_values[i][0] * 1000).replace(tzinfo=None) for i in range(len(epochtime_values))]
        irise_values_10min: list[float] = [epochtime_values[i][1] for i in range(len(epochtime_values))]
        while irise_datetimes_10min[0].minute != 0:
            del irise_datetimes_10min[0]
            del irise_values_10min[0]
        for i in range(0, len(irise_datetimes_10min)-6, 6):
            irise_datetimes.append(irise_datetimes_10min[i])
            irise_values.append(sum([irise_values_10min[i + j] for j in range(0, 6)]))
            irise_weekdays.append(irise_datetimes_10min[i].weekday())
            stime = irise_datetimes_10min[i].timetuple()
            similarity.append(stime.tm_yday + 366 * irise_datetimes_10min[i].hour)
        self.irise_df = pandas.DataFrame(index=irise_datetimes, data={'similarity': similarity, 'weekday': irise_weekdays, 'value': irise_values})
        self.irise_df: pandas.DataFrame = self.irise_df[(numpy.abs(scipy.stats.zscore(self.irise_df.value)) < 3)]
        self.irise_equivalent_datetimes = None
        
    @property
    def datetimes(self) -> list[datetime.datetime]:
        return self.irise_df.index.tolist()
    
    def _equivalent_datetime(self, target_datetime) -> datetime.datetime:
        stime: datetime.struct_time = target_datetime.timetuple()
        similarity: int = stime.tm_yday + target_datetime.hour * 366
        target_weekday: int = target_datetime.weekday()
        irise_target_weekday_df: pandas.DataFrame = self.irise_df.loc[self.irise_df.weekday == target_weekday]
        closest_irise_datetime: datetime.datetime = irise_target_weekday_df.similarity.sub(similarity).abs().idxmin()
        return closest_irise_datetime

    def consumptions_in_kWh(self, target_datetimes: list[datetime.datetime]=None)-> tuple[list[datetime.datetime], list[float]]:
        if target_datetimes is None:
            return [c/1000 for c in self.irise_df.value.tolist()]
        else: 
            if self.irise_equivalent_datetimes is None:
                self.irise_equivalent_datetimes: list[datetime.datetime] = [self._equivalent_datetime(target_datetime) for target_datetime in target_datetimes]
            return [c/1000 for c in self.irise_df.loc[self.irise_equivalent_datetimes].value.tolist()]
        
    def __str__(self) -> str:
        """Return a description of the appliance.

        :return: a text description
        :rtype: str
        """
        return 'Appliance: ' + self.name


if __name__ == '__main__':
    irise_houses = IRISE(zipcode_pattern='38130')  # Echirolles: 2000908
    print(irise_houses)
    open_weather_map_json_reader = buildingenergy.openweather.WeatherJsonReader('grenoble1979-2022.json', from_stringdate='1/01/2020', to_stringdate='1/01/2021', altitude=117, albedo=0.1, pollution=0.1, location="Grenoble")
    site_weather_data: buildingenergy.openweather.SiteWeatherData = open_weather_map_json_reader.site_weather_data
    target_datetimes: list[datetime.datetime] = site_weather_data.get('datetime')
    
    house: House = irise_houses.get_house(2000908)

    house_consumptions_in_kWh: list[float] = house.consumptions()
    house_year_consumptions_in_kWh: float = sum(house_consumptions_in_kWh)
    print('house total consumptions in kWh:', house_year_consumptions_in_kWh)
    
    ground_surface_in_m2: float = 9  # in m2
    peak_power_in_kW: float = 0.400
    PV_efficiency = 0.2
    panel_height_in_m: float = 1.2  # in m
    PV_inverter_efficiency: float = 0.95 
    temperature_coefficient: float = 0.0035
    array_width: float = 3  # in m
    exposure_in_deg=0
    slope_in_deg=45
    distance_between_arrays_in_m = .8
    skyline_azimuths_altitudes_in_deg = ([(-180.0,13.8), (-170.0,18.9), (-145.1,9.8), (-120.0,18.3), (-96.1,17.3), (-60.8,6.2), (-14.0,2.6), (-8.4,5.6), (0.8,2.6), (21.6,5.5), (38.1,14.6), (49.4,8.9), (60.1,11.3), (87.4,10.4), (99.3,12.0), (142.1,2.6), (157.8,4.0), (175.1,17.1), (180.0,15.9)])
    mount_type: MOUNT_TYPE = MOUNT_TYPE.SAW

    solar_model = buildingenergy.solar.SolarModel(site_weather_data) 

    pv_system: buildingenergy.solar.PVsystem = buildingenergy.solar.PVsystem(solar_model, surfacePV_in_m2=ground_surface_in_m2, array_width=array_width, panel_height_in_m=panel_height_in_m, efficiency=PV_efficiency*PV_inverter_efficiency, skyline_azimuths_altitudes_in_deg=None, temperature_coefficient=temperature_coefficient)
    print('Number of arrays:', pv_system.number_of_arrays(distance_between_arrays_in_m, mount_type))
    pv_productions_in_kWh: list[float] = pv_system.productions_in_kWh(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, mount_type)
    print('PV production without skyline %.0fkWh' % sum(pv_productions_in_kWh))
    pv_year_production_in_kWh = sum(pv_productions_in_kWh)
    
    print("total production in kWh:", pv_year_production_in_kWh)
    print('number of arrays:', pv_system.number_of_arrays(distance_between_arrays_in_m, mount_type))
    print('PV surface in m2:', pv_system.pv_surface_in_m2(distance_between_arrays_in_m))
    print('surface for year neutrality:', pv_system.pv_surface_in_m2(distance_between_arrays_in_m) / pv_year_production_in_kWh * house_year_consumptions_in_kWh)

    print('self-consumption in %:', 100 * ecommunity.indicators.self_consumption(house_consumptions_in_kWh, pv_productions_in_kWh))
    print('self-sufficiency in %:', 100 * ecommunity.indicators.self_sufficiency(house_consumptions_in_kWh, pv_productions_in_kWh))
    print('dependency in %:', 100 * ecommunity.indicators.dependency(house_consumptions_in_kWh, pv_productions_in_kWh))
    print('autonomy in %:', 100 * ecommunity.indicators.year_autonomy(house_consumptions_in_kWh, pv_productions_in_kWh))
    print('NEEG per day in kWh:', ecommunity.indicators.NEEG_per_day(house_consumptions_in_kWh, pv_productions_in_kWh))
    grid_injection_tariff_kWh = .1313
    grid_drawn_tariff_kWh = .2062
    year_cost = ecommunity.indicators.cost(house_consumptions_in_kWh, pv_productions_in_kWh, grid_injection_tariff_kWh, grid_drawn_tariff_kWh)
    print('year cost:', year_cost)
    number_of_panels = round(pv_system.number_of_arrays(distance_between_arrays_in_m, mount_type) * panel_height_in_m / 1.096)
    
    expense: float = 160 * number_of_panels - number_of_panels * peak_power_in_kW * 370
    print('ROI:')
    for y in range(15):
        expense += house_year_consumptions_in_kWh * grid_drawn_tariff_kWh - ecommunity.indicators.cost(house_consumptions_in_kWh, pv_productions_in_kWh, grid_injection_tariff_kWh, grid_drawn_tariff_kWh)
        print('year', y+1, '>', expense, 'euros')
        if expense > 0:
            break
        
    
    
    # https://www.oscaro-power.com/panneau-solaire/975-panneau-solaire-perfomance-trina-solar-400-wc-cadre-noir-et-fond-blanc.html (400Wc)
    # https://www.les-energies-renouvelables.eu/conseils/photovoltaique/tarif-rachat-electricite-photovoltaique/
    # prime de 370 € /kwc (soit 3330 € pour 9 kwc) + vente à 13,13 c€/kWh)
    # https://www.fournisseurs-electricite.com/edf/tarifs/base
    #
    #  subscription: 179.36€/an + 0.2062€/kWh
    
    plt.figure()
    plt.plot(target_datetimes, house.consumptions(target_datetimes))   
    plt.show()