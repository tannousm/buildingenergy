"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

A helper module dedicated to the design of time-varying state space model approximated by bilinear state space model.

Author: stephane.ploix@grenoble-inp.fr
"""
from __future__ import annotations
import math
import numpy
import buildingenergy.parameters
import buildingenergy.building
import buildingenergy.thermal
import buildingenergy.statemodel
import buildingenergy.data
from joblib import Parallel, delayed


class VaryingStateModel:
    """
    This class gets the state space equation generated from the RC graph model and physics by modules buildings and physics and prepare it for use: generating and discrete time (1 hour) recurrent non-linear state model, identifying and organizing variables by type: inputs or outputs, but also by kind: temperature, heat gain, CO2 concentration, CO2 production for each zone, depending on the information provided by the designer as for example whether a zone has been declared simulated of not.
    """
    def __init__(self, data_provider: buildingenergy.data.DataProvider, building: buildingenergy.building, verbose: bool = False) -> None:
        self.dp: buildingenergy.data.DataProvider = data_provider
        self.building: buildingenergy.building = building
        # self.airflows_names = [airflow.name for airflow in self.building.airflows]
        # self.verbose = verbose
        # if verbose:
        #     print('State model depends on airflows: ' + ', '.join([airflow for airflow in self.airflows_names]))
        self.state_models_cache: dict[int, buildingenergy.statemodel.StateModel] = {}
        
    def cache_state_models(self):
        delayed_calls = list()
        for k in range(len(self.dp)):
            self.dp.set_k(k)
            fingerprint: list[int] = self.dp.fingerprint()
            if fingerprint not in self.state_models_cache:
                delayed_calls.append(delayed(self.building.make_state_model)({airflow.name: self.dp(airflow.name) for airflow in self.airflows}, fingerprint=fingerprint))
                if self.verbose:
                    print('*', end='')
                self.state_models_cache[fingerprint] = None
        results_delayed = Parallel(n_jobs=-1)(delayed_calls)
        for state_model in results_delayed:
            self.state_models_cache[state_model.fingerprint] = state_model

    # def __factors(self) -> list[dict[tuple[str, str], float], dict[tuple[str, str], float], dict[tuple[str], float]]:
    #     side_Rfactors: dict[tuple[str, str], float] = dict()
    #     side_Cfactors: dict[tuple[str, str], float] = dict()
    #     zone_Vfactors: dict[tuple[str], float] = dict()
    #     for zone_names in self.bindings.Rfactors():
    #         side_Rfactors[zone_names] = self.data_provider(self.bindings.Rfactor(*zone_names))
    #     for zone_names in self.bindings.Cfactors():
    #         side_Cfactors[zone_names] = self.data_provider(self.bindings.Cfactor(*zone_names))
    #     for zone_name in self.bindings.Vfactors():
    #         zone_Vfactors[zone_name] = self.data_provider(self.bindings.zone_parameters(zone_name))
    #     return side_Rfactors, side_Cfactors, zone_Vfactors
    
    # def state_model(self, k: int = 0) -> tuple[dict[str, list[float]], buildingenergy.statemodel.StateModel]:
    #     fingerprint: list[int] = self.data_provider.fingerprint(k) 
    #     if len(self.state_models_cache) > 0 and (fingerprint in self.state_models_cache):
    #         if self.verbose:
    #             print('.', end='')
    #         return self.state_models_cache[fingerprint]

    #     airflows_values: dict[str, float] = {airflow_name: self.data_provider(airflow_name, k, False) for airflow_name in self.airflows_names}
    #     # connected_zones_parameters = self.bindings.connected_zones_parameters()
    #     statemodel: buildingenergy.statemodel.StateModel = self.building.make_state_model(airflows_values, side_Rfactors=self.bindings.Rfactors(), side_Cfactors=side_Cfactors, zone_Vfactors=zone_Vfactors, recenter_on_factors=False)
    #     self.state_models_cache[fingerprint] = statemodel
    #     if self.verbose:
    #         print('*', end='')
    #     return statemodel
            
    # def simulate(self):
    #     side_Rfactors, side_Cfactors, zone_Vfactors = self.__factors()
    #     nominal_statemodel = self.building.make_state_model(airflow_values=dict(), side_Rfactors=side_Rfactors, side_Cfactors=side_Cfactors, zone_Vfactors=zone_Vfactors, recenter_on_factors=True)
    
        # if self.verbose:
        #     print('*', end='')
        # self.state_models_cache.clear()

    def simulate(self):

        simulated_outputs: dict[str, list[float]] = {variable_name: list() for variable_name in self.output_names}
        X = None
        # output_names: list[str] = self.output_names
        # simulated_outputs = {name: list() for name in output_names}
        for k in range(len(self.dp)):
            self.dp.set_k(k)
            current_input_values: dict[str, float] = {input_name: self.dp(input_name) for input_name in self.input_names}
            airflows_values: dict[str, float] = {airflow_name: self.dp(airflow_name) for airflow_name in self.airflows_names}
            if X is None:
                X: numpy.matrix = self.initialize(**current_input_values)
                
            self.set_state(X)
            [simulated_outputs[output_names[i]].append(val) for i, val in enumerate(self.output(**current_input_values))]
            X = self.step(**current_input_values)
        for output_name in output_names:
            self.dp.variable_set.add_external_variable(output_name+'_SIM', simulated_outputs[output_name])

        airflows_values: dict[str, float] = {airflow_name: self.dp(airflow_name, k=0) for airflow_name in self.airflows_names}
        delayed_calls = list()
        for k in range(len(self.dp)):
            fingerprint: list[int] = self.dp.fingerprint(k, self.airflows_names)
            if fingerprint not in self.state_models_cache:
                delayed_calls.append(delayed(self.building.make_state_model)(airflows_values, side_Rfactors=side_Rfactors, side_Cfactors=side_Cfactors, zone_Vfactors=zone_Vfactors, recenter_on_factors=False, fingerprint=fingerprint))
                if self.verbose:
                    print('*', end='')
                self.state_models_cache[fingerprint] = None
        results_delayed = Parallel(n_jobs=-1)(delayed_calls)
        for state_model in results_delayed:
            self.state_models_cache[state_model.fingerprint] = nominal_statemodel
        X = None
        state_model = self.state_model(0)
        # simulated_outputs: dict[str,list[float]] = {variable_name: list() for variable_name in state_model.output_names}
        simulated_outputs: dict[str,list[float]] = {variable_name: list() for variable_name in state_model.output_names}
        for k in range(len(self.dp)):
            current_input_values: dict[str, float] = {input_name: self.dp(input_name,k) for input_name in state_model.input_names}
            if X is None:
                X: numpy.matrix = state_model.initialize(**current_input_values)
            state_model.set_state(X)
            [simulated_outputs[state_model.output_names[i]].append(val) for i, val in enumerate(state_model.output(**current_input_values))]
            X = state_model.step(**current_input_values)
            if k > 0:
                state_model = self.state_model(k)
        return simulated_outputs
    
    def __str__(self) -> str:
        string: str = 'Model with following variables:\n'
        string += 'regular inputs: \n-%s resp. bounded on %s\n' % (','.join(self.regular_input_names), ','.join([self.bindings[name] for name in self.regular_input_names]))
        string += 'air flows: \n-%s resp. bounded on %s\n' % (','.join([self.bindings[name] for name in self.airflow_names]), ','.join(self.airflow_names))
        string += 'regular outputs:  \n-%s resp. bounded on %s\n' % (','.join(self.regular_output_names), ','.join([self.bindings[name] for name in self.regular_output_names]))
        return string


class Preference:
    """Provide de a model of the occupants'preferences. It deals with thermal comfort, air quality, number of home configuration changes, energy cost, icone,..."""

    def __init__(self, preferred_temperatures=(21, 23), extreme_temperatures=(18, 26), preferred_CO2_concentration=(500, 1500), temperature_weight_wrt_CO2: float = 0.5, power_weight_wrt_comfort: float = 0.5e-3):
        """Definition of comfort regarding  number of required actions by occupants, temperature and CO2 concentration, but also weights between cost and comfort, and between thermal and air quality comfort.

        :param preferred_temperatures: preferred temperature range, defaults to (21, 23)
        :type preferred_temperatures: tuple, optional
        :param extreme_temperatures: limits of acceptable temperatures, defaults to (18, 26)
        :type extreme_temperatures: tuple, optional
        :param preferred_CO2_concentration: preferred CO2 concentration range, defaults to (500, 1500)
        :type preferred_CO2_concentration: tuple, optional
        :param temperature_weight_wrt_CO2: relative importance of thermal comfort wrt air quality (1 means only temperature is considered), defaults to 0.5
        :type temperature_weight_wrt_CO2: float, optional
        :param power_weight_wrt_comfort: relative importance of energy cost wrt comfort (1 means only energy cost is considered), defaults to 0.5e-3
        :type power_weight_wrt_comfort: float, optional
        """
        self.preferred_temperatures = preferred_temperatures
        self.extreme_temperatures = extreme_temperatures
        self.preferred_CO2_concentration = preferred_CO2_concentration
        self.temperature_weight_wrt_CO2 = temperature_weight_wrt_CO2
        self.power_weight_wrt_comfort = power_weight_wrt_comfort

    def change_dissatisfaction(self, occupancy, action_set):
        """Compute the ratio of the number of hours where occupants have to change their home configuration divided by the number of hours with presence.

        :param occupancy: a vector of occupancies
        :type occupancy: list[float]
        :param action_set: different vectors of actions
        :type action_set: tuple[list[float]]
        :return: the number of hours where occupants have to change their home configuration divided by the number of hours with presence
        :rtype: float
        """
        number_of_changes = 0
        number_of_presences = 0
        previous_actions = [actions[0] for actions in action_set]
        for k in range(len(occupancy)):
            if occupancy[k] > 0:
                number_of_presences += 1
                for i in range(len(action_set)):
                    actions = action_set[i]
                    if actions[k] != previous_actions[i]:
                        number_of_changes += 1
                        previous_actions[i] = actions[k]
        return number_of_changes / number_of_presences if number_of_presences > 0 else 0

    def thermal_comfort_dissatisfaction(self, temperatures, occupancies):
        """Compute average dissatisfaction regarding thermal comfort: 0 means perfect and greater than 1 means not acceptable. Note that thermal comfort is only taken into account if occupancy > 0, i.e. in case of presence.

        :param temperatures: vector of temperatures
        :type temperatures: list[float]
        :param occupancies: vector of occupancies (number of people per time slot)
        :type occupancies: list[float]
        :return: average dissatisfaction regarding thermal comfort
        :rtype: float
        """
        if type(temperatures) is not list:
            temperatures = [temperatures]
            occupancies = [occupancies]
        dissatisfaction = 0
        for i in range(len(temperatures)):
            if occupancies[i] != 0:
                if temperatures[i] < self.preferred_temperatures[0]:
                    dissatisfaction += (self.preferred_temperatures[0] - temperatures[i]) / (self.preferred_temperatures[0] - self.extreme_temperatures[0])
                elif temperatures[i] > self.preferred_temperatures[1]:
                    dissatisfaction += (temperatures[i] - self.preferred_temperatures[1]) / (self.extreme_temperatures[1] - self.preferred_temperatures[1])
        return dissatisfaction / len(temperatures)

    def air_quality_dissatisfaction(self, CO2_concentrations, occupancies):
        """Compute average dissatisfaction regarding air quality comfort: 0 means perfect and greater than 1 means not acceptable. Note that air quality comfort is only taken into account if occupancy > 0, i.e. in case of presence.

        :param CO2_concentrations: vector of CO2 concentrations
        :type CO2_concentrations: list[float]
        :param occupancies: vector of occupancies (number of people per time slot)
        :type occupancies: list[float]
        :return: average dissatisfaction regarding air quality comfort
        :rtype: float
        """
        if type(CO2_concentrations) is not list:
            CO2_concentrations = [CO2_concentrations]
            occupancies = [occupancies]
        dissatisfaction = 0
        for i in range(len(CO2_concentrations)):
            if occupancies[i] != 0:
                dissatisfaction += max(0., (CO2_concentrations[i] - self.preferred_CO2_concentration[0]) /
                                       (self.preferred_CO2_concentration[1] - self.preferred_CO2_concentration[0]))
        return dissatisfaction / len(CO2_concentrations)

    def comfort_dissatisfaction(self, temperatures, CO2_concentrations, occupancies):
        """Compute the comfort weighted dissatisfaction that combines thermal and air quality dissatisfactions: it uses the thermal_dissatisfaction and air_quality_dissatisfaction methods.

        :param temperatures: list of temperatures
        :type temperatures: list[float]
        :param CO2_concentrations: list of CO2 concentrations
        :type CO2_concentrations: list[float]
        :param occupancies: list of occupancies
        :type occupancies: list[float]
        :return: the global comfort dissatisfaction
        :rtype: float
        """
        return self.temperature_weight_wrt_CO2 * self.thermal_comfort_dissatisfaction(temperatures, occupancies) + (1 - self.temperature_weight_wrt_CO2) * self.air_quality_dissatisfaction(CO2_concentrations, occupancies)

    def cost(self, Pheat, kWh_price=.13) -> float:
        """Compute the heating cost.

        :param Pheat: list of heating power consumptions
        :type Pheat: list[float]
        :param kWh_price: tariff per kWh, defaults to .13
        :type kWh_price: float, optional
        :return: energy cost
        :rtype: float
        """
        if type(Pheat) is not list:
            Pheat = [Pheat]
        return sum(Pheat) / 1000 * kWh_price

    def icone(self, CO2_concentration, occupancy) -> float:
        """Compute the ICONE indicator dealing with confinement regarding air quality.

        :param CO2_concentration: list of CO2 concentrations
        :type CO2_concentration: list[float]
        :param occupancy: list of occupancies
        :type occupancy: list[float]
        :return: value between 0 and 5
        :rtype: float
        """
        n_presence = 0
        n1_medium_containment = 0
        n2_high_containment = 0
        for k in range(len(occupancy)):
            if occupancy[k] > 0:
                n_presence += 1
                if 1000 <= CO2_concentration[k] < 1700:
                    n1_medium_containment += 1
                elif CO2_concentration[k] >= 1700:
                    n2_high_containment += 1
        f1 = n1_medium_containment / n_presence if n_presence > 0 else 0
        f2 = n2_high_containment / n_presence if n_presence > 0 else 0
        return 8.3 * math.log10(1 + f1 + 3 * f2)

    def assess(self, Pheater, temperatures, CO2_concentrations, occupancies) -> float:
        """Compute the global objective to minimize including both comforts and energy cost for heating.

        :param Pheater: list of heating powers
        :type Pheater: list[float]
        :param temperatures: list of temperatures
        :type temperatures: list[float]
        :param CO2_concentrations: list of CO2 concentrations
        :type CO2_concentrations: list[float]
        :param occupancies: list of occupancies
        :type occupancies: list[float]
        :return: objective value
        :rtype: float
        """
        return self.cost(Pheater) * self.power_weight_wrt_comfort + (1 - self.power_weight_wrt_comfort) * self.comfort_dissatisfaction(temperatures, CO2_concentrations, occupancies)

    def print_assessment(self, Pheat, temperatures, CO2_concentrations, occupancies, action_sets):
        """Print different indicators to appreciate the impact of a series of actions.

        :param Pheat: list of heating powers
        :type Pheat: list[float]
        :param temperatures: list of temperatures
        :type temperatures: list[float]
        :param CO2_concentrations: list of CO2 concentrations
        :type CO2_concentrations: list[float]
        :param occupancies: list of occupancies
        :type occupancies: list[float]
        :param actions: list of actions
        :type actions: tuple[list[float]]
        """
        print('- global objective: %s' % self.assess(Pheat, temperatures, CO2_concentrations, occupancies))
        print('- average thermal dissatisfaction: %.2f%%' % (self.thermal_comfort_dissatisfaction(temperatures, occupancies) * 100))
        print('- average CO2 dissatisfaction: %.2f%%' % (self.air_quality_dissatisfaction(CO2_concentrations, occupancies) * 100))
        print('- ICONE: %.2f' % (self.icone(CO2_concentrations, occupancies)))
        print('- average comfort dissatisfaction: %.2f%%' % (self.comfort_dissatisfaction(temperatures, CO2_concentrations, occupancies) * 100))
        print('- change dissatisfaction (number of changes / number of time slots with presence): %.2f%%' % (self.change_dissatisfaction(occupancies, action_sets) * 100))
        print('- heating cost: %s' % self.cost(Pheat))

        temperatures_when_presence = list()
        CO2_concentrations_when_presence = list()
        for i in range(len(occupancies)):
            if occupancies[i] > 0:
                temperatures_when_presence.append(temperatures[i])
                CO2_concentrations_when_presence.append(CO2_concentrations[i])
        if len(temperatures_when_presence) > 0:
            temperatures_when_presence.sort()
            CO2_concentrations_when_presence.sort()
            office_temperatures_estimated_presence_lowest = temperatures_when_presence[:math.ceil(len(temperatures_when_presence) * 0.1)]
            office_temperatures_estimated_presence_highest = temperatures_when_presence[math.floor(len(temperatures_when_presence) * 0.9):]
            office_co2_concentrations_estimated_presence_lowest = CO2_concentrations_when_presence[:math.ceil(len(CO2_concentrations_when_presence) * 0.1)]
            office_co2_concentrations_estimated_presence_highest = CO2_concentrations_when_presence[math.floor(len(CO2_concentrations_when_presence) * 0.9):]
            print('- average temperature during presence:', sum(temperatures_when_presence) / len(temperatures_when_presence))
            print('- average 10% lowest temperature during presence:', sum(office_temperatures_estimated_presence_lowest) / len(office_temperatures_estimated_presence_lowest))
            print('- average 10% highest temperature during presence:', sum(office_temperatures_estimated_presence_highest) / len(office_temperatures_estimated_presence_highest))
            print('- average CO2 concentration during presence:', sum(CO2_concentrations_when_presence) / len(CO2_concentrations_when_presence))
            print('- average 10% lowest CO2 concentration during presence:', sum(office_co2_concentrations_estimated_presence_lowest) / len(office_co2_concentrations_estimated_presence_lowest))
            print('- average 10% highest CO2 concentration during presence:',
                  sum(office_co2_concentrations_estimated_presence_highest) / len(office_co2_concentrations_estimated_presence_highest))

    def __str__(self):
        """Return a description of the defined preferences.

        :return: a descriptive string of characters.
        :rtype: str
        """
        string = 'preference: temperature in %f<%f-%f>%f, concentrationCO2 %f>%f\n' % (
            self.extreme_temperatures[0], self.preferred_temperatures[0], self.preferred_temperatures[1], self.extreme_temperatures[1], self.preferred_CO2_concentration[0], self.preferred_CO2_concentration[1])
        string += '%.3f * cost + %.3f disT + %.3f disCO2' % (self.power_weight_wrt_comfort, (1-self.power_weight_wrt_comfort) * self.temperature_weight_wrt_CO2, (1-self.power_weight_wrt_comfort) * (1-self.temperature_weight_wrt_CO2))
        return string
