from __future__ import annotations
import configparser
import math
import copy
import plotly.graph_objects as go
import numpy
import time
import prettytable
import configparser
import sys, os, os.path, shutil
import matplotlib.pyplot as plt
import matplotlib.image as mplimg
import buildingenergy.solar
from datetime import datetime
from matplotlib import cm
from matplotlib.ticker import PercentFormatter
from windrose import WindAxes, WindroseAxes
from buildingenergy.openweather import WeatherJsonReader
from buildingenergy.solar import SolarModel, SkyLineMask
from buildingenergy.thermics import Composition
import buildingenergy.physics
from buildingenergy.timemg import datetime_to_stringdate
import ecommunity.indicators


class  Configuration:

    @staticmethod
    def setup(*references):
        _setup = configparser.ConfigParser()
        _setup.read('setup.ini')
        configparser_obj = _setup
        for ref in references:
            configparser_obj = configparser_obj[ref]
        return configparser_obj

    def __init__(self, weather_file: str, weather_year: int, location: str, albedo: float, pollution: float, latitude: float, longitude: float, skyline: list[tuple[float, float]]) -> None:
        
        self._names: list[str] = list()
        self._parameters: dict = dict()
        self._temporary_parameters: dict = dict()
        self._parametrics: dict = dict()
        self._current_parametric = None
        
        weather_json_reader = WeatherJsonReader(weather_file, from_stringdate='1/1/%i' % weather_year, to_stringdate='31/12/%i' % weather_year, albedo=albedo, pollution=pollution, location=location, latitude=latitude, longitude=longitude, skyline=skyline)
        self.site_weather_data = weather_json_reader.site_weather_data
        
        self.section('site')
        self(weather_file_name=weather_file)
        self(location=location)
        self(latitude=latitude)
        self(longitude=longitude)
        self(weather_year=weather_year)
        self(albedo=albedo)
        self(pollution=pollution)
        self(skyline=self.site_weather_data.skyline)

    def section(self, name: str) -> None:
        self._names.append(name)

    def signature(self) -> int: 
        _signature = ''
        for parameter in self._parameters:
            _signature += str(self(parameter))
        return hash(_signature)

    def clone(self) -> 'Configuration':
        config_copy = Configuration(self('weather_file_name'), self('weather_year'), self('albedo'), self('pollution'), self('location'), self('latitude'), self('longitude'), self('skyline'))
        config_copy._names = copy.deepcopy(self._names)
        config_copy._parameters = copy.deepcopy(self._parameters)
        config_copy._temporary_parameters = copy.deepcopy(self._temporary_parameters)
        config_copy._parametrics = copy.deepcopy(self._parametrics)
        return config_copy

    def __call__(self, parameter_name=None, **parameter_values) -> None | float | dict[str, float] | list[tuple[str, float]]:
        if parameter_name is not None:
            if parameter_name in self._temporary_parameters:
                return self._temporary_parameters[parameter_name]
            else:
                return self._parameters[parameter_name]
        else:
            if len(parameter_values) == 1 or (len(parameter_values) == 2 and 'parametric' in parameter_values and type(parameter_values['parametric']) is list):
                _parameter_name = ''
                parametric = list()
                for parameter_name in parameter_values:
                    if parameter_name == 'parametric':
                        parametric: list = parameter_values['parametric']
                    else:
                        _parameter_name: str = parameter_name
                        self._names.append(parameter_name)
                        self._parameters[parameter_name] = parameter_values[parameter_name]
                if len(parameter_values) == 2:
                    if parameter_values[_parameter_name] not in parameter_values['parametric']:
                        if (type(parameter_values[_parameter_name]) is float or type(parameter_values[_parameter_name]) is int):
                            parametric.append(parameter_values[_parameter_name])
                            parametric.sort()
                        elif type(parameter_values[_parameter_name]) is dict:
                            for key in parameter_values[_parameter_name]:
                                if parameter_values[_parameter_name][key] not in parametric:
                                    parametric.append(parameter_values[_parameter_name][key])
                                parametric.sort()
                    self._parametrics[_parameter_name] = parametric
                    
    def __contains__(self, parameter_name):
        return parameter_name in self._parameters

    def __str__(self) -> str:
        string: str = ''
        for name in self._names:
            if name not in self._parameters:
                string += '[ ] %s' % name
            else:
                string += '- %s = ' % (name) + str(self._parameters[name])
                if name in self._parametrics:
                    string += ' [' + ','.join([str(v) for v in self._parametrics[name]]) + ']'
            string += '\n'
        return string + '\n'

    def reset(self, name: str):
        if name in self._temporary_parameters:
            del self._temporary_parameters[name]

    def set(self, name: str, value, key=None):
        if name not in self._parameters:
            raise ValueError('Unknown parameter: %s' % name)
        if key is None:
            self._temporary_parameters[name] = value
        else:
            self._temporary_parameters[name] = copy.deepcopy(self._parameters[name])
            self._temporary_parameters[name][key] = value

    def parametric(self, name):
        if name not in self._parametrics:
            raise ValueError('%s has no specified parametric')
        self._current_parametric = name
        return self._parametrics[name]

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n < len(self._parametrics[self._current_parametric]):
            value = self._parametrics[self._current_parametric][self.n]
            self.n += 1
            return value
        else:
            self.reset(self._current_parametric)
            self._current_parametric = None
            raise StopIteration

plot_size: tuple[int, int] = (int(Configuration.setup('sizes', 'width')), int(Configuration.setup('sizes', 'height')))


def value_sort(datetimes, values):
    """sort the time series defined by the lists datetimes and values according to the values, with descending order.

    :param datetimes: times corresponding to values
    :type datetimes: list[datetime]
    :param values: values to be sorter
    :type values: list[float]
    :return: both input series sorted
    :rtype: list[datetime], list[float]
    """
    values_array = numpy.array(values)
    months_array = numpy.array([datetimes[i].month for i in range(len(datetimes))])
    indices = (-values_array).argsort()
    sorted_values_array = values_array[indices]
    sorted_months_array = months_array[indices]
    return sorted_months_array.tolist(), sorted_values_array.tolist()


class Averager:
    
    def __init__(self, values: list[float]) -> None:
        self._values: list[float] = values
        self._avg_values: list[float] = list()
        self.kind = None
        self._period = None
        
    def sliding(self, samples: int, decreasing_weight: bool = False):
        if self.kind is not None:
            raise ValueError('A same averager can be used only ones')
        for i in range(len(self._values)):
            i_min, i_max = max(0, i - samples), i
            if i_max > i_min:
                self._avg_values.append(sum(self._values[i_min:i_max]) / (i_max - i_min))
            else:
                self._avg_values.append(self._values[i])
        return self._avg_values
    
    def in_period(self, datetimes: list[datetime], month: bool = False, sumup: bool = False):
        if self.kind is not None:
            raise ValueError('A same averager can be used only ones')
        current_period: int = -1
        accumulator: list[float] = list()
        self._periods: list[int] = list()
        self._avg_values: list[float] = list()
        for k, dt in enumerate(datetimes):
            if current_period == -1:  # initialization
                current_period = dt.day if not month else dt.month
                accumulator.append(self._values[k])
            else:
                if (not month and dt.day == current_period) or (month and dt.month == current_period):
                    accumulator.append(self._values[k])  # accumulating
                else: 
                    if sumup:
                        avg: float = sum(accumulator) 
                    else:
                        avg: float = sum(accumulator) / len(accumulator)
                    self._periods.extend([current_period for _ in range(len(accumulator))])
                    self._avg_values.extend([avg for _ in range(len(accumulator))])
                    current_period = dt.day if not month else dt.month
                    accumulator = [self._values[k]]
        if sumup:
            avg: float = sum(accumulator)
        else:
            avg: float = sum(accumulator) / len(accumulator)
        self._periods.extend([current_period for _ in range(len(accumulator))])
        self._avg_values.extend([avg for _ in range(len(accumulator))])
        return self._avg_values
    
    @property
    def avg_values(self):
        return self._avg_values

    @property
    def periods(self):
        return self._periods

def to_markdown_table(pretty_table):
    """
    Print a pretty table as a markdown table
    
    :param py:obj:`prettytable.PrettyTable` pt: a pretty table object.  Any customization
      beyond int and float style may have unexpected effects
    
    :rtype: str
    :returns: A string that adheres to git markdown table rules
    """
    _junc = pretty_table.junction_char
    if _junc != "|":
        pretty_table.junction_char = "|"
    markdown = [row[1:-1] for row in pretty_table.get_string().split("\n")[1:-1]]
    pretty_table.junction_char = _junc
    return "\n".join(markdown)


class DataPlot:
        
    def __init__(self, variable: str):
        self.indicators: list[str] = list()
        self.setids: list[int] = list()
        self.variable: str = variable
        self.indicators_colors: list[str] = ['black', 'blue', 'cyan', 'orange', 'red', 'green', 'yellow']
        self.values = list()
        self.group_indicator_data = dict()
        self.setid_group_indicators = dict()
        
    def set_parametric_values(self, values):
        self.values = values

    def add(self, indicator: str, group: str, setid: int, data: list[float]):
        if group not in self.group_indicator_data:
            self.group_indicator_data[group]=dict()
            self.setid_group_indicators[group]=dict()
        if setid not in self.setid_group_indicators[group]:
            self.setid_group_indicators[group][setid] = [indicator]
        elif indicator not in self.setid_group_indicators[group][setid]:
            self.setid_group_indicators[group][setid].append(indicator)
        if type(data) == list:
            self.group_indicator_data[group][setid][indicator].extend(data)
        else:
            if indicator not in self.group_indicator_data[group]:
                self.group_indicator_data[group][indicator] = list()
            self.group_indicator_data[group][indicator].append(data)
        if indicator not in self.indicators:
            self.indicators.append(indicator)
        if setid not in self.setids:
            self.setids.append(setid)

    def get_group_set_variables(self, group: str, setid: int) -> list[str]:
        result = list()
        for variable in self.group_indicator_data[group]:
            if variable in self.setid_group_indicators[group][setid]:
                result.append(variable)
        return result
    
    def get_group_variable_colors(self, group: str, setid: int) -> list[str]:
        colors = list()
        for variable in self.get_group_set_variables(group, setid):
            colors.append(self.indicators_colors[self.indicators.index(variable)%len(self.indicators_colors)])
        return colors
    
    def get_group_set_data(self, group: str, setid: int) -> list[list[float]]:
        data = list()
        for variable in self.get_group_set_variables(group,setid):
            data.append(self.group_indicator_data[group][variable])
        return data
        
    def get_groups(self) -> list[str]:
        return list(self.group_indicator_data.keys())
    
    def get_group_sets(self, group) -> int:
        return len(self.setid_group_indicators[group])
            
    def __sizeof__(self) -> int:
        return len(self.values)


class Experiment:
    """
    report maker.
    """

    def __init__(self, configuration, on_screen: bool=True) -> None:
        """Initialize the report maker

        :param configuration: configuration class
        :type configuration: Configuration
        """
        self.configuration: Configuration = configuration
        self.on_screen = on_screen
        self.figure_counter: int = 0
        if not on_screen:
            filename: str = Configuration.setup('folders', 'results') + "experiment.md"
            if os.path.exists(filename):
                os.remove(filename)
            figures_folder: str = Configuration.setup('folders', 'results') + Configuration.setup('folders', 'figures')
            if os.path.exists(figures_folder):
                shutil.rmtree(figures_folder, ignore_errors=True)
            os.mkdir(figures_folder)
            sys.stdout = open(filename, 'w')
        self.add_text('# Lambda-House Report for %s with file %s' % (configuration('location'), configuration('weather_file_name')))
        self.add_text('## Table of contents')
        self.add_text('1. [The principle](#principle)')
        self.add_text('2. [Local weather analysis](#climate)')
        self.add_text('3. [Long term climate evolution](#evolution)')
        self.add_text('4. [House analysis](#house)')
        self.add_text('5. [Neutrality analysis](#neutrality)')
        self.add_text('6. [Features of the lambda-house](#features)')
        self.add_text('________________________________________________________________________')
        
        self.add_text('## 1. The principle <a name="principle"></a>')
        self.add_image("lambda.png")
        
        self.add_text("Pre-design stage is characterized by a known location for the construction but little ideas about the building to design. Nevertheless, this is during this stage that strong direction are taken during the design. Because engineers do not have enough data to setup simulations, they use to intervene a little during this stage. For instance, it can show whether it is interesting or not to set large windows for each facade? or what the best direction for the building? Given an floor surface, is it more interesting to design a single floor building or a multiple floor one, keeping the useful floor surface same? etc...")

        self.add_text("The idea of the lambda house is to locate a standard house, with few customizations at first and to analyze its behavior regarding energy in order to point out the impact of possible choices on energy performances. By default, the lambda house is a $100m^2$ single floor square house equipped by an invertible heat pump and a dual flow ventilation system. If the indoor temperature is passing over a limit and an habitant is present, inhabitants will open the window to preserve their comfort.")
        
        self.add_text("The minimum requirement for setting up a lambda-house at a specific location has to be specified:")
        self.add_text("- choosing the year for the analysis (the weather file has to include the specified year)")
        self.add_text("- an openweathermap json historical bulk file, that can be obtained from [open weather map](https://home.openweathermap.org/history_bulks/new) or, weather data can alternatively by open from [open-meteo](https://open-meteo.com)")
        self.add_text("- the skyline at the investigated location that can obtained from [the global solar atlas website](https://globalsolaratlas.info) (go to open details) or, for a free registration, from [solargis](https://apps.solargis.com/prospect/map).")
        self.add_text("The other parameters of the lambda-house are given at the end of the report they can be modified to better match a given context.")
        self.figure_counter = 1
        
    def close(self):
        """Close the report and save it.
        """
        self.add_text('## 6. Features of the lambda-house <a name="features"></a>')
        self.add_text("The parameters below describe the house context. They can be modified to better match a given context:")
        
        self.add_text("### 'site' section")
        self.add_text("- weather_file_name: the name of the openweathermap history bulk json file (Celsius degrees")
        self.add_text("- location: the location name defining the dataset to be considered in the openweathermap file")
        self.add_text("- weather_year: the selected year for the analyses")
        self.add_text("- albedo: the albedo characterizing the ground surface around the house")
        self.add_text("- skyline: a list of coordinates sorted according to increasing azimuths from -180° to 180° describing the skyline. A point is defined by a couple (azimuth, altitude) in trigonometric degrees, where the azimuth is defined wrt to the south (0°=south, -90°=west, 90°=east and 180°=north) i.e. 180° has to be removed from the azimuth given by the solar global atlas website, whose azimuth is related to the north). The altitude is the angle between the horizontal directed to the south and the position of the sun (0°=horizontal, 90°=vertical")
        
        self.add_text("### 'house' section")
        self.add_text("- total_living_surface: the useful surface of the house. If the shape of the building is changing (number of floor, larger wall length on a side,...), this value is kept constant. It's the sum of the floor surfaces")
        self.add_text("- height_per_floor: height of a floor in meter")
        self.add_text("- shape_factor: a ratio of the south wall length and the east wall length. The south wall length is equal to $\\sqrt{\\text{total\\_living\\_surface}}\\times\\text{shape\\_factor}$")
        self.add_text("- number_of_floors: number of floors")
        self.add_text("- wall_composition_in_out: composition of plain walls between indoor and outdoor. Each layer is defined by a material and a thickness")
        self.add_text("- roof_composition_in_out: composition of the flat roof between indoor and outdoor. Each layer is defined by a material and a thickness")
        self.add_text("- glass_composition_in_out: composition of the glazing between indoor and outdoor. Each layer is defined by a material and a thickness")
        self.add_text("- ground_composition_in_out: composition of the slab between indoor and the ground. Each layer is defined by a material and a thickness")
        self.add_text("- polystyrene: material chosen for studying the influence of insulation: its thickness will be replaced by the specified value: it will replace the value appearing in the compositions")
        self.add_text("- offset_exposure: trigonometric rotation angle in degrees defining the direction of the south wall wrt the south (0° means south directed)")
        self.add_text("- glazing: it is the surface ratio of grazing for each wall of the house. 'north': 0.1 means 10% of external wall directed to the north is composed of double layer glazing (window).")
        self.add_text("- solar_factor: it is the ratio of solar energy passing through the windows. 85% (0.85) means that 85% of the collected solar energy is captured by the house indoor")
        self.add_text("- south_solar_protection_angle: it is an angle in degrees corresponding to the maximum altitude for which the is visible i.e. energy is passing through the glazing. Over this limit, the sun is hidden")
        
        self.add_text("### 'HVAC and photovoltaic (PV) system' section")
        self.add_text("- heating_setpoint: heating temperature setpoint used in case of presence")
        self.add_text("- delta_temperature_absence_mode: delta temperature setpoint used in case of absence")
        self.add_text("- cooling_setpoint: cooling temperature setpoint used in case of presence. In case of absence, the air conditioning system is stopped")
        self.add_text("- winter_hvac_trigger_temperature: thresholds used to determine the heating period. The first time the averaged outdoor temperature pass over this threshold determines the end of the heating period, and the last time it passed under, it corresponds to the start of the heating period")
        self.add_text("- summer_hvac_trigger_temperature: same idea than before but for the air conditioning: the first time the averaged outdoor temperature passes over this threshold determines the beginning the cooling period, and last time it passes down, the end.")
        self.add_text("- hvac_hour_delay_for_trigger: it determines the heating and the cooling periods both by smoothing the outdoor temperature with an average filter with an 2xhvac_hour_delay_for_trigger+1 horizon, and it defines the minimum length of the heating and cooling period in hours.")
        self.add_text("- inertia_level: inertia is modeled by an average filter: it defines the horizon of the average filter: 2xinertia_level+1. The higher, the more inertia.")
        self.add_text("- hvac_COP: coefficient of performance of HVAC system: Pelec = COP * Ptherm")
        self.add_text("- final_to_primary_energy_coefficient: conversion coefficient relating primary energy to the electricity coming from the grid")
        self.add_text("- air_renewal_presence: air renewal in house indoor volume per hour in case of inhabitant presence")
        self.add_text("- air_renewal_absence: air renewal in house indoor volume per hour in case of inhabitant absence")
        self.add_text("- ventilation_heat_recovery_efficiency: heat recovery performance of a dual flow ventilation system: 0=no heat recovery and 0.85=85% of the heat of the air removed from inside is recovered in the insuflated air")
        self.add_text('- PV_efficiency: coefficient modelling the conversion of solar power to electricity')
        
        self.add_text("### 'inhabitants' section")
        self.add_text('- occupancy_schema: it defines the occupation schema. For instance "(1, 2, 3, 4, 5): {(18,8): 3, (8, 18): 0}" means that for the days 1 to 5 (Monday to Friday), there are 3 occupants from 18:00 to 8:00, and no occupant the rest of the time')
        self.add_text('- average_occupancy_metabolic_gain: it represents the heat gain per occupant composed by its metabolism')
        self.add_text('- average_occupancy_electric_gain: it represents the heat gain per occupant composed by its extra consumption induced by its presence')
        self.add_text('- average_permanent_electric_gain: permanent heat coming from electric appliances: it\'s independent of the occupancy')
        self.add_text('- air_renewal_overheat_threshold: threshold temperature from which inhabitants will open the windows')
        self.add_text('- air_renewal_overheat: air renewal in vol/hour when inhabitants open the windows')
        
        self.add_text('For each parameter, a list of values defined in "parametric" can be specified. They are used for parametric studies.')
        self.add_text("This report has been generated for a lambda-house defined by:")
        self.add_text(self.configuration.__str__())
        sys.stdout.close()
        sys.stdout = sys.__stdout__
        
    def add_image(self, file_name: str):
        if not self.on_screen:
            self.add_text("![](../img/%s)" % file_name)
        else:
            image = mplimg.imread('./img/%s' % file_name)
            plt.imshow(image)
            plt.show()

    def add_text(self, text: str, on_screen_only:bool=False, on_mmd_only: bool=False):
        """Add a text line in the report

        :param text: text to be added
        :type text: str
        """
        if (self.on_screen and not on_mmd_only) or (not self.on_screen and not on_screen_only):
            print(str(text) + '\n')
            if not self.on_screen and not str(text).startswith('!'):
                print(str(text) + '\n', file=sys.stderr)
                
    def add_pretty_table(self, pretty_table: prettytable.PrettyTable, on_screen_only:bool=False, on_mmd_only: bool=False):
        if self.on_screen:
            self.add_text(str(pretty_table), on_screen_only, on_mmd_only)
        else:
            self.add_text(to_markdown_table(pretty_table), on_screen_only, on_mmd_only)

    def add_figure(self, fig = None, on_screen_only:bool=False, on_mmd_only: bool=False):
        """Add the last figure to the report

        :param figure_name: name of the figure used for saving, defaults to None
        :type figure_name: str, optional
        """
        if self.on_screen and not on_mmd_only:
            if fig is None:
                plt.show()
            else:
                fig.show()
        elif not self.on_screen and not on_screen_only:
            figure_name: str = Configuration.setup('folders', 'figures') + 'figure%i.png' % self.figure_counter
            self.figure_counter += 1
            if fig is None:
                plt.savefig(Configuration.setup('folders', 'results') + figure_name, dpi=600)
                plt.close()
            else:
                fig.write_image(Configuration.setup('folders', 'results') + figure_name, scale=2)
            self.add_text('![](%s)' % figure_name)

    def add_timeplot(self, main_data_name: str, datetimes: list, values: list, datetimemarks: list=[], valuemarks: list=[], **other_values):
        _, axis = plt.subplots(figsize=plot_size)
        axis.plot(datetimes, values, alpha=1)
        for serie_name in other_values:
            axis.plot(datetimes, other_values[serie_name], ':', alpha=0.5)
        min_value, max_value = None, None
        for i in range(len(values)):
            if values[i] is not None:
                if min_value is None:
                    min_value = values[i]
                    max_value = values[i]
                else:
                    min_value = min(min_value, values[i])
                    max_value = max(max_value, values[i])
        for datetimemark in datetimemarks:
            axis.plot([datetimemark, datetimemark], [min_value, max_value], 'r-.', alpha=0.5)
        for valuemark in valuemarks:
            axis.plot([datetimes[0], datetimes[-1]], [valuemark, valuemark], 'r-.', alpha=0.5)
        if len(other_values) > 0:
            legends = [main_data_name]
            legends.extend([value_name.replace('_', ' ') for value_name in other_values])
            axis.legend(legends)
            axis.grid()
        self.add_figure()

    def add_monotonic(self, title, datetimes, values: list, datetimemarks: list=[], valuemarks: list=[]):
        indices = [100*i/(len(values)-1) for i in range(len(values))]
        sorted_months, sorted_outdoor_temperatures = value_sort(datetimes, values)
        _, axis = plt.subplots(figsize=plot_size)
        axis.fill_between(indices, sorted_outdoor_temperatures, alpha=1)
        min_value, max_value = min(values), max(values)
        avg_value = (sum(values) / len(values))
        for datetimemark in datetimemarks:
            axis.plot([datetimemark, datetimemark], [min_value, max_value], 'r:')
        for valuemark in valuemarks:
            axis.plot([0, 100], [valuemark, valuemark], 'r:')
        axis.plot([0, 100], [avg_value, avg_value], 'b')
        axis.set_xlim(left=0, right=100)
        axis.grid(True)
        axis.set_xlabel('% of the year')
        axis.set_ylabel(title)
        for label in axis.get_xticklabels():
            label.set_visible(True)
        ax2 = axis.twinx()
        ax2.plot(indices, sorted_months,'.c')
        ax2.set_ylabel('month number')
        plt.tight_layout()
        self.add_figure()
        
    def add_windrose(self, wind_directions_deg: list, wind_speeds_m_s):
        speed_bins = 20
        wind_speeds_km_h = [speed * 3.6 for speed in wind_speeds_m_s]
        speed_counts, speed_bin_edges = numpy.histogram(wind_speeds_km_h, bins=speed_bins, range=(0,max(wind_speeds_km_h)))

        direction_bins = 16
        direction_counts, direction_bin_edges = numpy.histogram(wind_directions_deg, bins=direction_bins, range=(0,360))

        ax = WindroseAxes.from_ax()
        ax.contourf(direction=wind_directions_deg, var=wind_speeds_km_h, bins=speed_bins, normed=True, cmap=cm.hot)
        ax.contour(direction=wind_directions_deg, var=wind_speeds_km_h, bins=speed_bins, normed=True, colors='black', linewidth=.5)
        ax.set_legend()
        ax.set_xlabel('radius stands for number of occurrences')
        ax.set_title('windrose where color stands for wind speed in km/h')
        ax.yaxis.set_major_formatter(PercentFormatter(100))
        self.add_figure()

        _, ax = plt.subplots()
        ax.bar(speed_bin_edges[:-1], [count/len(wind_speeds_km_h) for count in speed_counts], width=0.8, align='center')
        ax.set_xlabel('wind speed in km/h')
        ax.set_ylabel('probability of wind speed')
        ax.yaxis.set_major_formatter(PercentFormatter(1))
        ax.grid()
        self.add_figure()
        
        ax = WindAxes.from_ax()
        ax.bar(direction_bin_edges[:-1], [count/len(wind_directions_deg) for count in direction_counts], width = 0.8*360/direction_bins)
        xlabels = ('N','','N-E','','E','','S-E','','S','','S-W','','W','','N-W','')
        xticks = numpy.arange(0, 360, 360/direction_bins)
        ax.set_xticks(xticks)
        ax.set_xticklabels(xlabels)
        ax.grid()
        ax.set_xlabel('wind directions (from where the wind is blowing)')
        ax.set_ylabel('probability')
        ax.yaxis.set_major_formatter(PercentFormatter(1))
        self.add_figure()
        
    def add_barchart(self, title: str, ylabel: str, **category_series_sets: dict):
        category_names: list[str] = list(category_series_sets.keys())
        fig, ax = plt.subplots(tight_layout=True, figsize=plot_size)
        
        if not isinstance(category_series_sets[category_names[0]], (int, float)):
            width = 1/len(category_names)
            bars = list()
            for series_position, series_set_name in enumerate(category_series_sets):
                series = category_series_sets[series_set_name]
                w = width / (len(series)-1)
                for i, series_name in enumerate(series):
                    bars.append(ax.bar(series_position - width + i * w, round(series[series_name],1), w, label=series_name))
            ax.set_xticks([p- width/2 for p in range(len(category_series_sets))], category_names)
            for bar in bars:
                ax.bar_label(bar, padding=3)
            ax.legend()     
        else:
            ax.bar(x=[i for i in range(len(category_names))], height=[round(category_series_sets[category_name],1) for category_name in category_series_sets], label=[category_name for category_name in category_series_sets])
            ax.set_xticks([p for p in range(len(category_series_sets))], category_names)
          
        ax.set_ylabel(ylabel)
        ax.set_title(title.replace('_', ' '))
        self.add_figure()

    def add_monthly_trend(self, title, datetimes, values):

        class YearMonthData:

            def __init__(self) -> None:
                self.month_data = dict()
                self.months = list()

            def append(self, datetime, value):
                month_name = datetime.strftime('%b')
                if month_name not in self.months:
                    self.month_data[month_name] = list()
                    self.months.append(month_name)
                self.month_data[month_name].append(value)

            def data(self):
                months_average = dict()
                for month in self.months:
                    if month in self.month_data:
                        try:
                            months_average[month] = sum(self.month_data[month]) / len(self.month_data[month])
                        except:
                            months_average[month] = None
                    # else:
                    #     months_average[month] = None
                return self.months, [months_average[month] for month in self.months]
                    # return self.months, [sum(self.month_data[month]) / len(self.month_data[month]) for month in self.months]

        year_monthly_values = dict()
        for i, datetime in enumerate(datetimes):
            if datetime.year not in year_monthly_values:
                year_monthly_values[datetime.year] = YearMonthData()
            year_monthly_values[datetime.year].append(datetime, values[i])

        colors = [f'rgb(%i,%i,%i)' %(255-i*255/(len(year_monthly_values) - 1), abs(128-i*255/(len(year_monthly_values)- 1)) , i*255/(len(year_monthly_values)- 1)) for i in range(len(year_monthly_values))]   # Get the colors

        fig = go.Figure() # create a figure
        for i, year in enumerate(year_monthly_values):  # Plot each year with a corresponding color
            months, values = year_monthly_values[year].data()
            fig.add_trace(go.Scatterpolar(r=values, theta=months, name=str(year), line_color=colors[i]))
        fig.update_layout(autosize=False, width=1000, height=800, title=title)  # Adjust the size of the figure
        self.add_figure(fig)
        

    def add_parametric(self, data_plot: DataPlot, plottype: str="energy"):
        groups = data_plot.get_groups() 
        nrows = math.ceil(math.sqrt(len(groups)))
        ncols = math.ceil(len(groups)/nrows)
        fig, axs = plt.subplots(nrows=nrows, ncols=ncols, tight_layout=True, figsize=plot_size)
        
        for i in range(len(groups)):
            irow = (i // ncols) 
            icol = (i % ncols) 
            
            group = groups[i]
            nsets = data_plot.get_group_sets(group)

            k=0
            if nrows > 1 and ncols > 1:
                axsi = axs[irow, icol]
            elif ncols > 1:
                axsi = axs[irow]
            else:
                axsi = axs

            axss = None 
            for nset in range(nsets):
                indicators = data_plot.get_group_set_variables(group, nset)
                data = data_plot.get_group_set_data(group, nset)

                colors = data_plot.get_group_variable_colors(group, nset)
                if nset == 0:
                    for i in range(len(indicators)):
                        axsi.plot(data_plot.values, data[i], color=colors[i])
                    axsi.legend(indicators, loc='lower left')
                else:
                    for i in range(len(indicators)):
                        if axss is None:
                            axss = axsi.twinx()
                        axss.plot(data_plot.values, data[i], color=colors[i])
                    axss.legend(indicators, loc='upper right')
                    if plottype == "energy":
                        axss.set_ylabel('% of occupied time in discomfort')
            if plottype == "energy":            
                axsi.set_ylabel(group + ': ' + ', % of ∆energy needs/nominal')
            elif plottype == "electricity":
                axsi.set_ylabel('electricity indicator in %')
            axsi.set_xlabel(data_plot.variable.replace('_', ' '))
            axsi.set_xticks(data_plot.values)
            axsi.grid()
        self.add_figure()  # 'nb of hours in discomfort'



class SimResult:

    def __init__(self, configuration: Configuration, datetimes: list[datetime]):
        if not hasattr(SimResult, 'nominal'):
            SimResult.nominal = self
        self.configuration: Configuration = configuration
        self.zeros: list[int] = [0 for _ in range(len(datetimes))]
        self.datetimes = datetimes
        
        self.avg_outdoor_temperatures: list[float]
        self.avg_outdoor_temperatures_for_hvac_periods: list[float]
        
        self.Hfloor: float
        self.Sfloor: float
        self.Hbuilding: float 
        self.Lsouth_north_wall: float 
        self.Least_west_wall: float 
        self.Ssouth_north_wall: float 
        self.Seast_west_wall: float
        self.Swalls: float
        self.total_glazing_surface: float
        
        self.Uwall: float
        self.Uglass: float
        self.Uroof: float
        self.Uground: float
        
        self.heating_period_indices: tuple[int, int]
        self.cooling_period_indices: tuple[int, int]
        
        self.occupancy: list[float]
        self.discomfort18: float
        self.discomfort29: float
        
        self.neeg: float
        self.year_autonomy: float
        self.self_consumption: float
        self.self_production: float
        self.electricity_needs_in_kWh: list[float]
        self.PVproduction_kWh: list[float]
        self.electricity_grid_exchange_in_kWh: list[float]
        self.month_average_PV_energy: list[float]
        self.max_grid_withdraw_datetime: datetime
        self.max_grid_withdraw_in_kWh: float
        self.max_grid_withdraw_PVcovering: float
        
        self.month_average_needed_energy_kWh: list[float]
        self.monthly_electricity_consumption_in_kWh: list[float]
        self.windows_solar_gains_Wh: dict[str, list[float]]
        
        self.indoor_temperatures: list[float]
        self.avg_indoor_temperatures: list[float]
        self.setpoint_temperatures: list[float|None]
        self.heating_needs_kWh: list[float]
        self.cooling_needs_kWh: list[float]
        self.max_heating_power: float
        self.max_cooling_power: float
        
    @property
    def air_volume(self)-> float:
        return self.configuration('total_living_surface') * self.Hfloor  
    
    @property
    def heating_needs_kWh_total(self) -> float:
        return sum(self.heating_needs_kWh)
    
    @property
    def heating_needs_percent(self) -> float:
        return 100 * (self.heating_needs_kWh_total - SimResult.nominal.heating_needs_kWh_total) / self.heating_needs_kWh_total if self.heating_needs_kWh_total !=0 else 0
        
    @property
    def cooling_needs_in_kWh_total(self) -> float:
        return sum(self.cooling_needs_kWh)
    
    @property
    def cooling_needs_percent(self) -> float:
        return 100 * (self.cooling_needs_in_kWh_total - SimResult.nominal.cooling_needs_in_kWh_total) / self.cooling_needs_in_kWh_total if self.cooling_needs_in_kWh_total !=0 else 0
    
    @property
    def hvac_needs_kWh(self) -> list:
        return [self.heating_needs_kWh[i] + self.cooling_needs_kWh[i] for i in range(len(self.datetimes))]
    
    @property
    def hvac_needs_in_kWh_total(self) -> float:
        return sum(self.hvac_needs_kWh)
    
    @property
    def hvac_needs_percent(self) -> float:
        return 100 * (self.hvac_needs_in_kWh_total - SimResult.nominal.hvac_needs_in_kWh_total) / self.hvac_needs_in_kWh_total if self.hvac_needs_in_kWh_total !=0 else 0
    

class Analysis:

    def __init__(self, report: Experiment) -> None:
        self.configuration: Configuration = report.configuration
        self.skyline: list[tuple[float, float]] = self.configuration('skyline')
        self.latitude = self.configuration('latitude')
        self.longitude = self.configuration('longitude')
        self.simulator = Simulator(self.configuration)
        self.simulator.simulate(self.configuration)  # first simulation is taken as reference (nominal)
        #SimResult.nominal = SimResult.nominal  
        print(file=sys.stderr)

    def climate(self, experiment: Experiment):
        experiment.add_text('## 2. Local climate Analysis <a name="climate"></a>')
        experiment.add_text('### Evolution of the outdoor and its averaged temperatures with detected heating and cooling periods')
        experiment.add_text('The first time the averaged outdoor temperatures pass over the threshold "summer_hvac_trigger_temperature" determines the end of the heating period, and the last time it passed under, it determines the start of the heating period. Similarly, the first time the averaged outdoor temperature passes over the "winter_hvac_trigger_temperature" threshold determines the beginning the cooling period, and last time it passes down, the end.')

        datetimemarks = []
        if SimResult.nominal.heating_period_indices is not None and len(SimResult.nominal.heating_period_indices) > 0:
            datetimemarks.append(SimResult.nominal.datetimes[SimResult.nominal.heating_period_indices[0]])
            datetimemarks.append(SimResult.nominal.datetimes[SimResult.nominal.heating_period_indices[1]])
            experiment.add_text('- The detected heating period is actually composed of 2 periods: one from January 1st until ' + datetime_to_stringdate(SimResult.nominal.datetimes[SimResult.nominal.heating_period_indices[0]], date_format='%d %B') + ' and another one from ' + datetime_to_stringdate(SimResult.nominal.datetimes[SimResult.nominal.heating_period_indices[1]], date_format='%d %B') + ' to the end of the year.\n')

        if SimResult.nominal.cooling_period_indices is not None and len(SimResult.nominal.cooling_period_indices) > 0:
            datetimemarks.append(SimResult.nominal.datetimes[SimResult.nominal.cooling_period_indices[0]])
            datetimemarks.append(SimResult.nominal.datetimes[SimResult.nominal.cooling_period_indices[1]])
            experiment.add_text('- The detected cooling period starts from ' + datetime_to_stringdate(SimResult.nominal.datetimes[SimResult.nominal.cooling_period_indices[0]], date_format='%d %B') + ' till ' + datetime_to_stringdate(SimResult.nominal.datetimes[SimResult.nominal.cooling_period_indices[1]], date_format='%d %B') + '.\n')

        experiment.add_text('This curve shows the local outdoor temperatures along time during the reference year. It comes from the openweathermap file. Moreover, the orange curveis the averaged temperature values used to detect the heating and cooling periods. The red lines corresponds to the detection thesholds.')
        experiment.add_text('- Outdoor temperature and averaged values with heating/cooling periods')
        experiment.add_timeplot('Outdoor temperature and averaged values', SimResult.nominal.datetimes, self.simulator.outdoor_temperatures, datetimemarks=datetimemarks, valuemarks=[self.configuration('winter_hvac_trigger_temperature'), self.configuration('summer_hvac_trigger_temperature')], averaged_values=SimResult.nominal.avg_outdoor_temperatures_for_hvac_periods)

        experiment.add_text('The following figures are named monotonics. The values are not sorted with respect to the time but in a decreasing order: it corresponds to the curve filled with blue, left scale. The x-axis stands for the percentage of the values higher than the corresponding value given by the curve. It is therefore easy to analyse how values are distributed.')
        experiment.add_image('monotonic.png')
        experiment.add_text('The cyan dots indicate in which month the value has been recorded (right scale with month number in the year (1=January,..., 12=December)).')

        experiment.add_text('- Monotonic of the outdoor temperatures in Celsius')
        experiment.add_monotonic('Monotonic of the outdoor temperatures in Celsius', SimResult.nominal.datetimes, self.simulator.outdoor_temperatures, valuemarks=(self.configuration('winter_hvac_trigger_temperature'), self.configuration('summer_hvac_trigger_temperature')))
        experiment.add_text('- Monotonic of the wind speed in km/h')
        experiment.add_text('- Monotonic of the cloudiness in percentage of the sky covered by clouds')
        experiment.add_monotonic('Monotonic of the cloudiness in percentage of the sky covered by clouds', SimResult.nominal.datetimes, self.simulator.cloudiness)
        experiment.add_text('- Monotonic of the relative humidity in percentage')
        experiment.add_monotonic('Monotonic of the relative humidity in percentage', SimResult.nominal.datetimes, self.simulator.humidities)
        experiment.add_text('- Monotonic of the horizontal solar radiation in W/m2')
        experiment.add_monotonic('Monotonic of the horizontal solar radiation in W/m2', SimResult.nominal.datetimes, self.simulator.directed_unit_solar_gains_Wh['horizontal'])
        experiment.add_text('- Precipitations (rain + hail + snow), rains and snowfalls (including hails) along time in mm')
        experiment.add_timeplot('Précipitations', SimResult.nominal.datetimes, self.simulator.precipitation,  rains=self.simulator.rain, snowfalls=self.simulator.snowfall)
        experiment.add_text('- Monotonic of the precipitations in mm/h')
        experiment.add_monotonic('Monotonic of the precipitations in mm/h', SimResult.nominal.datetimes, self.simulator.precipitation)
        experiment.add_text('- Monotonic of the rains in mm/h')
        experiment.add_monotonic('Monotonic of the rains in mm/h', SimResult.nominal.datetimes, self.simulator.rain)
        experiment.add_text('- Monotonic of the snowfalls in mm/h')
        experiment.add_monotonic('Monotonic of the snowfalls in mm/h', SimResult.nominal.datetimes, self.simulator.snowfall)

        experiment.add_text('The wind speeds and directions over a time period, are usually represented by a wind rose. The colors represent the speed of the wind and a radius stands for the so-called meteorological direction i.e. the direction from where the wind is coming from. Along a radius, the circles indicate the percentage of a sample occurrence i.e. a given interval of wind direction and wind speed.')
        experiment.add_windrose(self.simulator.wind_directions_deg, self.simulator.wind_speeds_m_s)

    def evolution(self, experiment: Experiment):
        experiment.add_text('## 3. Long term climate Evolution <a name="evolution"></a>')
        experiment.add_text('These curves represent the long term evolution of the weather variables. Each radius corresponds to a month. Each curve corresponds to a year with averaged month values. Yellow color stands for oldest years, violet for middle and blue to most recent years.')
        all_site_weather_data = WeatherJsonReader(self.configuration('weather_file_name'), None, None, albedo=self.configuration('albedo'), location=self.configuration('location')).site_weather_data
        experiment.add_text('### Outdoor temperature evolution (month average)')
        experiment.add_monthly_trend('Outdoor temperature evolution (month average)', all_site_weather_data.get('datetime'), all_site_weather_data.get('temperature'))
        experiment.add_text('### Outdoor cloudiness evolution (month average)')
        experiment.add_text('This curve represents the long term month averaged cloudiness or nebulosity in percentage (0%=blue sky, 100% sky totally covered by clouds).')
        experiment.add_monthly_trend('Outdoor cloudiness evolution (month average)', all_site_weather_data.get('datetime'), all_site_weather_data.get('cloudiness'))

    def solar(self, experiment): 
        experiment.add_text('## 3. Solar radiation analysis <a name="solar"></a>')
        experiment.add_text('An heliodor represents the sun path along the year. The position of the sun is represented by 2 angles: the azimut, the angle formed by a vertical plan directed to the south and the vertical plan where the sun is i.e. the azimut angle, and the altitude (or elevation) of the sun formed by the horizontal plan tangent to earth and the horizontal where the sun is with 0° means: directed to the south (for azimut, east is negative and west positive, and altitude 0° and 90° stand respectively for horizontal and vertical positions). The heliodor plot represents the trajectory of the sun the 21th of each month of the year.')
        experiment.add_image('solar_angles.png')
        experiment.add_text('Additionally, the solar masks coming from the skyline in particular (specified in the configuration file) are also drawn: gray dots represent the angles where the sun is visible.')
        experiment.add_text('- Heliodor at local position, with the azimut angles on the x-axis and the altitude angle on the y-axis')
        axis = self.simulator.solar_model.plot_heliodor(self.configuration('weather_year'))
        buildingenergy.solar.SkyLineMask(*self.skyline).plot('Skyline', axis=axis)
        experiment.add_figure()
        Sfloor: float = self.configuration('total_living_surface') / self.configuration('number_of_floors')
        experiment.add_text("The best exposure (horizontal angle of the perpendicular to the PV panel wrt the south) and best tilt angle (vertical angle of the perpendicular to the PV panel wrt to the south), have been computed. An exposure of -90° means the panel is directed to the the east, +90° to the west. A slope of 90° means the panel is facing the south whereas 0° means facing vertically the sky.")
        experiment.add_image("exposure_tilt.png")
        experiment.add_text("- The best PV exposure angle is: %.2f°E with a tilt angle of %.2f° with a production of %ikWh/year for %im2" % (self.simulator.best_PV_exposure_in_deg, self.simulator.best_PV_slope_in_deg, sum(self.simulator.best_PV_unit_productions_in_kWh) * SimResult.nominal.Sfloor, SimResult.nominal.Sfloor))
        experiment.add_text('- The next figure gives the collected solar energy (not PV production) on different ($1m^2$) surface direction')
        experiment.add_barchart('Collected solar energy on different surfaces', 'kWh/m2.year', **{direction: sum(self.simulator.directed_unit_solar_gains_Wh[direction])/1000 for direction in self.simulator.directed_unit_solar_gains_Wh}) 

    def house(self, experiment: Experiment):
        experiment.add_text('## 4. House Analysis <a name="house"></a>')
        experiment.add_text('### Global results')
        
        experiment.add_text('- The following time plot represents the evolution along time of the indoor temperatures (blue), the setpoints of the HVAC system (orange) and the outdoor temperatures (green).')
        experiment.add_text('The horizontal dashed red lines point out the values that are used to estimate the inhabitant discomfort. The percentage of the occupancy hours where the temperature is over 29°C stands for summer discomfort and the percentage of the occupancy hours where the temperature is under 18°C stands for winter discomfort. These values may be more important than in reality because the model does not represent the window openings and other reative actions done by the occupants in reaction to overheating.')
        experiment.add_timeplot('indoor temperatures', SimResult.nominal.datetimes, SimResult.nominal.avg_indoor_temperatures, valuemarks=[18, 29], setpoints=SimResult.nominal.setpoint_temperatures, outdoor_temperatures=self.simulator.outdoor_temperatures)  
        
        experiment.add_text('The resulting primary energy needs are given below. In addition to these values, the final energy need taking into account the coefficient of performance of the HVAC system are also given.')
        experiment.add_text('- The primary year heat needed for heating the lambda-house is: %.fkWh, with a final energy needs = %.fkWh and a maximum power of %.fW' % (SimResult.nominal.heating_needs_kWh_total, SimResult.nominal.heating_needs_kWh_total / self.configuration('hvac_COP'), SimResult.nominal.max_heating_power_W))
        experiment.add_text('- The primary year heat removal needed for cooling the lambda-house is: %.fkWh, with a final energy needs = %.fkWh and a maximum power of %.fW' % (SimResult.nominal.cooling_needs_in_kWh_total, SimResult.nominal.cooling_needs_in_kWh_total / self.configuration('hvac_COP'), SimResult.nominal.max_cooling_power_W))
        experiment.add_text('- The primary year heat need for the HVAC system (heating and cooling) is: %.fkWh (with a final energy needs = %.fkWh)' % (SimResult.nominal.hvac_needs_in_kWh_total, SimResult.nominal.hvac_needs_in_kWh_total / self.configuration('hvac_COP')))
        
        # consumption comparison
        packed_results = {
            'heating': SimResult.nominal.heating_needs_kWh_total/self.configuration('total_living_surface')/self.configuration('hvac_COP'),
            'cooling': SimResult.nominal.cooling_needs_in_kWh_total/self.configuration('total_living_surface')/self.configuration('hvac_COP')
            }        
        
        experiment.add_text('- The following bar chart represents the final energy from a heat pump (COP=%.1f) needed per square meter of useful living surface.' % (self.configuration('hvac_COP')))
        experiment.add_barchart('Final energy for heating and cooling with a heat pump', 'kWh/m2/year', needed_energy = packed_results)
        
        experiment.add_text('Monthly electricity needs are plotted below, together with heat needs and the PV production')
        experiment.add_timeplot('monthly electricity needs in kWh', SimResult.nominal.datetimes, SimResult.nominal.monthly_electricity_consumption_in_kWh, monthly_energy_needs_in_kWh=SimResult.nominal.month_average_needed_energy_kWh, monthly_PV_energy_produced_in_kWh=SimResult.nominal.month_average_PV_energy)
        
        experiment.add_text('- The following bar chart represents the discomfort18 (the ratio of hours of presence where the temperature is lower than 18°C) and discomfort29 (the ratio of hours of presence where the temperature is higher than 29°C).')
        experiment.add_barchart('ratio of hours of presence with discomfort', 'hours in discomfort / hours of occupancy in %', needed_energy = {'discomfort18': SimResult.nominal.discomfort18, 'discomfort29': SimResult.nominal.discomfort29})

        experiment.add_text("### Parametric analyses")
        experiment.add_text("Different parametric analyses are performed in the next. It consists in modifying one parameter while keeping all the others at their nominal values. The impact is computed in percentage of variation wrt nominal impacts: heating primary energy needs, cooling primary energy needs (and their total), but also indicators dealing with inhabitant comfort: Discomfort18, the frequency of hours with presence where indoor temperature is lower than 18°C. In the same way, Discomfort29 is the frequency of hours where the indoor temperature is higher than 29°C.") 
        experiment.add_text("Right hand scale is representing the percentage of variation wrt to nominal value. For instance, 0% means the result is the same than the one of the nominal parameter values. 100% means the value is the double of the case of nominal results, and -50% stands for half of the nominal value. It concerns the variable representing the heating, cooling and total energy needs.")
        experiment.add_text("Left hand scale represents the discomfort indicators: discomfort18 and discomfort29, see above).")
        experiment.add_text("The first parametric analysis focuses on glazing. Variation of the surface of glazing (10% for each house side for nominal) are computed: there are as many plot that the studied direction.")
        
        experiment.add_text("- Parametric analysis of the glazing for each side.")
        self.parametric_analysis(parameter_name='glazing', experiment=experiment)
        
        experiment.add_text("- Parametric analysis of lambda-house direction (exposure of the south side).")
        
        experiment.add_text("The house is rotated east/west to analyze the resulting global impacts (remember that the skyline is also impacting the results).")
        experiment.add_text('The best angle of the south wall with the South (0° stands for South wall facing the South, 90° the West and -90° the East.) ')
        experiment.add_image('exposure.png')
        self.parametric_analysis(parameter_name='offset_exposure', experiment=experiment)
        
        experiment.add_text('- The shape factor parametric analysis')
        experiment.add_text("Changing the shape factor, a square ground print at first, aims at defining the best house shape.")
        experiment.add_text('It keeps the useful building surface constant, 1 yields a square,  and higher than 1 value yields a rectangle with South/North sides bigger than East/West sides and lower than one, the opposite')
        table = prettytable.PrettyTable()
        Sfloor: float = self.configuration('total_living_surface') / self.configuration('number_of_floors')
        table.field_names = ('shape factor', 'south/north side length', 'west/east side length')
        for shape_factor in self.configuration.parametric('shape_factor'):
            Lsouth_north_wall = math.sqrt(Sfloor * shape_factor)
            Least_west_wall =  math.sqrt(Sfloor / shape_factor)
            table.add_row(('%.2f' % shape_factor, '%.2f' % Lsouth_north_wall, '%.2f' % Least_west_wall))
        experiment.add_pretty_table(table) # delta_temperature_absence_mode
        self.parametric_analysis(parameter_name='shape_factor', experiment=experiment)
        
        experiment.add_text('- The number_of_floors: the useful surface can be distributed over different floors, reducing thus the floor print, and increasing the height of the house.')
        self.parametric_analysis(parameter_name='number_of_floors', experiment=experiment)
        
        experiment.add_text('- Parametric study of the solar protection over the South glazing')
        experiment.add_text('The parameter "south_solar_protection_angle" stands for the maximum altitude angle where the sun is not hidden by the passive solar protection mask over the South glazing.')
        experiment.add_text('The nominal lambda house has a passive solar mask, which is masking the sun at a specified altitude:. This parametric analysis makes it possible to define a relevant compomise for this exposure angle leading to lower energy needs while limiting the inhabitant discomfort.')
        self.parametric_analysis(parameter_name='south_solar_protection_angle', experiment=experiment)
        
        experiment.add_text('- Parametric study of the air renewal through ventilation in indoor volume per hour')
        self.parametric_analysis(parameter_name='air_renewal_presence', experiment=experiment)
        
        experiment.add_text('- Parametric study of the ventilation heat recovery efficiency')
        experiment.add_text('The ventilation heat recovery efficiency reduces the heat exchanges between indoor and outdoor. 0% means there is no dual flow ventilation system and 85%, which is the greatest value than can be found on rotating heat exchangers with wheels, means that 85% of the heat from the extracted air is recovered and reinjected into the new air (heat exchange represents 15% of the one of a single flow ventilation system).')
        self.parametric_analysis(parameter_name='ventilation_heat_recovery_efficiency', experiment=experiment)
        
        experiment.add_text('- Parametric study of the inertia level')
        experiment.add_text('It simulates different inertia levels acting as an average filter of the outdoor temperatures. The value corresponds the number of hours of the average filter: the highest, the best.')
        self.parametric_analysis(parameter_name='inertia_level', experiment=experiment)
        
        experiment.add_text('- Parametric study of the HVAC heating temperature setpoint')
        experiment.add_text('This setpoint temperature is applied only during time periods where at least an inhabitant is present.')
        self.parametric_analysis(parameter_name='heating_setpoint', experiment=experiment)
        
        experiment.add_text('- Parametric study of the HVAC cooling temperature setpoint')
        experiment.add_text('This setpoint temperature is applied only during time periods where at least an inhabitant is present.')
        self.parametric_analysis(parameter_name='cooling_setpoint', experiment=experiment)
        
        
        experiment.add_text('- Parametric study of the delta of temperature reduction for heating, and increase for cooling during absence of inhabitants')
        self.parametric_analysis(parameter_name='delta_temperature_absence_mode', experiment=experiment)
        
        if 'insulation' in self.configuration.__dict__:
            experiment.add_text('- Parametric study of the insulation thickness')
            experiment.add_text('It modifies the thickness of the chosen material for insulation.')
            
            self.parametric_analysis(parameter_name=self.configuration.insulation, experiment=experiment)
        
    def neutrality(self, experiment):
        simulation_results = self.simulator.simulate(configuration=self.configuration)
        experiment.add_text('## 5. Neutrality analysis <a name="neutrality"></a>')
        
        experiment.add_text('### Zero energy over the year')
        experiment.add_text('The aim is to appreciate the yearly energy needed by the HVAC system. To do it, the energy neutrality is searched thanks to a certain surface of photovoltaic panels.')

        experiment.add_text('- The required surface of photovoltaic panels for balancing the annual energy consumption of the HVAC system is:')
        table = prettytable.PrettyTable()
        table.field_names = ('PV (efficiency: %.f%%)' % (100*self.configuration('PV_efficiency')), 'energy needs (in best eq. PV m2)')
        table.add_row(('heater', '%.fm2' % (SimResult.nominal.heating_needs_kWh_total / self.configuration('hvac_COP') / sum(self.simulator.best_PV_unit_productions_in_kWh))))
        table.add_row(('climatizer', '%.fm2' %  (SimResult.nominal.cooling_needs_in_kWh_total / self.configuration('hvac_COP') / sum(self.simulator.best_PV_unit_productions_in_kWh))))
        table.add_row(('HVAC', '%.fm2' %  (SimResult.nominal.hvac_needs_in_kWh_total / self.configuration('hvac_COP') / sum(self.simulator.best_PV_unit_productions_in_kWh))))
        table.add_row(('ALL', '%.fm2' %  (sum(SimResult.nominal.electricity_needs_in_kWh) / sum(self.simulator.best_PV_unit_productions_in_kWh))))
        experiment.add_pretty_table(table)
        
        experiment.add_text('The electricity hourly consumption and phoyovoltaic production (surface=%.2fm2) is plotted below:' % self.configuration('PV_surface'))
        experiment.add_timeplot('electricity needs (kWh/year)', simulation_results.datetimes, simulation_results.electricity_needs_in_kWh, PV_production=simulation_results.PVproductions_in_kWh)
        
        experiment.add_text('Different indicators are used to appreciate the level of autonomy and dependency from the grid:')
        experiment.add_text('- the self-consumption is the part of the PV electricity consumed locally: the more, the lower the electricity bill: %.0f%%' % (SimResult.nominal.self_consumption))
        experiment.add_image('self_consumption.png')
        experiment.add_text('- the self-production is the part of the consumption produced locally by PV: it is representing how much the energy needs are covered: %.0f%%'%(SimResult.nominal.self_production))
        experiment.add_image('self_production.png')
        experiment.add_text('When self-consumption=self-production it means that all the load is produced locally.')
        experiment.add_text('- the L-NEEG is the net energy exchange with the grid (import + export) normalized by the total load. The less, the more independent of the grid: %.0f%%'%(SimResult.nominal.neeg))
        experiment.add_image('L-NEEG.png')
        if SimResult.nominal.max_grid_withdraw_in_kWh <= 0:
            experiment.add_text('- the surface of PV (%.0f m2) is sufficient to cover the everyday electricity needs' % (self.configuration('PV_surface')))
        else:
            the_date = SimResult.nominal.max_grid_withdraw_datetime.strftime('%d/%m/%Y')
            experiment.add_text('- the surface of PV (%.0f m2) is not sufficient to cover all the daily electricity needs. The worst day of the year is %s: %im2 of PV should be added' % (self.configuration('PV_surface'), the_date, SimResult.nominal.max_grid_withdraw_PVcovering))
        
        
        experiment.add_text('- Parametric study of the net energy exchange with the grid, the self consumption and self-production')
        self.parametric_analysis(parameter_name='PV_surface', experiment=experiment, plottype="electricity")
        
        experiment.add_text('- The Net Energy Exchange from or to grid normalized by the total exlectricity needs is: %.f%%' % (simulation_results.neeg))
        experiment.add_text('- The year electricity autonomy is: %.f%%' % (simulation_results.year_autonomy))
        experiment.add_text('- The self consumption, i.e. the part of the production consumed locally, normalized by the total electricity needs is: %.f%%' % (simulation_results.self_consumption))
        experiment.add_text('- The self production, i.e. the part of the consumption produced locally, normalized by the total electricity needs is: %.f%%' % (simulation_results.self_production))
        
    def get_energy_indicators(self, simresults: SimResult):
        return [{'heating':simresults.heating_needs_percent, 'cooling': simresults.cooling_needs_percent, 'total': simresults.hvac_needs_percent}, {'discomfort18': simresults.discomfort18, 'discomfort29': simresults.discomfort29}]
    
    def get_electricity_indicators(self, simresults: SimResult):
        return [{'neeg': simresults.neeg, 'self_consumption': simresults.self_consumption, 'self_production': simresults.self_production, 'year_autonomy': simresults.year_autonomy}]

    def parametric_analysis(self, parameter_name: str, experiment: Experiment, plottype: str="energy"):
        experiment.add_text('Parametric analysis on %s' % parameter_name.replace('_',' '))
        
        groups = None
        if type(self.configuration(parameter_name)) == dict:
            groups = [group for group in Simulator.directions]
        else: 
            groups = [parameter_name]
         
        if parameter_name in self.configuration._parametrics:    
            data_plot = DataPlot(parameter_name)
            for group in groups:
                pvals = self.configuration.parametric(parameter_name)
                data_plot.set_parametric_values(pvals)
                for pval in self.configuration:
                    if len(groups) > 1:
                        self.configuration.set(parameter_name, pval, group)
                    else:
                        self.configuration.set(parameter_name, pval)
                    simresults: SimResult = self.simulator.simulate(configuration=self.configuration)
                    
                    if plottype == 'energy':
                        resulting_indicator_sets = self.get_energy_indicators(simresults)
                    else:
                        resulting_indicator_sets = self.get_electricity_indicators(simresults)
                        
                    for setid, resulting_indicators in enumerate(resulting_indicator_sets):
                        for resulting_indicator in resulting_indicators:
                            data_plot.add(resulting_indicator, group, setid, resulting_indicator_sets[setid][resulting_indicator])
            print(file=sys.stderr)
            experiment.add_parametric(data_plot, plottype)


class Simulator:

    directions = {'north': 180, 'west': 90, 'east': -90, 'south': 0}

    def __init__(self, configuration: Configuration):
        """Initializer of the lambda-house

        :param configuration: configuration class
        :type configuration: Configuration
        """
        self.outdoor_temperatures: list[float]
        self.average_outdoor_temperature: float
        self.wind_speeds_m_s: list[float]
        self.average_wind_speed: float
        self.cloudiness: list[float]
        self.humidities: list[float]
        self.precipitation: list[float]
        self.rain: list[float]
        self.snowfall: list[float]
        
        self.best_PV_unit_productions_in_kWh: list[float]
        self.best_PV_slope_in_deg: float
        self.best_PV_exposure_in_deg: float
        #self.directed_unitary_solar_gains_kWh: dict[str, list[float]]
        
        self.configuration_results = dict()
        
        self.site_weather_data: buildingenergy.openweather.SiteWeatherData = WeatherJsonReader(configuration('weather_file_name'), '1/01/%i' % configuration('weather_year'), '1/01/%i' % (configuration('weather_year') + 1), albedo=configuration('albedo'), location=configuration('location'), longitude=configuration('longitude'), latitude=configuration('latitude')).site_weather_data
        self.solar_model = SolarModel(self.site_weather_data)
        
        self.outdoor_temperatures = self.site_weather_data.get('temperature')
        self.wind_speeds_m_s = self.site_weather_data.get('wind_speed')
        self.wind_directions_deg = self.site_weather_data.get('wind_direction_in_deg')
        self.cloudiness = self.site_weather_data.get('cloudiness')
        self.humidities = self.site_weather_data.get('humidity')
        self.precipitation = self.site_weather_data.get('precipitation')
        self.rain = self.site_weather_data.get('rain')
        self.snowfall = self.site_weather_data.get('snowfall')
        
        self.average_outdoor_temperature = sum(self.outdoor_temperatures) / len(self.outdoor_temperatures)
        self.average_wind_speed = sum(self.wind_speeds_m_s) / len(self.wind_speeds_m_s)
        self.skyline: list[tuple[float, float]] = configuration('skyline')
        
        self.unit_pv_system = buildingenergy.solar.PVsystem(self.solar_model, surfacePV_in_m2=1, array_width=1, panel_height_in_m=1, efficiency=configuration('PV_efficiency'))
        self.best_PV_unit_productions_in_kWh, self.best_PV_exposure_in_deg, self.best_PV_slope_in_deg = self.get_best_PV_unit_production_in_kWh(configuration, self.unit_pv_system) 
        
        self.unit_solar_radiations = buildingenergy.solar.SolarSystem(self.solar_model)
        for direction in Simulator.directions:
            self.unit_solar_radiations.add_collector(direction, surface=1, exposure_in_deg=Simulator.directions[direction], slope_in_deg=90, solar_factor=1)
        self.unit_solar_radiations.add_collector('horizontal', surface=1, exposure_in_deg=0, slope_in_deg=0, solar_factor=1)
        self.unit_solar_radiations.add_collector('best', surface=1, exposure_in_deg=self.best_PV_exposure_in_deg, slope_in_deg=self.best_PV_slope_in_deg, solar_factor=1)
        self.global_unitary_solar_gains_Wh, self.directed_unit_solar_gains_Wh = self.unit_solar_radiations.solar_gains_in_Wh()
        
        self.solar_building = buildingenergy.solar.SolarSystem(self.solar_model)
        
    def compute_thermal(self, configuration: Configuration, simresults) -> SimResult:
        
        simresults.Hfloor = configuration('height_per_floor')
        simresults.Sfloor: float = configuration('total_living_surface') / configuration('number_of_floors') 
        simresults.Hbuilding: float = configuration('height_per_floor') * configuration('number_of_floors') 
        simresults.Lsouth_north_wall = math.sqrt(simresults.Sfloor*configuration('shape_factor')) 
        simresults.Least_west_wall =  math.sqrt(simresults.Sfloor/configuration('shape_factor')) 
        
        simresults.Ssouth_north_wall = simresults.Lsouth_north_wall * simresults.Hbuilding
        simresults.Seast_west_wall = simresults.Least_west_wall * simresults.Hbuilding
        
        simresults.total_glazing_surface = simresults.Ssouth_north_wall * (configuration('glazing')['north'] + configuration('glazing')['south']) + simresults.Seast_west_wall * (configuration('glazing')['east'] + configuration('glazing')['west'])  
        
        simresults.Swalls = 2 * (simresults.Ssouth_north_wall + simresults.Seast_west_wall) - simresults.total_glazing_surface 

        wall_composition = Composition(first_layer_indoor=True, last_layer_indoor=False, position='vertical', indoor_average_temperature_in_celsius=21, outdoor_average_temperature_in_celsius=self.average_outdoor_temperature, wind_speed_is_m_per_sec=self.average_wind_speed, heating_floor=False)
        for material, thickness in configuration('wall_composition_in_out'):  
            if material in configuration:
                thickness = configuration(material)
            wall_composition.add_layer(material, thickness)

        glass_composition = Composition(first_layer_indoor=True, last_layer_indoor=False, position='vertical', indoor_average_temperature_in_celsius=21, outdoor_average_temperature_in_celsius=self.average_outdoor_temperature, wind_speed_is_m_per_sec=self.average_wind_speed, heating_floor=False)
        for material, thickness in configuration('glass_composition_in_out'):
            if material in configuration:
                thickness = configuration(material)
            glass_composition.add_layer(material, thickness)

        roof_composition = Composition(first_layer_indoor=True, last_layer_indoor=False, position='horizontal', indoor_average_temperature_in_celsius=21, outdoor_average_temperature_in_celsius=self.average_outdoor_temperature, wind_speed_is_m_per_sec=self.average_wind_speed, heating_floor=False)
        for material, thickness in configuration('roof_composition_in_out'):
            if material in configuration:
                thickness = configuration(material)
            roof_composition.add_layer(material, thickness)

        ground_composition = Composition(first_layer_indoor=True, last_layer_indoor=False, position='horizontal', indoor_average_temperature_in_celsius=21, outdoor_average_temperature_in_celsius=self.average_outdoor_temperature, wind_speed_is_m_per_sec=self.average_wind_speed, heating_floor=False)
        for material, thickness in configuration('ground_composition_in_out'):  
            if material in configuration:
                thickness = configuration(material)
            ground_composition.add_layer(material, thickness)

        simresults.Uwall = wall_composition.U * simresults.Swalls
        simresults.Uglass = glass_composition.U * simresults.total_glazing_surface
        simresults.Uroof = roof_composition.U * simresults.Sfloor
        simresults.Uground = ground_composition.U * simresults.Sfloor
        return simresults
        
    def compute_solar_gain(self, configuration: Configuration, simresults: SimResult) -> SimResult:
        self.solar_building = buildingenergy.solar.SolarSystem(self.solar_model)
        glazing_surfaces: dict[str, float] = {'south': simresults.Ssouth_north_wall * configuration('glazing')['south'], 'north': simresults.Ssouth_north_wall * configuration('glazing')['north'], 'east': simresults.Seast_west_wall * configuration('glazing')['east'], 'west': simresults.Seast_west_wall * configuration('glazing')['west']}  
        
        for direction in Simulator.directions:
            if direction == 'south':
                window_solar_mask = buildingenergy.solar.RectangularMask((-90 + configuration('offset_exposure') + Simulator.directions[direction], 90 + configuration('offset_exposure') + Simulator.directions[direction]), (0, configuration('south_solar_protection_angle')))  
                self.solar_building.add_collector(direction, surface=glazing_surfaces[direction], exposure_in_deg=configuration('offset_exposure'), slope_in_deg=90, solar_factor=configuration('solar_factor'), collector_mask=window_solar_mask)  # , collector_mask=window_solar_mask
            else:  
                self.solar_building.add_collector(direction, surface=glazing_surfaces[direction], exposure_in_deg=configuration('offset_exposure') + Simulator.directions[direction], slope_in_deg=90, solar_factor=configuration('solar_factor'))  # , collector_mask=window_solar_mask  
                
        total_solar_gains_in_Wh, collector_solar_gains_in_Wh = self.solar_building.solar_gains_in_Wh()
        simresults.windows_solar_gains_Wh = total_solar_gains_in_Wh 
        return simresults
    
    def compute_occupancy(self, configuration: Configuration, simresults) -> SimResult:
        occupancy_schema = configuration('occupancy_schema')
        occupancy = list()
        for datetime in simresults.datetimes:
            day_of_week = datetime.isoweekday()
            hour_in_day = datetime.hour
            for weekdays in occupancy_schema:  
                if day_of_week in weekdays:
                    for period in occupancy_schema[weekdays]:  
                        if period[0] <= hour_in_day <= period[1]:
                            occupancy.append(occupancy_schema[weekdays][period])  
                        elif  hour_in_day >= period[0] or hour_in_day <= period[1]:
                            occupancy.append(occupancy_schema[weekdays][period])  
                        else:
                            occupancy.append(0)
                else:
                    occupancy.append(0)
        simresults.occupancy = occupancy
        return simresults
    
    def step(self, configuration: Configuration, simresults: SimResult, i: int, overventilation=False) -> tuple[str, float, float, float, float | None]:
        if overventilation and simresults.occupancy[i] > 0:
            Uvent = 1.204 * 1005 * simresults.air_volume * configuration('air_renewal_overheat')/3600  
        elif simresults.occupancy[i] == 0:
            Uvent = configuration('ventilation_heat_recovery_efficiency') * 1.204 * 1005 * simresults.air_volume * configuration('air_renewal_presence')/3600  
        else:
            Uvent = (1 - configuration('ventilation_heat_recovery_efficiency')) * 1.204 * 1005 * simresults.air_volume * configuration('air_renewal_presence')/3600  
        
        total_gain_Wh: float = simresults.windows_solar_gains_Wh[i] + simresults.occupancy[i] * (configuration('average_occupancy_electric_gain') + configuration('average_occupancy_metabolic_gain')) + configuration('average_permanent_electric_gain')  
        Uoutvent: float = simresults.Uwall + simresults.Uglass + simresults.Uroof + Uvent
        free_temperature: float = (Uoutvent * simresults.avg_outdoor_temperatures[i] + simresults.Uground * self.average_outdoor_temperature + total_gain_Wh) / (Uoutvent + simresults.Uground)  
        if simresults.heating_period_indices is not None and i <= simresults.heating_period_indices[0] or i >= simresults.heating_period_indices[1]: # heating period
            setpoint_temperature_value: float = configuration('heating_setpoint') if simresults.occupancy[i] > 0 else (configuration('heating_setpoint') - configuration('delta_temperature_absence_mode'))  
            raw_need_Wh  = Uoutvent * (setpoint_temperature_value - simresults.avg_outdoor_temperatures[i]) + simresults.Uground * (setpoint_temperature_value - self.average_outdoor_temperature) - total_gain_Wh
            if raw_need_Wh >= 0:
                indoor_temperature: float = setpoint_temperature_value
                heating_need_Wh = raw_need_Wh
                setpoint_temperature: float = setpoint_temperature_value
                cooling_need_Wh = 0
            else: 
                indoor_temperature = free_temperature
                heating_need_Wh: float = 0
                cooling_need_Wh: float = 0
                setpoint_temperature = None
        elif simresults.cooling_period_indices is not None and i >= simresults.cooling_period_indices[0] and i <= simresults.cooling_period_indices[1]:  # cooling period
            if simresults.occupancy[i] > 0:
                setpoint_temperature_value = configuration('cooling_setpoint')  
            else:
                setpoint_temperature_value = configuration('cooling_setpoint') + configuration('delta_temperature_absence_mode')  
            raw_need_Wh = Uoutvent * (setpoint_temperature_value - simresults.avg_outdoor_temperatures[i]) + simresults.Uground * (setpoint_temperature_value - self.average_outdoor_temperature) - total_gain_Wh
            if raw_need_Wh <= 0:
                indoor_temperature = setpoint_temperature_value
                heating_need_Wh = 0
                cooling_need_Wh = -raw_need_Wh
                setpoint_temperature = setpoint_temperature_value
            else: 
                indoor_temperature = free_temperature
                heating_need_Wh = 0
                cooling_need_Wh = 0
                setpoint_temperature = None
        else:  # period with HVAC off (spring and automn)
            indoor_temperature = free_temperature
            heating_need_Wh = 0
            cooling_need_Wh = 0
            setpoint_temperature = None
        return indoor_temperature, heating_need_Wh, cooling_need_Wh, setpoint_temperature

    
    def simulate(self, configuration: Configuration) -> SimResult:
        print('.', end='', file=sys.stderr)
        if configuration.signature() in self.configuration_results:
            print('x', end='', file=sys.stderr)
            return self.configuration_results[configuration.signature()]

        simresults = SimResult(configuration.clone(), self.site_weather_data.get('datetime'))
        
        simresults.avg_outdoor_temperatures_for_hvac_periods = Averager(self.outdoor_temperatures).sliding(configuration('hvac_hour_delay_for_trigger'))  
        avg_outdoor_temperatures_for_hvac_periods_array = numpy.array(simresults.avg_outdoor_temperatures_for_hvac_periods)
        
        no_heating_period = numpy.where(avg_outdoor_temperatures_for_hvac_periods_array > configuration('winter_hvac_trigger_temperature'))[0]
        if no_heating_period.size < len(simresults.datetimes) - configuration('hvac_hour_delay_for_trigger'):  
            i_end_heating: int = no_heating_period[0]
            i_start_heating: int = no_heating_period[-1]
            simresults.heating_period_indices = (i_end_heating, i_start_heating)
            
        no_cooling_period = numpy.where(avg_outdoor_temperatures_for_hvac_periods_array > configuration('summer_hvac_trigger_temperature'))[0]
        if len(no_cooling_period) > configuration('hvac_hour_delay_for_trigger'):  
            i_start_cooling = no_cooling_period[0]
            i_end_cooling = no_cooling_period[-1]
            simresults.cooling_period_indices = (i_start_cooling, i_end_cooling)
           
        simresults: SimResult = self.compute_thermal(configuration, simresults)
        simresults: SimResult = self.compute_solar_gain(configuration, simresults)
        simresults: SimResult = self.compute_occupancy(configuration, simresults)
    
        max_heating_power_W, max_cooling_power_W = 0, 0
        setpoint_temperatures = list()
        # avg_indoor_temperatures: dict[int, float] = dict()
        indoor_temperatures = list()
        heating_needs_kWh = list()
        cooling_needs_kWh = list()
        
        simresults.avg_outdoor_temperatures= Averager(self.outdoor_temperatures).sliding(configuration('inertia_level'))
        
        for i in range(len(simresults.datetimes)):
            indoor_temperature, heating_need_Wh, cooling_need_Wh, setpoint_temperature = self.step(configuration, simresults, i, overventilation=False)
            if simresults.occupancy[i] and indoor_temperature >= configuration('air_renewal_overheat_threshold'):
                indoor_temperature, heating_need_Wh, cooling_need_Wh, setpoint_temperature = self.step(configuration, simresults, i, overventilation=True)
            indoor_temperatures.append(indoor_temperature)
            heating_needs_kWh.append(heating_need_Wh / 1000)
            cooling_needs_kWh.append(cooling_need_Wh / 1000)
            setpoint_temperatures.append(setpoint_temperature)
            max_heating_power_W: float = max(heating_need_Wh, max_heating_power_W)
            max_cooling_power_W: float = max(cooling_need_Wh, max_cooling_power_W)
            
        simresults.indoor_temperatures = indoor_temperatures
        simresults.avg_indoor_temperatures = Averager(indoor_temperatures).sliding(configuration('inertia_level'))
        simresults.setpoint_temperatures = setpoint_temperatures
        simresults.indoor_temperatures = indoor_temperatures
        simresults.heating_needs_kWh = heating_needs_kWh
        simresults.cooling_needs_kWh = cooling_needs_kWh
        simresults.max_heating_power_W = max_heating_power_W
        simresults.max_cooling_power_W = max_cooling_power_W
        
        discomfort18_29 = [0, 0]
        occupancy_counter = 0
        
        for k, temperature in enumerate(indoor_temperatures):
            if simresults.occupancy[k] > 0:
                occupancy_counter += 1
                if temperature < 18:
                    discomfort18_29[0] += 1
                if temperature > 29:
                    discomfort18_29[1] += 1
                
        simresults.discomfort18 = 100*discomfort18_29[0]/occupancy_counter if occupancy_counter!=0 else 0
        simresults.discomfort29 = 100*discomfort18_29[1]/occupancy_counter if occupancy_counter!=0 else 0
        
        simresults.electricity_needs_in_kWh = [(heating_needs_kWh[i] + cooling_needs_kWh[i]) / configuration('hvac_COP')  + (configuration('average_permanent_electric_gain') + simresults.occupancy[i] * configuration('average_occupancy_electric_gain')) / 1000 for i in range(len(simresults.datetimes))]  
        
        # best_total_PV_unit_productions_in_kWh: float = sum(self.best_PV_unit_productions_in_kWh)
        simresults.PVproductions_in_kWh = [p * configuration('PV_surface') for p in self.best_PV_unit_productions_in_kWh]
        
        simresults.electricity_grid_exchange_in_kWh = [simresults.PVproductions_in_kWh[i] - simresults.electricity_needs_in_kWh[i] for i in range(len(simresults.datetimes))]
        averager: Averager = Averager(simresults.electricity_grid_exchange_in_kWh)
        daily_electricity_grid_exchange_kWh: list[float] = [- p*24 for p in averager.in_period(simresults.datetimes)]
        day_numbers: list[int] = averager.periods
        max_grid_withdraw_in_kWh: float = max(daily_electricity_grid_exchange_kWh)
        starting_day_hour_index: int = daily_electricity_grid_exchange_kWh.index(max_grid_withdraw_in_kWh)
        ending_day_hour_index: int = starting_day_hour_index + 1
        if ending_day_hour_index < len(day_numbers):
            while ending_day_hour_index < len(day_numbers) and day_numbers[ending_day_hour_index] == day_numbers[starting_day_hour_index]:
                ending_day_hour_index += 1
            max_grid_withdraw_datetime: datetime = simresults.datetimes[round((starting_day_hour_index+ending_day_hour_index)/2)]
            max_grid_withdraw_PVcovering: float = max_grid_withdraw_in_kWh / sum(self.best_PV_unit_productions_in_kWh[starting_day_hour_index: ending_day_hour_index])
        
            simresults.max_grid_withdraw_datetime = max_grid_withdraw_datetime
            simresults.max_grid_withdraw_in_kWh = max_grid_withdraw_in_kWh
            simresults.max_grid_withdraw_PVcovering = max_grid_withdraw_PVcovering
        
        simresults.year_autonomy = 100 * ecommunity.indicators.year_autonomy(simresults.electricity_needs_in_kWh, simresults.PVproductions_in_kWh)
        simresults.neeg: float = 100 *  ecommunity.indicators.NEEG_percent(simresults.electricity_needs_in_kWh, simresults.PVproductions_in_kWh)
        #sum([abs(simresults.electricity_grid_exchange_in_kWh[i]) for i in range(len(simresults.datetimes))])
        # load_kWh: float = sum(simresults.electricity_needs_in_kWh)
        # production_kWh: float = sum(simresults.PVproductions_in_kWh)
        # covered_consumption_in_kWh: float = sum([min(simresults.PVproductions_in_kWh[i], simresults. electricity_needs_in_kWh[i]) for i in range(len(simresults.datetimes))])
      
        # simresults.neeg = 100 * neeg_percent / load_kWh
        simresults.self_consumption =  100 * ecommunity.indicators.self_consumption(simresults.electricity_needs_in_kWh, simresults.PVproductions_in_kWh)
        #100 * covered_consumption_in_kWh / production_kWh
        simresults.self_production = 100 * ecommunity.indicators.self_sufficiency(simresults.electricity_needs_in_kWh, simresults.PVproductions_in_kWh)
        # 100 * covered_consumption_in_kWh / load_kWh
        
        _month_average_needed_energy_kWh: list[float] = Averager(simresults.hvac_needs_kWh).in_period(simresults.datetimes, month=True, sumup=True)
        _month_average_PV_energy: list[float] = Averager(simresults.PVproductions_in_kWh).in_period(simresults.datetimes, month=True, sumup=True)
        simresults.month_average_needed_energy_kWh = _month_average_needed_energy_kWh
        simresults.month_average_PV_energy = _month_average_PV_energy
        _monthly_electricity_consumption_in_kWh: list[float] = Averager(simresults.electricity_needs_in_kWh).in_period(simresults.datetimes, month=True, sumup=True)
        simresults.monthly_electricity_consumption_in_kWh = _monthly_electricity_consumption_in_kWh
        
        self.configuration_results[configuration.signature()] = simresults
        return simresults

    def get_best_PV_unit_production_in_kWh(self, configuration, pv_system) -> tuple[float, list[float], float, float]:
        print('Optimizing PV direction', file=sys.stderr)
        neighborhood: list[tuple[float, float]] = [(-1,0), (-1,1), (-1,-1), (0,-1), (0,1), (1,-1), (1,0), (1,1)]
        taboo = list()

        exposure_slope_in_deg_candidate: tuple[float, float] = (0, 30)
        best_exposure_slope_in_deg  = tuple(exposure_slope_in_deg_candidate)
        # pv_system.add_collector('PV', surface=1, exposure_in_deg=candidate[0], slope_in_deg=candidate[1], solar_factor=configuration('PV_efficiency'))
        
        exposure_in_deg=exposure_slope_in_deg_candidate[0]
        slope_in_deg=exposure_slope_in_deg_candidate[1]
       
        productions_in_kWh = pv_system.productions_in_kWh(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m=2, mount_type=buildingenergy.solar.MOUNT_TYPE.FLAT)
        best_total_production_in_kWh = sum(productions_in_kWh)
        taboo.append(exposure_slope_in_deg_candidate)

        improvement = True
        while improvement:
            improvement = False
            for neighbor in neighborhood:
                exposure_slope_in_deg_candidate = (best_exposure_slope_in_deg[0] + neighbor[0], best_exposure_slope_in_deg[1] + neighbor[1])
                exposure_in_deg: float = exposure_slope_in_deg_candidate[0]
                slope_in_deg: float = exposure_slope_in_deg_candidate[1]
                if -90<=exposure_in_deg<=90 and 0<=slope_in_deg<=90 and exposure_slope_in_deg_candidate not in taboo:
                    taboo.append(exposure_slope_in_deg_candidate)
                    productions_in_kWh=pv_system.productions_in_kWh(exposure_in_deg, slope_in_deg, 1, buildingenergy.solar.MOUNT_TYPE.FLAT)
                    # pv_system.add_collector('PV', surface=1, exposure_in_deg=candidate[0], slope_in_deg=candidate[1], solar_factor=configuration('PV_efficiency'))
                    # _, phi_collectors = pv_system.solar_gain()
                    # production = phi_collectors['PV']
                    total_production_in_kWh = sum(productions_in_kWh)
                    if total_production_in_kWh > best_total_production_in_kWh:
                        improvement = True
                        best_exposure_slope_in_deg: tuple[float, float] = exposure_slope_in_deg_candidate
                        best_productions_in_kWh = productions_in_kWh
                        best_total_production_in_kWh: float = sum(best_productions_in_kWh)
        return best_productions_in_kWh, best_exposure_slope_in_deg[0], best_exposure_slope_in_deg[1]
    

class DefaultConfiguration(Configuration):

    def __init__(self, weather_file: str, location: str, latitude: float, longitude: float, skyline: list[tuple[float, float]]=[(-180,0),(180,0)], insulation = 'polystyrene') -> None:
        super().__init__()
        
        self.section('site')
        self(weather_file_name = weather_file)
        self(location = location)
        self(latitude = latitude)
        self(longitude = longitude)
        self(weather_year=2019)
        self(albedo=0.1)
        self(skyline = skyline)
        
        self.section('house')
        self(total_living_surface = 100)
        self(height_per_floor = 3)
        self(shape_factor = 1, parametric = [.5, .75, 1, 1.25, 1.5, 1.75, 2])
        self(number_of_floors = 1, parametric=[1,2,3])
        self(wall_composition_in_out = [('plaster', 13e-3), ('polystyrene', 30e-2), ('concrete', 13e-2)])
        self(roof_composition_in_out = [('plaster', 13e-3), ('polystyrene', 30e-2), ('concrete', 13e-2)])
        self(glass_composition_in_out = [('glass', 5e-3), ('air', 12e-3), ('glass', 5e-3)])
        self(ground_composition_in_out = [('concrete', 13e-2), ('polystyrene', 30e-2), ('gravels', 20e-2)])
        self.insulation = insulation
        self(polystyrene = 40e-2, parametric = [0, 5e-2, 10e-2, 15e-2, 20e-2, 25e-2, 30e-2, 35e-2, 40e-2])
        self.section('windows')
        self(offset_exposure = 0, parametric = [offset_exposure for offset_exposure in range(-45, 45, 5)])
        self(glazing = {'north': 0.1, 'west': 0.1, 'east': 0.1, 'south': 0.1}, parametric = [0, .2, .4, .6, .8])
        self(solar_factor = 0.3)
        self(south_solar_protection_angle = 50, parametric = [30, 35, 40, 45, 50, 55, 60])
        
        self.section('HVAC and photovoltaic (PV) systems')
        self(heating_setpoint = 21, parametric = [18, 19, 20, 22, 23])
        self(delta_temperature_absence_mode = 3, parametric = [0, 1, 2, 3, 4])
        self(cooling_setpoint = 26, parametric = [23, 24, 25, 27, 28, 29])
        self(winter_hvac_trigger_temperature = 16)
        self(summer_hvac_trigger_temperature = 24)
        self(hvac_hour_delay_for_trigger = 24)
        self(inertia_level=15, parametric = [3, 6, 9, 12, 15, 18, 21, 24])
        self(hvac_COP = 3)
        self(PV_surface = 10, parametric = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22])
        self(final_to_primary_energy_coefficient=2.54)
        self(air_renewal_presence = 2, parametric = [.5, 1, 2, 3])  # in vol/h
        self(air_renewal_absence = .1)
        self(ventilation_heat_recovery_efficiency = 0.8, parametric = [0, .25, .5, .7, .9])
        self(PV_efficiency = 0.20)
        
        self.section('inhabitants')
        self(occupancy_schema = { (1, 2, 3, 4, 5): {(18, 8): 3, (8, 18): 0}, (6, 7): {(0, 24): 2} })  # days of weeks (1=Monday,...), period (start. hour, end. hour) : avg occupancy
        self(average_occupancy_electric_gain = 50)
        self(average_occupancy_metabolic_gain = 100)
        self(average_permanent_electric_gain = 200)
        self(air_renewal_overheat_threshold = 26)
        self(air_renewal_overheat = 5)


class GrenobleConfiguration(DefaultConfiguration):

    def __init__(self) -> None:
        # super().__init__(weather_file='grenoble1979-2022.json', location='Grenoble', latitude=45.186737, longitude=5.736511, skyline = [(-180.0,13.8), (-170.0,18.9), (-145.1,9.8), (-120.0,18.3), (-96.1,17.3), (-60.8,6.2), (-14.0,2.6), (-8.4,5.6), (0.8,2.6), (21.6,5.5), (38.1,14.6), (49.4,8.9), (60.1,11.3), (87.4,10.4), (99.3,12.0), (142.1,2.6), (157.8,4.0), (175.1,17.1), (180.0,15.9)])
        super().__init__(weather_file='Grenoble-INP1990.json', weather_year=2022, location='Grenoble', albedo=0.1, pollution=0.1, latitude=45.186737, longitude=5.736511, skyline = [(-180.0,13.8), (-170.0,18.9), (-145.1,9.8), (-120.0,18.3), (-96.1,17.3), (-60.8,6.2), (-14.0,2.6), (-8.4,5.6), (0.8,2.6), (21.6,5.5), (38.1,14.6), (49.4,8.9), (60.1,11.3), (87.4,10.4), (99.3,12.0), (142.1,2.6), (157.8,4.0), (175.1,17.1), (180.0,15.9)])
        

class BarcelonnetteConfiguration(DefaultConfiguration):

    def __init__(self) -> None:
        super().__init__(weather_file='parin1979_2022.json', location='Barcelonnette', latitude=44.387359, longitude=6.651733, skyline = [(-180,17.81), (-159,17.81), (-127.1,7.5), (-111.2,7.5), (-90,7.5), (-74.2,10.31), (-34.4,10.31), (-19,17.8), (0,11.2), (5,11.2), (29, 7.5),(47.6,10.3), (82,3.75), (103.2,6.6), (116.5,11.25), (140.3,11.25), (180,16.875)])
    
class Meolans(DefaultConfiguration):

    def __init__(self) -> None:
        super().__init__(weather_file='parin1979_2022.json', location='Meolans', latitude=44.399190, longitude=6.497175, skyline =  [(0-180, 17), (45-180, 14), (80-180, 13), (100-180, 14), (120-180, 5),(140-180, 25), (220-180, 15), (225-180, 13), (250-180, 12), (260-180, 13), (270-180, 12), (300-180, 28), (315-180, 28), (330-180, 17), (360-180, 16)])
        
class StGermainConfiguration(DefaultConfiguration):

    def __init__(self) -> None:
        super().__init__(weather_file='parin1979_2022.json', location="Saint-Germain-au-Mont-d'Or", latitude=45.884843, longitude=4.801576, skyline = [(-180.0, 0),(5-180,0), (225-180, 10), (180.0, 0)])
        
class CairoConfiguration(DefaultConfiguration):
    
    def __init__(self):
        super().__init__(weather_file='parin1979_2022.json', location="Cairo", latitude=30.069648, longitude=31.255973, skyline = [(-180.0, 0), (180.0, 0)])
        
class BrianconConfiguration(DefaultConfiguration):
    
    def __init__(self):
        super().__init__(weather_file='briancon.json', location=None, latitude=44.901334, longitude=6.644723, skyline = [(-180,25), (22-180,15), (150-180,10), (210-180,5), (250-180,12), (290-180,7), (340-180,20), (180,20)])
        
class TiranaConfiguration(DefaultConfiguration):
    
    def __init__(self):
        super().__init__(weather_file='tirana.json', location="Tirana", latitude=41.330815, longitude=19.819229, skyline = [(-180.0, 5), (50-180,7), (130-180,5), (260-180,3), (355-180,2), (180.0, 0)])
        
class CampusTransitionConfiguration(DefaultConfiguration):
    
    def __init__(self):
        super().__init__(weather_file='campus_transition_alpespace.json', location="Campus de la Transition", latitude=48.419742, longitude=2.962580, skyline = [(-180.0, 0), (180.0, 0)])
        
if __name__ == '__main__':
    starting_time = time.time()
    configuration: GrenobleConfiguration = GrenobleConfiguration()
    experiment = Experiment(configuration, on_screen=False)
    analysis = Analysis(experiment)  # 1
    analysis.climate(experiment)  # 2
    analysis.evolution(experiment)  # 3
    analysis.solar(experiment)  # 4
    analysis.house(experiment)  # 5
    analysis.neutrality(experiment)  # 6
    experiment.close()  #7
    print('duration: %i minutes' % (int(round((time.time() - starting_time ) / 60))))
