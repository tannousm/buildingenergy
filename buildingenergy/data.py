"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

A module that contains a data container of on-site measurements.

The module is a container for 1h sampled measurement data loaded from a csv file, and from any
open weather file related to the site under study. It can be connected to the solar module to the
generate data for solar irradiation taking into account weather (mostly cloudiness) and masks.

The module aims at generating useful data for the other modules like total heat gain or occupancy.
"""
from __future__ import annotations
import ipywidgets
from typing import Literal
from IPython.display import display
import configparser
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime, timedelta
import warnings
import pandas
from buildingenergy.parameters import ParameterSet
from buildingenergy import timemg
from buildingenergy.openweather import SiteWeatherData, WeatherJsonReader
import plotly
import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express
warnings.simplefilter("ignore", category=FutureWarning)

config = configparser.ConfigParser()
config.read('setup.ini')


def in_jupyter() -> bool:
    from IPython import get_ipython
    if get_ipython().__class__.__name__ != 'NoneType':
        return True
    return False


if in_jupyter():
    plotly.offline.init_notebook_mode()
    from plotly.offline import iplot
else:
    import matplotlib
    matplotlib.use('tkagg')
    from matplotlib.backends.backend_tkagg import *  # noqa
    import tkinter as tk


def minmaxval(sequence: list[float]) -> tuple[float, float]:
    i: int = 0
    while i < len(sequence) and sequence[i] is None:
        i += 1
    if i < len(sequence):
        _minval = sequence[i]
        _maxval = sequence[i]
        for j in range(i+1, len(sequence)):
            if sequence[j] is not None:
                if sequence[j] < _minval:
                    _minval = sequence[j]
                elif sequence[j] > _maxval:
                    _maxval = sequence[j]
        return _minval, _maxval
    else:
        return None, None


class VariableSet:
    """It gathers measurement data corresponding to dates with a regular sample time. A Data container gives an easy access to them and make it possible to plot or save them. Indeed, data whose length is equal to the number of hours of the data container, can be added at any time and analyzed at the end. Data can come from measurements or deduced from a parameter-dependent formula applying to measurements"""

    def __init__(self, csv_measurement_filename: str, json_openweather_filename: str, starting_stringdate: str = None, ending_stringdate: str = None, albedo: float = .1, pollution: float = 0.1, location: str = None, deleted_variables: tuple[str] = (), number_of_levels: int = 4, verbose: bool = False):
        """Create a data container by collecting data from a csv file.

        :param csv_measurement_filename: name of the csv file containing measurement data with different format of date in the 3 first columns (string like '15/02/2015 00:00:00' for the first one, epochtime in ms for the second one, and datetime.datetime for the 3rd one). The first row is used as name for the data of the related column. The file with the provided name will be search in the data folder defined in the setup.ini file in the project root folder. Data must be organized in ascending order.
        :type csv_measurement_filename: str
        :param initial_string_date: initial date in format 'dd/mm/YYYY' or None. If None the first date of the file starting at 0:00:00 time will be selected, default to None, optional
        :type initial_string_date: str
        :param final_string_date: final date in format 'dd/mm/YYYY' or None. If None the latest date of the file starting at 23:00:00 time will be selected, default to None, optional
        :type final_string_date: str
        """
        self.sample_time_in_secs: int = 3600
        self.variable_bounds: dict[str, tuple[float, float]] = dict()
        self.site_weather_data: None = None
        self.number_of_levels: int = number_of_levels
        self._variable_levels: dict[str, list[int]] = dict()

        if csv_measurement_filename is not None:
            self._dataframe = pandas.read_csv(config['folders']['data'] + csv_measurement_filename, dialect='excel', parse_dates=['datetime'])
            self._dataframe = self._dataframe.astype({'epochtime': 'int64', 'stringtime': 'str', 'datetime': 'datetime64[ns]'})
            self._dataframe = self._dataframe.astype({v: float for v in self._dataframe.columns[3:]})

            if verbose:
                print('variables read from measurement file:')
                for column_name in self._dataframe.columns:
                    print(column_name, end=', ')
                print()
            if deleted_variables is not None:
                for column_name in self._dataframe.columns:
                    if column_name in deleted_variables and column_name not in ('stringtime', 'epochtime', 'datetime'):
                        self._dataframe.drop(column_name, inplace=True, axis=1)
                        if verbose:
                            print('Variable %s has been removed' % column_name)
            min_values: float = self._dataframe.min()
            max_values: float = self._dataframe.max()
            for i, column_name in enumerate(self._dataframe.columns):
                self.variable_bounds[column_name] = (min_values.iloc[i], max_values.iloc[i])  # type: ignore

            if starting_stringdate is not None:
                self.starting_stringdatetime = starting_stringdate + " 0:00:00"
                starting_datetime = timemg.stringdate_to_datetime(self.starting_stringdatetime, date_format='%d/%m/%Y %H:%M:%S')
                self._dataframe.drop(self._dataframe[self._dataframe['datetime'] < starting_datetime].index, inplace=True)
            else:
                i: int = 0
                while not (self._dataframe['stringtime'][i].split(' ')[1] == '0:00:00' or self._dataframe['stringtime'][i].split(' ')[1] == '00:00:00'):
                    if verbose:
                        print(self._dataframe['stringtime'][i])
                    i += 1
                if i != 0:
                    self._dataframe: pandas.dataframe = self._dataframe[i:]
                self.starting_stringdatetime = self._dataframe['stringtime'].iloc[0]

            if ending_stringdate is not None:
                self.ending_stringdatetime = ending_stringdate + " 23:00:00"
                ending_datetime: datetime = timemg.stringdate_to_datetime(self.ending_stringdatetime, date_format='%d/%m/%Y %H:%M:%S')
                ending_datetime += timedelta(hours=1)
                self._dataframe.drop(self._dataframe[self._dataframe['datetime'] > ending_datetime].index, inplace=True)
            else:
                i: int = len(self._dataframe)-1
                while self._dataframe['stringtime'][i].split(' ')[1] != '23:00:00':
                    i -= 1
                if i != len(self._dataframe)-1:
                    self._dataframe = self._dataframe[:i+1]
                self.ending_stringdatetime = self._dataframe['stringtime'].iloc[-1]
            openweather_reader: WeatherJsonReader = WeatherJsonReader(json_openweather_filename, self.starting_stringdate, self.ending_stringdate, albedo, pollution, location)
            self.site_weather_data = openweather_reader.site_weather_data
        else:
            openweather_file = WeatherJsonReader(json_openweather_filename,  starting_stringdate, ending_stringdate, albedo=albedo, pollution=pollution, location=location)
            self.site_weather_data = openweather_file.site_weather_data
            self.starting_stringdatetime = self.site_weather_data.from_stringdate
            self.ending_stringdatetime = self.site_weather_data.to_stringdate
            data = dict()
            data['datetime'] = self.site_weather_data.get('datetime')
            data['stringtime'] = self.site_weather_data.get('stringdate')
            data['epochtime'] = [int(timemg.datetime_to_epochtimems(dt) / 1000) for dt in data['datetime']]
            self._dataframe = pandas.DataFrame.from_dict(data, orient='columns')

        self._dataframe.reset_index(inplace=True, drop=True)
        self._dataframe['day_of_year'] = self._dataframe['datetime'].dt.day_of_year
        self._dataframe['day_of_week'] = self._dataframe['datetime'].dt.day_of_week
        self._dataframe['hour'] = self._dataframe['datetime'].dt.hour
        self._dataframe['year'] = self._dataframe['datetime'].dt.year

        self.day_counter: int = -1
        _current_day: int = -1
        day_index: list[int] = list()
        hour_index: list[int] = list()
        for i in self._dataframe.index:
            hour_index.append(i)
            if self._dataframe['day_of_year'][i] != _current_day:
                self.day_counter += 1
                _current_day: int = self._dataframe['day_of_year'][i]
            day_index.append(self.day_counter)
        self.variable_bounds['day_of_year'] = (0, self.day_counter)
        self.variable_bounds['day_of_week'] = (self._dataframe['day_of_week'].min(), self._dataframe['day_of_week'].max())
        self.variable_bounds['hour'] = (self._dataframe['hour'].min(), self._dataframe['hour'].max())
        self.variable_bounds['year'] = (self._dataframe['year'].min(), self._dataframe['year'].max())
        self.add_external_variable('hour_index', hour_index)
        self.add_external_variable('day_index', day_index)
        self.datetime_pd_series: pandas.TimestampSeries = pandas.to_datetime(self._dataframe["datetime"])
        self.starting_stringdatetime: str = self._dataframe['stringtime'][0]
        self.ending_stringdatetime: str = self._dataframe['stringtime'][len(self._dataframe)-1]

        print('Weather data variables:')
        for variable_name in self.site_weather_data.variable_names:
            if variable_name not in deleted_variables and variable_name != "epochtimems":
                values = self.site_weather_data.get(variable_name)
                self.add_external_variable('weather_' + variable_name, values)
                self.variable_bounds[variable_name] = minmaxval(values)
                print('weather_' + variable_name, end=', ')
        print()

    def value_to_level(self, variable_name: str, variable_value: float) -> int:
        if variable_name in self.variable_bounds:
            return ((self.number_of_levels - 1) * (variable_value - self.variable_bounds[variable_name][0])) // (self.variable_bounds[variable_name][1] - self.variable_bounds[variable_name][0])
        else:
            return 0

    def level_to_value(self, variable_name: str, level: int) -> bool:
        return self.variable_bounds[variable_name][0] + level / (self.number_of_levels - 1) * (self.variable_bounds[variable_name][1] - self.variable_bounds[variable_name][0])

    def levels(self, variable_names: str | list[str], k: int = None) -> list[list[int]] | list[int]:
        if type(variable_names) is not list:
            variable_names = list(variable_names)
        for variable_name in variable_names:
            if variable_name not in self._variable_levels:
                self._variable_levels[variable_name] = [self.value_to_level(variable_name, values) for values in self(variable_name)]
        _levels = list()
        for variable_name in variable_names:
            if k is not None:
                _levels.append(self._variable_levels[variable_name][k])
            else:
                _levels.append(self._variable_levels[variable_name])
        return _levels

    def bounds(self, variable_name: str) -> tuple[float, float]:
        return self.variable_bounds[variable_name]

    def __contains__(self, data_name: str) -> bool:
        return data_name in self._dataframe.columns

    @property
    def starting_stringdate(self) -> str:
        return self.starting_stringdatetime.split(' ')[0]

    @property
    def ending_stringdate(self) -> str:
        return self.ending_stringdatetime.split(' ')[0]

    @property
    def number_of_variables(self) -> int:
        return len(self._dataframe.columns) - 3

    @property
    def variable_names(self) -> list[str]:
        return [self._dataframe.columns[i] for i in range(3, len(self._dataframe.columns))]

    def add_external_variable(self, name: str, values: list[float]):
        """Use to add a invariant series of values to the container. It will appears as any other measurements. The number of values must correspond to the number of hours for the data container.

        :param label: name of the series of timed values (each value corresponds to 1 hour)
        :type label: str
        :param values: series of values but it must be compatible with the times which are common to all series
        :type values: list[float]
        """
        try:
            self._dataframe[name] = values
        except:  # noqa
            print('Variable "%s" has a length equal to %i instead of %i' % (name, len(values), len(self)))
            exit(-1)
        self.variable_bounds[name] = minmaxval(values)
        self._dataframe = self._dataframe.astype({name: float})

    @property
    def weather_data(self) -> SiteWeatherData:
        return self.site_weather_data

    @property
    def number_of_days(self) -> int:
        return self.day_counter

    def __call__(self, variable_name: str, k: int = None) -> list[float]:
        """
        Functor used to get data defined by its name that could be:
        - for invariant data: for a single hour, for a single day (24 values) or all the data in the container (the length is therefore equal to the number of hours)
        - for parameterized data: the only option is to request for a single hour

        :param variable_name: name of the data
        :type variable_name: str
        :param hour_index: index of the hour in the data series (get the priority on day index if provided), defaults to None
        :type hour_index: int, optional
        :param day_index: index of the day return 24 values corresponding to the day index but only if no hour index are given, defaults to None
        :type day_index: int, optional
        :raises ValueError: error raised when the data name is not in the list
        :return: the requested values
        :param number_of_invariant_variables:
        :rtype: dict[str]
        """
        if k is None:
            if variable_name == "datetime":
                return self.datetime_pd_series.tolist()
            elif variable_name in self._dataframe:
                return self._dataframe[variable_name].values.tolist()
        else:
            if variable_name == "datetime":
                return self.datetime_pd_series.iloc[[k]]
            elif variable_name in self._dataframe:
                return self._dataframe.iloc[k][variable_name]
        raise ValueError('Unknown data "%s"' % variable_name)

    def __len__(self) -> int:
        return len(self._dataframe)

    def __str__(self) -> str:
        """Make it possible to print a data container.

        :return: description of the data container
        :rtype: str
        """
        string: str = '__________\nData cover period from %s to %s with time period: %d seconds\n' % (
            self.starting_stringdatetime, self.ending_stringdatetime, self.sample_time_in_secs)
        string += '%i levels/values are considered to represent nonlinearities\n' % self.number_of_levels
        string += '- Available invariant variables: ' + ', '.join(self.variable_set.variable_names)
        return string

    def save(self, file_name: str = 'results.csv', selected_variables: list[str] = None):
        if selected_variables is None:
            _selected_variables = self._dataframe.columns
        else:
            _selected_variables: list[str] = ['epochtime', 'stringtime', 'datetime']
            _selected_variables.extend(selected_variables)
        file_name = config['folders']['results'] + file_name
        self._dataframe[_selected_variables].to_csv(file_name, index=False)
        print('Following variables have been saved into file "%s": ' % file_name, end='')
        for selected_variable in _selected_variables:
            print(selected_variable, end=', ')
        print()


class ParameterizedVariableSet:
    """It manages data calculated from model parameters and raw data"""

    def __init__(self, data_provider: DataProvider) -> None:
        """Initialize the set with double dependency with the data provider thanks to a formula

        :param data_provider: the main data container that gathers all the data, including the parameterized ones
        :type data_provider: DataProvider
        """
        self.data_provider: DataProvider = data_provider
        self.formulas: dict[str, function] = dict()  # noqa
        self.parameterized_variable_names = list()
        self.data_provider.register_parameterized_dataset(self)

        self.all_required_variable_names = list()
        self.all_required_parameter_names = list()
        self.required_variable_names_dict: dict[str, list[str]] = dict()
        self.required_parameter_names_dict: dict[str, list[str]] = dict()

        self._variable_levels: dict[str, list[int]] = dict()

    def __str__(self) -> str:
        string = '- Parameterized Variable containing: '
        for name in self.parameterized_variable_names:
            string += '%s(%s|%s), ' % (name, ','.join(self.required_variable_names_dict[name]), ','.join(self.required_parameter_names_dict[name]))
        return string

    def add(self, parameterized_data_name: str, formula: function, *required_data_names: tuple[str]) -> None:  # noqa
        """add a parameterized variable to the data set

        :param name: name of the parameterized variable
        :type name: str
        :param formula: calculation formula
        :type formula: function like f(parameters: ParameterSet, data: DataSet) -> list[float], list[]
        """
        self.parameterized_variable_names.append(parameterized_data_name)
        self.formulas[parameterized_data_name] = formula
        for required_data_name in required_data_names:
            if required_data_name in self.data_provider.variable_set.variable_names:
                self.all_required_variable_names.append(required_data_name)
                self.required_variable_names_dict[parameterized_data_name] = required_data_name
            elif required_data_name in self.data_provider.parameter_names:
                self.all_required_parameter_names.append(required_data_name)
                self.required_parameter_names_dict[parameterized_data_name] = required_data_name

    def __contains__(self, variable_name: str) -> bool:
        return variable_name in self.parameterized_variable_names

    def __call__(self, parameterized_variable_name: str, k: int = None) -> list[float]:
        if parameterized_variable_name in self.parameterized_variable_names:
            if k is not None:
                return self.formulas[parameterized_variable_name](k)
            else:
                return [self.formulas[parameterized_variable_name](k) for k in range(len(self.data_provider))]
        raise ValueError('Unknown data "%s"' % parameterized_variable_name)

    def __iter__(self):
        return self.parameterized_variable_names.__iter__()

    def __next__(self):
        return self.parameterized_variable_names.__next__()

    @property
    def number_of_parameterized_variables(self) -> int:
        return len(self.parameterized_variable_names)


class DataProvider:

    def __init__(self, parameters: ParameterSet, variables: VariableSet, bindings: Bindings) -> None:
        self.parameter_set: ParameterSet = parameters
        self.parameter_names = list(self.parameter_set._parameter_names)

        self.variable_set: VariableSet = variables
        self.sample_time_in_secs = self.variable_set.sample_time_in_secs

        self.parameterized_variable_set: ParameterizedVariableSet = None
        self.parameterized_variable_names = None
        # self.extended_data_names = list(self.variable_set.variable_names)

        self.bindings: Bindings = bindings
        self.data_overload: dict[str, dict[int, float]] = dict()
        self.k = 0

    def set_k(self, k: int) -> None:
        self.k = k

    def register_parameterized_dataset(self, parameterized_variable_set: ParameterizedVariableSet) -> None:
        self.parameterized_variable_set = parameterized_variable_set
        self.parameterized_variable_names: list[str] = parameterized_variable_set.parameterized_variable_names
        self.parameterized_variable_set.data_provider = self
        self.parameterized_data_names = parameterized_variable_set.parameterized_variable_names

    def reset_variable_set(self, data: VariableSet) -> None:
        self.variable_set = data

    def overload_data(self, data_name: str, value: float | list[float]) -> None:
        if type(value) is list:
            self.data_overload[data_name] = dict()
            for i, value in enumerate(value):
                self.data_overload[data_name][i] = value[i]
        elif data_name not in self.data_overload:
            if type(value) is float:
                self.data_overload[data_name] = {self.k: value}
        else:
            self.data_overload[data_name][self.k] = value

    def clear_overloaded_data(self, data_name: str = None) -> None:
        if data_name is None:
            self.data_overload.clear()
        elif data_name in self.data_overload:
            self.data_overload[data_name].clear()

    def reset_parameters(self, parameters: ParameterSet) -> None:
        parameters.reset()
        self.parameter_set = parameters
        self.parameterized_variable_set.set_parameters(parameters)

    def __call__(self, data_name: str, k: int = None, all: bool = False) -> float:
        if k is not None:
            self.k: int = k
        if data_name == 'datetime':
            datetimes = self.variable_set._dataframe.datetime.tolist()
            if all:
                return datetimes
            else:
                return datetimes[self.k]

        if data_name in self.bindings:
            synonym: str = self.bindings.synonym(data_name)

        if data_name in self.data_overload:
            if not all:
                return self.data_overload[data_name][self.k]
            else:
                __vals: list[float] = [0 for _ in range(len(self.variable_set))]
                for k in self.data_overload[data_name]:
                    __vals[k] = self.data_overload[data_name][k]
                return __vals
        elif data_name in self.bindings and synonym in self.data_overload:
            if not all:
                return self.data_overload[synonym][self.k]
            else:
                __vals: list[float] = [0 for _ in range(len(self.variable_set))]
                for k in self.data_overload[synonym]:
                    __vals[k] = self.data_overload[synonym][k]
                return __vals

        if data_name in self.parameter_set:
            if not all:
                return self.parameter_set(data_name)
            else:
                return [self.parameter_set(data_name) for _ in range(len(self.variable_set))]
        elif data_name in self.bindings and synonym in self.parameter_names:
            if not all:
                return self.parameter_set(synonym)
            else:
                return [self.parameter_set(synonym) for _ in range(len(self.variable_set))]

        if data_name in self.variable_set.variable_names:
            if not all:
                return self.variable_set(data_name, self.k)
            else:
                return self.variable_set(data_name, None)
        elif data_name in self.bindings and synonym in self.variable_set.variable_names:
            if not all:
                return self.variable_set(synonym, self.k)
            else:
                return self.variable_set(synonym, None)

        if data_name in self.parameterized_variable_names:
            if not all:
                return self.parameterized_variable_set(data_name, self.k)
            else:
                return [self.parameterized_variable_set(data_name, k) for k in range(len(self.variable_set))]
        elif data_name in self.bindings and synonym in self.parameterized_variable_names:
            if not all:
                return self.parameterized_variable_set(synonym, self.k)
            else:
                return [self.parameterized_variable_set(synonym, k) for k in range(len(self.variable_set))]
        raise ValueError('Unknown data "%s"' % data_name)

    @property
    def starting_stringdate(self) -> str:
        return self.variable_set.starting_stringdate

    @property
    def ending_stringdate(self) -> str:
        return self.variable_set.ending_stringdate

    @property
    def starting_stringdatetime(self) -> str:
        return self.variable_set.starting_stringdatetime

    @property
    def ending_stringdatetime(self) -> str:
        return self.variable_set.ending_stringdatetime

    def __len__(self):
        return self.variable_set.__len__()

    def fingerprint(self, k: int = None) -> int | list[int]:
        """fingerprint based on the adjustable parameter values and the required variables for parameterized variables

        :param k: the time sample or all the time samples, defaults to None
        :type k: int, optional
        :return: list of hash codes (if k is None), or a single hash code (if k is specified) representing the adjustable parameter values and the required variables for parameterized variables
        :rtype: int | list[int]
        """
        if k is not None:
            k_min: int = k
            k_max: int = k + 1
        else:
            k_min: int = 0
            k_max: int = len(self)
        _fingerprint: list[int] = list()
        for k in range(k_min, k_max):
            _levels: list[int] = list()
            _levels.extend(self.variable_set.levels(self.parameterized_variable_set.all_required_variable_names, k))
            _levels.extend(self.parameter_set.levels())
            _fingerprint.append(hash(tuple(_levels)))
        if k is not None:
            return _fingerprint[0]
        else:
            return _fingerprint

    def add_external_variable(self, name: str, values: list[float]):
        self.variable_set.add_external_variable(name, values)

    def bounds(self, variable_name: str) -> tuple[float, float]:
        if variable_name not in self.variable_set.variable_names:
            variable_name = self.bindings.synonym(variable_name)
        if self.datatype(variable_name) == 'data':
            return self.variable_set.bounds(variable_name)
        elif self.datatype(variable_name) == 'parameterized':
            return minmaxval(self(variable_name, None))
        elif self.datatype(variable_name) == 'parameter':
            if variable_name in self.parameter_set.adjustable_parameter_names:
                return self.parameter_set.adjustable_parameter_bounds
            else:
                return (self.parameter_set(variable_name), self.parameter_set(variable_name))
        return None

    def datatype(self, variable_name: str) -> Literal['parameter', 'data', 'parameterized']:
        if variable_name not in self.variable_set.variable_names:
            variable_name = self.bindings.synonym(variable_name)
        if variable_name is not None and variable_name in self.variable_set:
            return 'data'
        elif variable_name is not None and variable_name in self.parameterized_variable_set:
            return 'parameterized'
        elif variable_name in self.parameter_set:
            return 'parameter'
        else:
            raise ValueError('Value of %s cannot be found' % (variable_name))

    def _plot_selection(self, int_vars: list) -> None:
        """Use to plot curve using plotly library

        :param int_vars: reference to the variable to be plotted
        :type: list[int]
        """
        title: str = 'from %s to %s' % (self.starting_stringdatetime, self.ending_stringdatetime)
        dataframe_dict: dict[str, list[float]] = {'datetime': self.variable_set('datetime')}
        variables_to_plot: list[str] = list()
        _variable_names: list[str] = list(self.variable_set.variable_names)
        _variable_names.extend(self.parameterized_data_names)
        for i in range(len(int_vars)):
            if int_vars[i].get():
                variable_name: str = _variable_names[i]
                variables_to_plot.append(variable_name)
                dataframe_dict[variable_name] = self(variable_name, all=True)
                int_vars[i].set(0)
        dataframe: pandas.DataFrame = pandas.DataFrame.from_dict(dataframe_dict)
        plotly.express.line(dataframe, x="datetime", y=variables_to_plot, title=title).show()

    def plot(self, heatmap: bool = False) -> None:
        if heatmap:
            plotted_data: list[list[float]] = [self(v, all=True) for v in self.variable_set.variable_names]
            for i in range(len(plotted_data)):
                min_data, max_data = min(plotted_data[i]), max(plotted_data[i])
                plotted_data[i] = [100*(plotted_data[i][j]-min_data) / (max_data - min_data) if max_data != min_data else 0 for j in range(len(plotted_data[i]))]

            fig = plotly.express.imshow(plotted_data, labels=dict(x="date", y="measurement", color="no data"), x=self('datetime', all=True), y=self.variable_set.variable_names, height=1000)
            fig.layout.coloraxis.showscale = False
            fig.update_xaxes(side="top")
            iplot(fig)
        elif not in_jupyter():
            tk_variables = list()
            tk_window = tk.Tk()
            tk_window.wm_title('variable plotter')
            tk.Button(tk_window, text='plot', command=lambda: self._plot_selection(tk_variables)).grid(row=0, column=0, sticky=tk.W + tk.E)
            frame: tk.Frame = tk.Frame(tk_window).grid(row=1, column=0, sticky=tk.N + tk.S)
            vertical_scrollbar: tk.Scrollbar = tk.Scrollbar(frame, orient=tk.VERTICAL)
            vertical_scrollbar.grid(row=1, column=1, sticky=tk.N + tk.S)
            canvas: tk.Canvas = tk.Canvas(frame, width=400, yscrollcommand=vertical_scrollbar.set)
            tk_window.grid_rowconfigure(1, weight=1)
            canvas.grid(row=1, column=0, sticky='news')
            vertical_scrollbar.config(command=canvas.yview)
            checkboxes_frame = tk.Frame(canvas)
            checkboxes_frame.rowconfigure(1, weight=1)

            _variable_names: list[str] = self.variable_set.variable_names
            for i in range(len(_variable_names)):
                tk_variable = tk.IntVar()
                tk_variables.append(tk_variable)
                tk.Checkbutton(checkboxes_frame, text=_variable_names[i], variable=tk_variable, offvalue=0).grid(row=(i), sticky=tk.W)
            canvas.create_window(0, 0, window=checkboxes_frame)
            checkboxes_frame.update_idletasks()
            canvas.config(scrollregion=canvas.bbox('all'))
            tk_window.geometry(str(tk_window.winfo_width()) + "x" + str(tk_window.winfo_screenheight()))
            tk_window.mainloop()
        else:
            selector = ipywidgets.SelectMultiple(options=[variable_name for variable_name in self.variable_set.variable_names], description='Select variable to plot', disable=False)
            time_data = self('datetime', all=True)

            button = ipywidgets.Button(description='Plot')
            output = ipywidgets.Output()
            display(selector, button, output)

            def on_button_clicked(button):
                output.clear_output()
                with output:
                    fig = make_subplots(rows=1, cols=1, shared_xaxes=False)
                    for variable in selector.value:
                        fig.add_trace(go.Scatter(x=pandas.to_datetime(pandas.Series(time_data), format='%b_%d'), y=self(variable, all=True), name=variable, line_shape='hv'), row=1, col=1)
                    iplot(fig)

            button.on_click(on_button_clicked)

    def __contains__(self, variable_name: str) -> bool:
        return variable_name in self.variable_set or variable_name in self.parameter_set or variable_name in self.parameterized_variable_set or variable_name in self.bindings

    def __str__(self):
        string: str = ''
        if self.variable_set is not None:
            string += self.variable_set.__str__() + '\n'
        if self.parameterized_variable_set is not None:
            string += self.parameterized_variable_set.__str__() + '\n'
        if self.parameter_set is not None:
            string += self.parameter_set.__str__() + '\n'
        if self.bindings is not None:
            string += '\nwith the following data to model bindings:\n'
            string += self.bindings.__str__()
        return string


class PlotSaver:

    def __init__(self, data: VariableSet):
        self.data = data

    def time_plot(self, variable_names: list, filename: str):
        """
        generate time plot
        :param selected_variable_$names: full names (ie $name) of the variables to be plot
        """
        styles: tuple[str, str, str, str] = ('-', '--', '-.', ':')
        linewidths = (3.0, 2.5, 2.5, 1.5, 1.0, 0.5, 0.25)
        figure, axes = plt.subplots()
        axes.set_title('from %s to %s' % (self.data.starting_stringdatetime, self.data.ending_stringdatetime))
        text_legends = list()
        for i in range(len(variable_names)):
            style = styles[i % len(styles)]
            linewidth = linewidths[i // len(styles) % len(linewidths)]
            time_data = list(self.data('datetime', None))
            variable_name = variable_names[i]
            variable_data = list(self.data(variable_name, None))
            if len(time_data) > 1:
                time_data.append(time_data[-1] + (time_data[-1] - time_data[-2]))
                variable_data.append(variable_data[-1])
            axes.step(time_data, variable_data, linewidth=linewidth, linestyle=style, where='post')
            axes.set_xlim([time_data[0], time_data[-1]])
            text_legends.append(variable_names[i])
        axes.legend(text_legends, loc=0)
        axes.xaxis.set_minor_locator(mdates.DayLocator())
        axes.fmt_xdata = mdates.DateFormatter('%d/%m/%Y %H:%M')
        plt.gcf().autofmt_xdate()
        axes.grid(True)
        plt.savefig(filename+'.png')


class Bindings:

    def __init__(self) -> None:
        self.__variable_model_data_bindings: dict[str, str] = dict()
        self.__variable_data_model_bindings: dict[str, str] = dict()
        self.__synonyms: set[str] = set()

    def add_synonyms(self, model_name: str, data_name: str) -> None:
        if model_name in self.__synonyms or data_name in self.__synonyms:
            raise ValueError('Synonym is already existing for variable %s' % model_name)
        self.__variable_model_data_bindings[model_name] = data_name
        self.__variable_data_model_bindings[data_name] = model_name
        self.__synonyms.add(model_name)
        self.__synonyms.add(data_name)

    def __contains__(self, synonym):
        return synonym in self.__synonyms

    def synonym(self, variable_name: str) -> str:
        if variable_name in self.__variable_model_data_bindings:
            return self.__variable_model_data_bindings[variable_name]
        elif variable_name in self.__variable_data_model_bindings:
            return self.__variable_data_model_bindings[variable_name]
        return None

    def __str__(self) -> str:
        string: str = ''
        for variable_name1 in self.__variable_model_data_bindings:
            variable_name2 = self.__variable_model_data_bindings[variable_name1]
            string += "%s <=> %s\n" % (variable_name1, variable_name2)
        return string
