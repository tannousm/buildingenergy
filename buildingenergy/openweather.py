"""This is a reader for openweathermap (https://openweathermap.org) historical weather files.

Author: stephane.ploix@grenoble-inp.fr
"""
from __future__ import annotations
from buildingenergy import timemg
import json
import requests
import configparser
import os, glob, shutil, re
import webbrowser


config = configparser.ConfigParser()
config.read('setup.ini')


class ElevationRetriever:
    """create or get elevations from google website corresponding to a localization characterized by a longitude and latitude. To avoid offline issues with Internet, anytime an elevation is collected, it is stored into the localizations.json file in order to use it a next time.
    """

    def __init__(self, json_database_name: str = 'localizations.json') -> None:
        """initializer of the elevation retriever
        :param json_database_name: name of the file where longitudes, latitudes and elevations are saved, defaults to 'localizations.json'
        :type json_database_name: str, optional
        """
        filename: str = config['folders']['data'] + json_database_name
        self.json_database_name = filename
        if not os.path.isfile(filename):
            self.data = dict()
        else:
            with open(filename) as json_file:
                self.data: dict[tuple[str, str], str] = json.load(json_file)
        # self.last_move_time_slot: int = -1

    def __call__(self, longitude_deg_east: float, latitude_deg_north: float) -> float | tuple[float, float, float]:
        """functor returning the complete coordinate with the elevation or just the elevation at a given longitude and latitude
        :param longitude_deg_east: longitude in degree east
        :type longitude_deg_east: DMS or (degree, minute, seconde) | decimal angle
        :param latitude_deg_north: latitude in degree north
        :type latitude_deg_north: DMS or (degree, minute, seconde) | decimal angle
        :param elevation_only: True for elevation only, longitude, latitude and elevation otherwise, defaults to False
        :type elevation_only: bool, optional
        :return: longitude, latitude and elevation or just elevation
        :rtype: float | tuple[float, float, float]
        """
        coordinate = '(%s,%s)'%(longitude_deg_east, latitude_deg_north)
        if coordinate not in self.data:
            elevation = ElevationRetriever._google_elevation_meter(longitude_deg_east, latitude_deg_north)
            self.data[coordinate] = elevation
            with open(self.json_database_name, 'w') as json_file:
                json.dump(self.data, json_file)
        else:
            elevation: float = self.data[coordinate]
        return elevation

    def _google_elevation_meter(longitude_deg_east: float, latitude_deg_north: float) -> float:
        """search in local database if the coordinates exists, if so it gets the elevation in meters from google and add to the database.
        :param latitude_deg_north: north degree latitude
        :type latitude_deg_north: float
        :param longitude_deg_east: east degree longitude
        :type longitude_deg_east: float
        :return: the collected elevation
        :rtype: float
        """
        # response: requests.Response = requests.get('https://maps.googleapis.com/maps/api/elevation/json', params={'locations': '%f,%f' % (latitude_deg_north, longitude_deg_east), 'key': 'AIzaSyAwWKNdZQaJB4BFJjQCrD3ZKXKoo2ZHNA8'}, headers={'Accept': 'application/json'})
        response: requests.Response = requests.get('https://api.opentopodata.org/v1/test-dataset?locations=%f,%f' % (latitude_deg_north, longitude_deg_east), headers={'Accept': 'application/json'})
        data = response.json()
        return data['results'][0]['elevation']
    

class WeatherJsonReader:
    """Extract the content of a json openweather data file.

    :param json_filename: openweather data file in json format
    :param from_stringdate: initial date in format DD/MM/YYYY hh:mm:ss
    :param to_stringdate: final date in format DD/MM/YYYY hh:mm:ss
    :return: a tuple containing
        - city file_name
        - latitude in decimal north degree
        - longitude in decimal east degree
        - hourly time data variables as a dictionnary with variable file_name as a key
        - units as a dictionnary with variable file_name as a key
        - initial date as a string
        - final date as a string
    """

    def analyze_weather_files():
        print("Available weather files:\n")
        json_filenames = glob.glob(config['folders']['data'] + '*.json')
        for json_filename in json_filenames:
            if not json_filename.endswith('localizations.json'):
                print('- ' + json_filename)
                weather_locations = list()
                # epochtime = -1 
                with open(json_filename) as json_file:
                    json_content = json.load(json_file)
                    if 'generationtime_ms' in json_content: # openmeteo file
                        location = json_filename.split('.json')[0].split('/')[-1]
                        weather_locations.append(location)
                        from_openmeteo_date = json_content['hourly']['time'][0].split('T')[0]
                        to_openmeteo_date = json_content['hourly']['time'][-1].split('T')[0]
                        print('\t- [open-meteo] "%s" (lat:%f,lon:%f) from %s to %s' % (location, float(json_content['latitude']), float(json_content['longitude']), timemg.openmeteo_to_stringdate(from_openmeteo_date), timemg.openmeteo_to_stringdate(to_openmeteo_date)))
                    else:  # openweathtermap file        
                        for i in range(len(json_content)):
                            weather_location = json_content[i]['city_name']
                            if len(weather_locations) == 0 or weather_locations[-1] != weather_location:
                                weather_locations.append(weather_location)
                        for city_name in weather_locations:
                            print('\t- [openweathermap] "%s" latitude,longitude=(%f,%f) from %s to %s' % (city_name, float(json_content[0]['lat']), float(json_content[0]['lon']), timemg.epochtimems_to_stringdate(int(json_content[0]['dt'])*1000), timemg.epochtimems_to_stringdate(int(json_content[-1]['dt'])*1000)))


    def __init__(self, short_json_filename: str, from_stringdate: str=None, to_stringdate: str=None, albedo: float=.1, pollution: float=0.1, location: str=None, longitude: float=None, latitude:float=None, skyline: list[tuple[float, float]]=None, timezone='Europe/Berlin'):
        """Read data from an openweather map json file.

        :param json_filename: name of the openweathermap historical weather file
        :type json_filename: str
        :param from_stringdate: starting date for the data collection, defaults to None
        :type from_stringdate: str, optional
        :param to_stringdate: ending date for the data collection, defaults to None
        :type to_stringdate: str, optional
        :param altitude: sea level in meter of the site location, defaults to 290
        :type altitude: float, optional
        :param albedo: albedo at current site location (see https://en.wikipedia.org/wiki/Albedo), defaults to .1
        :type albedo: float, optional
        :param pollution: turbidity coefficient to model the air pollution at the current site location, defaults to 0.1
        :type pollution: float, optional
        :param location: name of the location to select, if None, the first location name is selected
        :type location: str, defaults to None
        """

        if from_stringdate is not None:
            from_epochtimems = timemg.stringdate_to_epochtimems(from_stringdate+' 0:00:00')  # 
        if to_stringdate is not None:
            to_epochtimems = timemg.stringdate_to_epochtimems(to_stringdate+' 23:00:00')  # 

        common_variable_units: dict[str, str] = {'epochtimems': 'ms', 'temperature': 'celsius', 'wind_speed': 'm/s', 'wind_direction_in_deg': 'degree', 'feels_like': 'celsius', 'humidity': 'percent', 'pressure': 'hPa', 'cloudiness': 'percent'}

        if short_json_filename.endswith('.json'):
            short_json_filename = short_json_filename.split('.json')[0]
        full_json_file_name = config['folders']['data'] + short_json_filename + '.json'

        if location is None:
            location = short_json_filename

        if not os.path.isfile(full_json_file_name):  ## load data from open-meteo.com and generate a weather file
            if latitude is None or longitude is None:
                raise ValueError('Latitude and longitude must be provided to a JsonWeatherReader is the weather file has to be generated')

            server_url = 'https://archive-api.open-meteo.com/v1/archive'

            from_openmeteo_date= timemg.stringdate_to_openmeteo_date(from_stringdate)
            to_openmeteo_date= timemg.stringdate_to_openmeteo_date(to_stringdate)

            weather_data: tuple[str] = ('temperature_2m', 'relativehumidity_2m', 'dewpoint_2m', 'apparent_temperature', 'precipitation', 'rain', 'snowfall', 'weathercode', 'pressure_msl', 'surface_pressure', 'cloudcover', 'cloudcover_low', 'cloudcover_mid', 'cloudcover_high', 'et0_fao_evapotranspiration', 'vapor_pressure_deficit', 'windspeed_10m', 'windspeed_100m', 'winddirection_10m', 'winddirection_100m', 'windgusts_10m', 'soil_temperature_0_to_7cm', 'soil_temperature_7_to_28cm', 'soil_temperature_28_to_100cm', 'soil_temperature_100_to_255cm', 'soil_moisture_0_to_7cm', 'soil_moisture_7_to_28cm', 'soil_moisture_28_to_100cm', 'soil_moisture_100_to_255cm', 'is_day', 'shortwave_radiation', 'direct_radiation', 'diffuse_radiation', 'direct_normal_irradiance')

            params = {'latitude': latitude, 'longitude': longitude, 'start_date': from_openmeteo_date, 'end_date': to_openmeteo_date, 'timezone': timezone, 'hourly': ','.join(weather_data)}

            response = requests.get(server_url, params=params, headers={'Accept': 'application/json'}, stream=True)
            data = response.json()
            if not short_json_filename.endswith('.json'):
                short_json_filename += '.json'
            with open(config['folders']['data'] + short_json_filename, 'w') as json_file:
                json.dump(data, json_file)

        with open(full_json_file_name) as json_file:
            weather_records = json.load(json_file)

            if 'generationtime_ms' in weather_records:  # openmeteo
                latitude_in_deg: float = float(weather_records['latitude'])
                longitude_in_deg: float = float(weather_records['longitude'])
                time_zone: str = weather_records['timezone']
                utc_offset_seconds: int = int(weather_records['utc_offset_seconds'])
                equivalence_openmeteo_openweathermap_names: dict[str, str] = {'time': 'epochtimems', 'temperature_2m': 'temperature', 'windspeed_10m': 'wind_speed', 'winddirection_10m':'wind_direction_in_deg', 'apparent_temperature':'feels_like', 'relativehumidity_2m':'humidity', 'surface_pressure':'pressure', 'cloudcover':'cloudiness'}
                common_variables_values: dict[str,list[float]] = dict()
                specific_variables_values: dict[str,list[float]] = dict()
                specific_variables_units: dict[str,str] = dict()
                if skyline is None:
                    skyline = self._read_solar_gis_HOR(latitude_in_deg, longitude_in_deg, location)
                self.site_weather_data = SiteWeatherData(location, latitude_in_deg, longitude_in_deg, tuple(common_variable_units.keys()), tuple(common_variable_units.values()), albedo=albedo, pollution=pollution,  skyline=skyline, _direct_call=False)
                for openmeteo_variable in weather_records['hourly']:
                    if openmeteo_variable == 'time':
                        common_variables_values['epochtimems'] = [timemg.stringdate_to_epochtimems(stringdatetime, date_format='%Y-%m-%dT%H:%M') for stringdatetime in weather_records['hourly'][openmeteo_variable]]
                    elif openmeteo_variable in equivalence_openmeteo_openweathermap_names:
                        common_variables_values[equivalence_openmeteo_openweathermap_names[openmeteo_variable]] = weather_records['hourly'][openmeteo_variable]
                    else:
                        specific_variables_values[openmeteo_variable] = weather_records['hourly'][openmeteo_variable]
                        specific_variables_units[openmeteo_variable] = weather_records['hourly_units'][openmeteo_variable]
                for k in range(len(common_variables_values['epochtimems'])):
                    _epochtimems = common_variables_values['epochtimems'][k]
                    if (from_stringdate is None or _epochtimems >= from_epochtimems) and (from_stringdate is None or _epochtimems <= to_epochtimems):
                        self.site_weather_data._add_row(_epochtimems, common_variables_values['temperature'][k], common_variables_values['wind_speed'][k], common_variables_values['wind_direction_in_deg'][k], common_variables_values['feels_like'][k], common_variables_values['humidity'][k], common_variables_values['pressure'][k], common_variables_values['cloudiness'][k], None, None, None)
                for openmeteo_variable in specific_variables_values:
                    self.site_weather_data.add_specific_variable(openmeteo_variable, specific_variables_units[openmeteo_variable], specific_variables_values[openmeteo_variable])
                self.site_weather_data.origin = "openmeteo"
            else:  # openweathermap
                latitude_in_deg = float(weather_records[0]['lat'])
                longitude_in_deg = float(weather_records[0]['lon'])
                variable_names = ('epochtimems', 'temperature', 'wind_speed', 'wind_direction_in_deg', 'feels_like', 'humidity', 'pressure', 'cloudiness', 'temp_min', 'temp_max', 'description')
                variable_units: dict[str, str] = {'epochtimems': 'ms', 'temperature': 'celsius', 'wind_speed': 'm/s', 'wind_direction_in_deg': 'degree', 'feels_like': 'celsius', 'humidity': 'percent', 'pressure': 'hPa', 'cloudiness': 'percent', 'temp_min': 'celsius', 'temp_max': 'celsius', 'description': 'text'}
                if location is None:
                    location: str = weather_records[0]['city_name']
                self.location = location
                if skyline is None:
                    skyline = self._read_solar_gis_HOR(latitude_in_deg, longitude_in_deg, location)
                self.site_weather_data = SiteWeatherData(location, latitude_in_deg, longitude_in_deg, variable_names, variable_units, elevation=None, albedo=albedo, pollution=pollution, skyline=skyline, _direct_call=False)

                for _record in weather_records:
                    _epochtimems = int(_record['dt']) * 1000
                    if (from_stringdate is None or _epochtimems >= from_epochtimems) and (from_stringdate is None or _epochtimems <= to_epochtimems) and _record['city_name'] == location:
                        _temperature = float(_record['main']['temp'])
                        _wind_speed = float(_record['wind']['speed'])
                        _wind_direction_in_deg = float(_record['wind']['deg'])
                        _feels_like = float(_record['main']['feels_like'])
                        _humidity = float(_record['main']['humidity'])
                        _pressure = float(_record['main']['pressure'])
                        _cloudiness = float(_record['clouds']['all'])
                        _temp_min = float(_record['main']['temp_min'])
                        _temp_max = float(_record['main']['temp_max'])
                        _description: str = ''
                        for weather_description in _record['weather']:
                            _description += weather_description['description'] + ', '
                        self.site_weather_data._add_row(_epochtimems, _temperature, _wind_speed, _wind_direction_in_deg, _feels_like, _humidity, _pressure, _cloudiness, _temp_min, _temp_max, _description[:-2])
                self.site_weather_data.origin = "openweathermap"

    def _read_solar_gis_HOR(self, latitude_north_deg: float, longitude_east_deg: float, location: str):
        full_hor_filename: str = config['folders']['data']+location+'.HOR'
        if not os.path.isfile(full_hor_filename):
            print("Select a location in solargis, create a project and go to project info.")
            print("Then download the HOR file by clicking 'modify horizon' in red at the lower left corner.")
            webbrowser.open('https://apps.solargis.com/prospect/map?s=%f,%f&c=%f,%f,12' % (latitude_north_deg, longitude_east_deg, latitude_north_deg, longitude_east_deg))
            try: 
                full_hor_filename = input('HOR filename >')
                if not full_hor_filename.endswith('.HOR'):
                    full_hor_filename += '.HOR'
                shutil.copyfile(full_hor_filename, config['folders']['data']+location+'.HOR')
            except:  # noqa
                return [(-180, 0), (180, 0)]

        with open(full_hor_filename) as file:
            contents = file.readlines()
            results = re.search("lat:([0-9]*.[0-9]*).*lng:([0-9]*.[0-9]*)", contents[0])
            latitude, longitude = results.groups()
            latitude = float(latitude)
            longitude = float(longitude)
            skyline = list()
            for i in range(1, len(contents)):
                results = re.search("(-?[0-9]*.[0-9]*)\\t(-?[0-9]*.[0-9]*)", contents[i])
                azimuth, altitude = results.groups()
                skyline.append((float(azimuth), float(altitude)))
        return skyline


class SiteWeatherData:
    """Gathers all the data related to a site dealing with location, albedo, pollution, timezone but also weather timedata coming from an openweather json file."""

    elevation_retriever = ElevationRetriever()

    def __init__(self, location: str, latitude: float, longitude: float, variable_names: tuple[str], variable_units: tuple[str], elevation: float = None, albedo: float = .1, timezone: str = 'Europe/Paris', pollution: float = .1, skyline: list[tuple[float, float]] = None, _direct_call: bool = True):
        """Create object containing data dealing with a specific site, including the weather data.

        :param location: name of the site
        :type location: str
        :param latitude_in_deg: latitude in East degree
        :type latitude_in_deg: float
        :param longitude_in_deg: longitude in North degree
        :type longitude_in_deg: float
        :param variable_names: name of the weather variables
        :type variable_names: tuple[str]
        :param variable_units: units of the weather variables
        :type variable_units: tuple[str]
        :param altitude: altitude of the site in meter from sea level, defaults to 290
        :type altitude: float, optional
        :param albedo: albedo of the site, defaults to .1
        :type albedo: float, optional
        :param timezone: timezone of the site, defaults to 'Europe/Paris'
        :type timezone: str, optional
        :param pollution: pollution coefficient between 0 and 1, defaults to 0.1
        :type pollution: float, optional
        :param _direct_call: internal use to prohibit direct calls of the initializer, defaults to True
        :type _direct_call: bool, optional
        :raises PermissionError: raised in case of direct use, the object must be created by OpenWeatherMapJsonReader
        """
        if _direct_call:
            raise PermissionError('SiteWeatherData cannot be called directly')
        self.origin: str = None
        self.location = location
        self.latitude_in_deg = latitude
        self.longitude_in_deg = longitude
        if elevation is None:
            self.elevation = SiteWeatherData.elevation_retriever(latitude, longitude)
        else:
            self.elevation = elevation
        self.skyline = skyline   
        self.albedo = albedo
        self.pollution = pollution
        self.timezone = timezone
        self.variable_names: tuple[str] = list(variable_names)
        self.variable_units: tuple[str] = list(variable_units)
        self._from_epochtimems = None
        self._to_epochtimems = None
        self._variable_data = dict()
        for variable_name in variable_names:
            self._variable_data[variable_name] = []
        self._cache = dict()
        self._cache_from_epochtimems = None
        self._cache_to_epochtimems = None
        self.origin = None

    def __str__(self):
        string: str = "site is %s with data from %s to %s\s, weather variables are:" % (self.location, timemg.epochtimems_to_stringdate(self._from_epochtimems), timemg.epochtimems_to_stringdate(self._to_epochtimems))
        for i in range(len(self.variable_names)):
            string += '- %s (%s)\n' % (self.variable_names[i], self.variable_units[i])
        return string

    def unit(self, variable_name):
        """Return the unit of a variable.

        :param variable_name: file_name of the variable
        :type variable_name: str
        :return: unit of this variable
        :rtype: str
        """
        return self.variable_units[variable_name]

    @property
    def from_stringdate(self):
        """Return the starting data of the data collection.

        :return: first date where data are available in epoch time (ms)
        :rtype: int
        """
        return timemg.epochtimems_to_stringdate(self._from_epochtimems)

    @property
    def to_stringdate(self):
        """Return the ending date of the data collection.

        :return: last date where data are available
        :rtype: str
        """
        return timemg.epochtimems_to_stringdate(self._to_epochtimems)

    def _add_row(self, *variable_values):
        """Add a row to the weather data collection.

        Internal usage: must not be used directly

        :param variable_values: a list of values corresponds to a row in the json openweather file
        :type variable_values: list[float]
        """
        epochtimems = variable_values[0]
        if self._from_epochtimems is None:
            self._from_epochtimems = epochtimems
            self._to_epochtimems = epochtimems
        elif epochtimems > self._to_epochtimems:
            self._to_epochtimems = epochtimems
        for i in range(len(self.variable_names)):
            self._variable_data[self.variable_names[i]].append(variable_values[i])

    def add_specific_variable(self, variable_name: str, variable_unit: str, values: list[float]):
        self.variable_names.append(variable_name)
        self.variable_units.append(variable_unit)
        self._variable_data[variable_name] = values

    def get(self, variable_name):
        """Return the data collection related to one variable.

        :param variable_name: variable file_name
        :type variable_name: str
        :return: list of float or str values corresponding to common dates for the specified variable
        :rtype: list[float or str]
        """
        if self._cache_from_epochtimems is not None and self._cache_from_epochtimems == self._from_epochtimems and self._cache_to_epochtimems == self._to_epochtimems and variable_name in self._cache:
            return self._cache[variable_name]
        _data = list()
        for i in range(len(self._variable_data['epochtimems'])):
            if self._from_epochtimems <= self._variable_data['epochtimems'][i] <= self._to_epochtimems:
                if variable_name == 'stringdate':
                    _data.append(timemg.epochtimems_to_stringdate(self._variable_data['epochtimems'][i]))
                elif variable_name == 'datetime':
                    _data.append(timemg.epochtimems_to_datetime(self._variable_data['epochtimems'][i]))
                else:
                    _data.append(self._variable_data[variable_name][i])
        if self._cache_from_epochtimems != self._from_epochtimems or self._cache_to_epochtimems != self._to_epochtimems:
            self._cache.clear()
        self._cache_from_epochtimems = self._from_epochtimems
        self._cache_to_epochtimems = self._to_epochtimems
        self._cache[variable_name] = _data
        return _data

    def day_degrees(self, temperature_reference=18, heat=True):
        """Compute heating or cooling day degrees and print in terminal the sum of day degrees per month.

        :param temperature_reference: reference temperature (default is 18°C)
        :param heat: True if heating, False if cooling
        :return: list of day dates as string, list of day average, min and max outdoor temperature and day degrees per day
        :rtype: [list[str], list[float], list[float], list[float], list[float]]
        """
        datetimes = self.get('datetime')
        stringdates = self.get('stringdate')
        temperatures = self.get('temperature')
        dd_months = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        day_stringdate_days = list()
        average_temperature_days = list()
        min_temperature_days = list()
        max_temperature_days = list()
        day_degrees = list()
        day_temperature = list()
        current_day = datetimes[0].day
        for k in range(len(datetimes)):
            if current_day == datetimes[k].day:
                day_temperature.append(temperatures[k])
            else:
                day_stringdate_days.append(stringdates[k-1].split(' ')[0])
                average_day_temperature = sum(day_temperature)/len(day_temperature)
                average_temperature_days.append(average_day_temperature)
                min_temperature_days.append(min(day_temperature))
                max_temperature_days.append(max(day_temperature))
                hdd = 0
                if heat:
                    if average_day_temperature < temperature_reference:
                        hdd = temperature_reference - average_day_temperature
                elif not heat:
                    if average_day_temperature > temperature_reference:
                        hdd = average_day_temperature - temperature_reference
                day_degrees.append(hdd)
                dd_months[datetimes[k].month-1] += hdd
                day_temperature = list()
            current_day = datetimes[k].day
        for i in range(len(dd_months)):
            print('day degrees', month_names[i], ': ', dd_months[i])
        return day_stringdate_days, average_temperature_days, min_temperature_days, max_temperature_days, day_degrees


class CreateOpenMeteoDB:

    def __init__(self, json_file_name: str, from_date: str, to_date: str, latitude: float = None, longitude: float = None, timezone='Europe/Berlin'):
        server_url = 'https://archive-api.open-meteo.com/v1/archive'

        if json_file_name.endswith('.json'):
            json_file_name = json_file_name.split('.json')[0]
        if os.path.isfile(config['folders']['data'] + json_file_name+'.json'):
            raise FileExistsError('Delete manually the file named:' + config['folders']['data'] + json_file_name+'.json')

        from_date = timemg.stringdate_to_openmeteo_date(from_date)
        to_date = timemg.stringdate_to_openmeteo_date(to_date)

        weather_data = ('temperature_2m', 'relativehumidity_2m', 'dewpoint_2m', 'apparent_temperature', 'precipitation', 'rain', 'snowfall', 'weathercode', 'pressure_msl', 'surface_pressure', 'cloudcover', 'cloudcover_low', 'cloudcover_mid', 'cloudcover_high', 'et0_fao_evapotranspiration', 'vapor_pressure_deficit', 'windspeed_10m', 'windspeed_100m', 'winddirection_10m', 'winddirection_100m', 'windgusts_10m', 'soil_temperature_0_to_7cm', 'soil_temperature_7_to_28cm', 'soil_temperature_28_to_100cm', 'soil_temperature_100_to_255cm', 'soil_moisture_0_to_7cm', 'soil_moisture_7_to_28cm', 'soil_moisture_28_to_100cm', 'soil_moisture_100_to_255cm', 'is_day', 'shortwave_radiation', 'direct_radiation', 'diffuse_radiation', 'direct_normal_irradiance')

        if longitude is not None and latitude is not None:
            params = {'latitude': latitude, 'longitude': longitude, 'start_date': from_date, 'end_date': to_date, 'timezone': timezone, 'hourly': ','.join(weather_data)}

        response = requests.get(server_url, params=params, headers={'Accept': 'application/json'}, stream=True)
        data = response.json()
        with open(config['folders']['data'] + json_file_name+'.json', 'w') as json_file:
            json.dump(data, json_file)


if __name__ == '__main__':
    open_meteo = CreateOpenMeteoDB('campus_transition', '1/01/1990', '12/10/2023', latitude=48.54699477777877, longitude=3.002923372261506)
    # WeatherJsonReader.analyze_weather_files()  # 45.054618, 5.552648
    