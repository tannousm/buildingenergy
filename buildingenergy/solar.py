"""Solar model calculation the radiations on a given unit surface with a specific direction.

Author: stephane.ploix@grenoble-inp.fr
"""
from __future__ import annotations
import configparser
import time, os, shutil
from abc import ABC, abstractmethod
from math import acos, asin, atan, atan2, cos, exp, log, pi, sin, sqrt, floor, ceil
from typing import *
import re
import webbrowser

import matplotlib.pyplot as plt
import datetime
import pyexcelerate
from pytz import utc
import logging
import buildingenergy.openweather
from buildingenergy import timemg
import numpy
import enum
import prettytable

logging.basicConfig(level=logging.ERROR)

DIRECTION: dict[str, int] = {'SOUTH': 0, 'EAST': -90, 'WEST': 90, 'NORTH': 180}
# 0° means facing the sky, 90° vertical with the collector directed to the defined direction, 180° facing the ground (but not lying on the ground)
SLOPE: dict[str, int] = {'HORIZONTAL': 0, 'VERTICAL': 90}
config = configparser.ConfigParser()
config.read('setup.ini')
plot_size: tuple[int, int] = (int(config['sizes']['width']), int(config['sizes']['height']))


def _encode4file(string: str, number: float = None):  # type: ignore
    if number is not None:
        string += '='+(str(number).replace('.', ','))
    return string.encode("iso-8859-1")

class Angle:

    def __init__(self, value: float, radian=False) -> None:
        if not radian:
            self._value_rad: float = self._normalize(value / 180 * pi)
        else:
            self._value_rad: float = self._normalize(value)

    @property
    def value_rad(self) -> float:
        return self._normalize(self._value_rad)

    @property
    def value_deg(self) -> float:
        return self._normalize(self._value_rad) / pi * 180

    @staticmethod
    def _normalize(value_rad: float) -> float:
        if -pi <= value_rad <= pi:
            return value_rad
        return (value_rad + pi) % (2 * pi) - pi

    def _diff(self, other_angle: Angle) -> float:
        a = self._value_rad
        b: float = other_angle._value_rad
        return atan2(cos(a)*sin(b)-cos(b)*sin(a), cos(a)*cos(b)+sin(a)*sin(b))/pi*180

    def __lt__(self, other_angle: Angle) -> bool:
        return self._diff(other_angle) > 0

    def __gt__(self, other_angle: Angle) -> bool:
        return self._diff(other_angle) < 0

    def __eq__(self, other_angle: Angle) -> bool:
        return self._diff(other_angle) == 0

    def __add__(self, other_angle: Angle) -> Angle:
        return Angle(self._value_rad + other_angle._value_rad)

    def __str__(self) -> str:
        return 'Angle:%fdegrees' % self.value_deg


class SolarPosition:
    """Contain self.azimuths_in_deg[i] and altitude angles of the sun for mask processing."""

    def __init__(self, azimuth_in_deg: float, altitude_in_deg: float):
        """Create a solar position with self.azimuths_in_deg[i] (south to west directed) and altitude (south to zenith directed) angles of the sun for mask processing.

        :param azimut_in_deg: solar angle with the south in degree, counted with trigonometric direction i.e. the self.azimuths_in_deg[i] (0=South, 90=West, 180=North, -90=East)
        :type azimut_in_deg: float
        :param altitude_in_deg: zenital solar angle with the horizontal in degree i.e. the altitude (0=horizontal, 90=zenith)
        :type altitude_in_deg: float
        """
        if type(azimuth_in_deg) is not Angle:
            self.azimuth_angle: Angle = Angle(azimuth_in_deg)
        else:
            self.azimuth_angle = azimuth_in_deg
        if type(altitude_in_deg) is not Angle:
            self.altitude_angle: Angle = Angle(altitude_in_deg)
        else:
            self.altitude_angle: Angle = altitude_in_deg

    def distance(self, solar_position: SolarPosition) -> float:
        return sqrt((self.azimuth_angle._value_rad-solar_position.azimuth_angle._value_rad)**2 + (self.altitude_angle._value_rad - solar_position.altitude_angle._value_rad)**2)

    def __eq__(self, solar_position: SolarPosition) -> bool:
        return self.altitude_angle == solar_position.altitude_angle and self.azimuth_angle == solar_position.azimuth_angle

    def __lt__(self, solar_position: SolarPosition) -> bool:
        if self.azimuth_angle < solar_position.azimuth_angle:
            return True
        elif self.azimuth_angle == solar_position.azimuth_angle:
            if self.altitude_angle < solar_position.altitude_angle:
                return True
        return False

    def __gt__(self, solar_position: SolarPosition) -> bool:
        if self.azimuth_angle > solar_position.azimuth_angle:
            return True
        elif self.azimuth_angle == solar_position.azimuth_angle:
            if self.altitude_angle > solar_position.altitude_angle:
                return True
        return False

    def __str__(self):
        """Return a description of the solar position.

        :return: string with normalized self.azimuths_in_deg[i] and altitude angles in degree
        :rtype: str
        """
        return '(AZ:%s,AL:%s)' % (self.azimuth_angle.__str__(), self.altitude_angle.__str__())


class SolarModel():
    """Model using the position of the sun and weather data to compute the resulting solar radiations on a directed ground surface."""

    def __init__(self, site_weather_data: buildingenergy.openweather.SiteWeatherData) -> None:
        """Create a site solar model.

        :param site_weather_data: object generated by an OpenWeatherMapJsonReader, that contains site data like location, altitude,... (see openweather.SiteWeatherData)
        :type site_weather_data: openweather.SiteWeatherData
        """
        self.site_weather_data: buildingenergy.openweather.SiteWeatherData = site_weather_data
        self.time_zone: str = site_weather_data.timezone
        self.latitude_rad: float = site_weather_data.latitude_in_deg / 180 * pi
        self.longitude_in_rad: float = site_weather_data.longitude_in_deg / 180 * pi
        self.elevation_m: int = site_weather_data.elevation
        self.albedo: float = site_weather_data.albedo
        self.pollution: float = self.site_weather_data.pollution
        self.skyline_mask = SkyLineMask(*self.site_weather_data.skyline)

        self.datetimes: list[datetime.datetime] = self.site_weather_data.get('datetime')
        self.temperatures: list[float] = self.site_weather_data.get('temperature')
        self.humidities: list[float] = self.site_weather_data.get('humidity')
        self.nebulosities_percent: list[int] = self.site_weather_data.get('cloudiness')
        self.pressures_Pa: list[int] = [100 * p for p in self.site_weather_data.get('pressure')]

        self.altitudes_rad = list()
        self.azimuths_rad = list()
        self.days_in_year = list()

        for administrative_datetime in self.datetimes:
            altitude_rad, azimuth_rad, day_in_year = self.compute_altitude_azimuth_rad_day_in_year(administrative_datetime)
            self.altitudes_rad.append(altitude_rad)
            self.azimuths_rad.append(azimuth_rad)
            self.days_in_year.append(day_in_year)
            
    @property
    def sun_altitudes_deg(self):
        return [a/pi*180 for a in self.altitudes_rad]
    
    @property
    def sun_azimuths_deg(self):
        return [a/pi*180 for a in self.azimuths_rad]
            
    def irradiances(self, exposure_deg: float, slope_deg: float, attenuation_factor: float = 1, mask: Mask = None):
        tilt_global_irradiances = list()
        tilt_direct_irradiances = list()
        tilt_diffuse_irradiances = list()
        tilt_reflected_irradiances = list()
        direct_normal_irradiances = list()
        
        for k in range(len(self.datetimes)):
            global_tilt_irradiance, direct_tilt_irradiance, diffuse_tilt_irradiance, reflected_tilt_irradiance, direct_normal_irradiance = self._compute_solar_irradiances(self.days_in_year[k], self.elevation_m, self.altitudes_rad[k], self.azimuths_rad[k], slope_deg, exposure_deg, self.nebulosities_percent[k], self.temperatures[k], self.humidities[k], self.pressures_Pa[k], self.pollution, self.albedo, attenuation_factor, mask)
            tilt_global_irradiances.append(global_tilt_irradiance)
            tilt_direct_irradiances.append(direct_tilt_irradiance)
            tilt_diffuse_irradiances.append(diffuse_tilt_irradiance)
            tilt_reflected_irradiances.append(reflected_tilt_irradiance)
            direct_normal_irradiances.append(direct_normal_irradiance)
        return {'total': tilt_global_irradiances, 'direct': tilt_direct_irradiances, 'diffuse': tilt_diffuse_irradiances, 'reflected': tilt_reflected_irradiances, 'normal': direct_normal_irradiances}

    def _compute_solar_time(self, administrative_datetime: datetime.datetime) -> tuple[int, int]:
        """Calculate the solar time from administrative.

        :param administrative_datetime: the date time
        :type administrative_datetime: datetime.datetime
        :return: elevation_in_rad, azimuth_in_rad, hour_angle_in_rad, latitude_in_rad, declination_in_rad
        :rtype: tuple[float]
        """
        utc_datetime: datetime.datetime = administrative_datetime.astimezone(utc)
        utc_timetuple: time.struct_time() = utc_datetime.timetuple()
        day_in_year: int = utc_timetuple.tm_yday
        hour_in_day: int = utc_timetuple.tm_hour
        minute_in_hour: int = utc_timetuple.tm_min
        seconde_in_minute: int = utc_timetuple.tm_sec
        greenwich_time_in_seconds = hour_in_day * 3600 + minute_in_hour * 60 + seconde_in_minute
        local_solar_time_seconds: float = greenwich_time_in_seconds + self.longitude_in_rad / (2*pi) * 24 * 3600
        # time correction takes into account seasonal variation of sun speed, eccentricity of sun trajectory, inclination of the earth axis (see https://www.vanoise49.fr/annexe_3.html)
        time_correction_equation_seconds = 229.2 * ( 
              0.0320*sin(2*pi*day_in_year/365) - 0.001868*cos(2*pi*day_in_year/365) + 0.014615*cos(4*pi*day_in_year/365) + 0.04089*sin(4*pi*day_in_year/365) - 0.000075)
        actual_solartime_in_secondes: float = local_solar_time_seconds - time_correction_equation_seconds
        return day_in_year, actual_solartime_in_secondes

    def compute_altitude_azimuth_rad_day_in_year(self, administrative_datetime: datetime.datetime) -> tuple[float, float]:
        """Return to the basic calculation of solar angles.

        :param administrative_datetime: administrative time
        :type administrative_datetime: datetime.datetime
        :return: the altitude of the sun in the sky from the horizontal plan to the sun altitude, the azimuth is the angle with south direction in the horizontal plan
        :rtype: tuple[float]
        """
        day_in_year, solartime_secondes = self._compute_solar_time(administrative_datetime)
        # the angular distance of the sun from the celestial equator: the value varies along the year from -23.45° to 23.45°
        declination_rad: float = 23.45 / 180 * pi * sin(2*pi *  (day_in_year+284)/365.25)
        # the hour angle describes the seeming movement of the Sun around Earth. Its value shows the seeming place of the Sun towards east or west compared to the local
        # meridian. Its value is negative in the morning and positive in the afternoon and 0 when the Sun is right on the meridian
        solar_angle_at_greenwich_rad: float = 2*pi * (solartime_secondes / 3600 - 12) / 24
        # the height of the Sun above the horizon is given by the solar altitude. Solar altitude changes continuously, its value is 0 at sunrise and sunset while its 
        # maximum is reached at culminate. Culmination height is also different in every season
        altitude_rad: float = max(0, asin(sin(declination_rad) * sin(self.latitude_rad) + cos(declination_rad) * cos(self.latitude_rad) * cos(solar_angle_at_greenwich_rad)))
        # The azimuth angle of the Sun is the angle between solar rays and the N–S direction of the earth. It is calculated based on the following equation for which 
        # we have to know geographical latitude declination angle, hour angle and solar altitude
        cos_azimuth: float = (cos(declination_rad) * cos(solar_angle_at_greenwich_rad) * sin(self.latitude_rad) - sin(declination_rad) * cos(self.latitude_rad)) / cos(altitude_rad)
        sin_azimuth: float = cos(declination_rad) * sin(solar_angle_at_greenwich_rad) / cos(altitude_rad)
        azimuth_rad = atan2(sin_azimuth, cos_azimuth)  # min(pi/2, max(-pi/2, ))
        return altitude_rad, azimuth_rad, day_in_year

    def _compute_solar_irradiances(self, day_in_year: int, elevation_m, altitude_rad: float, azimuth_rad: float, slope_deg: float, exposure_deg: float, nebulosity_percent: float, temperature: float, humidity: float, ground_atmospheric_pressure: float, pollution: float, albedo: float, attenuation_factor: float = 1, mask: Mask = None) -> dict[str, list[float]]:
        """Compute the solar power on a 1m2 flat surface.

        :param exposure_in_deg: clockwise angle in degrees between the south and the normal of collecting surface. O means south oriented, 90 means West, -90 East and 180 north oriented
        :type exposure_in_deg: float
        :param slope_in_deg: clockwise angle in degrees between the ground and the collecting surface. 0 means horizontal, directed to the sky, and 90 means vertical directed to the specified direction
        :type slope_in_deg: float
        :return: phi_total, phi_direct_collected, phi_diffuse, phi_reflected
        :rtype: list[float]
        """
        global_mask = None
        if self.skyline_mask is not None and mask is None:
            global_mask = self.skyline_mask
        elif self.skyline_mask is not None and mask is not None:
            OrMask(self.skyline_mask, mask)
        slope_rad, exposure_rad = (slope_deg - 180) / 180 * pi, exposure_deg / 180 * pi
        incidence_rad = acos(cos(altitude_rad) * sin(slope_rad) * cos(azimuth_rad - exposure_rad) + sin(altitude_rad) * cos(slope_rad))
    
        normal_atmospheric_irradiance = 1367 * (1 + 0.033 * cos(2*pi * (1+day_in_year) / 365.25))
        normal_atmospheric_irradiance_with_clouds: float = (1 - 0.75 * (nebulosity_percent/100) ** 3.4) * normal_atmospheric_irradiance      

        if 0 < altitude_rad < pi and (global_mask is None or SolarPosition(azimuth_rad/pi*180, altitude_rad/pi*180) not in global_mask):
            # Relative optical air mass is defined as the ratio of the air mass actually flown through to the air mass at vertical incidence.
            relative_optical_air_mass: float = ground_atmospheric_pressure / (101325 * sin(altitude_rad) + 15198.75 * (3.885 + altitude_rad/pi*180)**(-1.253))
            
            # The transmitivity coefficient represents the fraction of the incident radiation  at the surface of the atmosphere that reaches the ground along a vertical path.  M reflects the variation in atmospheric thickness as a function of the time of day. The Piedallu & Gégout formula is used
            M0 = sqrt(1229 + (614*sin(altitude_rad))**2) - 614*sin(altitude_rad)
            # In mountainous areas, it is necessary to use a correction factor linked to atmospheric pressure, which depends on elevation.
            Mh: float = (1-0.0065/288*elevation_m)**5.256 * M0
            transmitivity_coefficient: float = 0.6 ** Mh
            # The Rayleigh thickness is directly deduced from the relative optical air mass.
            rayleigh_thickness: float = 1 / (0.9 * relative_optical_air_mass + 9.4)
            # Partial steam pressure: Pv
            partial_steam_pressure: float = 2.165 * (1.098 + temperature/100)**8.02 * humidity
            # Linke factor modeling disturbances due to steam, fog or dust. Pollution = 0.002 in mountains, 0.05 in countryside, 0.1 in urban area and 0.2 in highly polluted area
            l_Linke: float = 2.4 + 14.6 * pollution + 0.4 * (1 + 2 * pollution) * log(partial_steam_pressure)
            
            direct_normal_irradiance: float = transmitivity_coefficient * normal_atmospheric_irradiance_with_clouds * exp(-relative_optical_air_mass * rayleigh_thickness * l_Linke)

            direct_tilt_irradiance: float = incidence_rad * direct_normal_irradiance * attenuation_factor
            diffuse_tilt_irradiance: float = max(0, normal_atmospheric_irradiance_with_clouds * (0.271 - 0.294 * transmitivity_coefficient) * sin(altitude_rad)) * attenuation_factor
            reflected_tilt_irradiance: float = max(0, albedo * normal_atmospheric_irradiance_with_clouds * (0.271 + 0.706 * transmitivity_coefficient) * sin(altitude_rad) * cos(slope_rad / 2) ** 2) * attenuation_factor
        else:
            global_tilt_irradiance = 0
            direct_tilt_irradiance = 0
            diffuse_tilt_irradiance = 0
            reflected_tilt_irradiance = 0
            direct_normal_irradiance = 0
        global_tilt_irradiance: float = direct_tilt_irradiance + diffuse_tilt_irradiance + reflected_tilt_irradiance
        return global_tilt_irradiance, direct_tilt_irradiance, diffuse_tilt_irradiance, reflected_tilt_irradiance, direct_normal_irradiance

    def try_export(self):
        """Export a TRY weather files for IZUBA Pleiades software. It generates 2 files per full year:
        - site_location + '_' + 'year' + '.INI'
        - site_location + '_' + 'year' + '.TRY'
        The station ID will correspond to the 3 first characters of the site_location in upper case
        """
        site_location: str = self.site_weather_data.location
        location_id: str = site_location[0:3].upper()
        temperatures: list[float] = self.site_weather_data.get('temperature')
        TempSol: float = round(sum(temperatures) / len(temperatures))
        temperature_tenth: list[float] = [10 * t for t in temperatures]
        humidities: list[float] = self.site_weather_data.get('humidity')
        wind_speeds: list[float] = self.site_weather_data.get('wind_speed')
        wind_directions_in_deg: list[float] = self.site_weather_data.get('wind_direction_in_deg')
        irradiations: dict[str, list[float]] = self.irradiances(slope_deg=SLOPE['HORIZONTAL'], exposure_deg=DIRECTION['SOUTH'])
        irradiations_global_horizontal: list[float] = irradiations['total']
        irradiations_sun_direction: list[float] =  irradiations['direct'] # self.irradiations_sun_direction(self.site_weather_data.get('cloudiness'))
        ini_file = None
        try_file = None
        for i, dt in enumerate(self.datetimes):
            year, month, day, hour = dt.year, dt.month, dt.day, dt.hour+1
            if month == 1 and day == 1 and hour == 1:
                if ini_file is not None:
                    ini_file.close()
                    try_file.close()
                file_name = config['folders']['results'] + site_location + '_' + str(year)
                ini_file = open(file_name + '.ini', "wb")
                ini_file.write(_encode4file('[Station]'))
                ini_file.write(_encode4file('Nom=' + file_name + ' (TRY)'))
                ini_file.write(_encode4file('Altitude', self.elevation_m))
                ini_file.write(_encode4file('Lattitude', self.latitude_rad/pi*180))
                ini_file.write(_encode4file('Longitude', self.longitude_in_rad/pi*180))
                ini_file.write(_encode4file('NomFichier=' + file_name+'.try'))
                ini_file.write(_encode4file('TempSol', TempSol))
                ini_file.write(_encode4file('TypeFichier=try'))
                ini_file.write(_encode4file('Heure solaire=0'))
                ini_file.write(_encode4file('Meridien', int(floor(self.latitude_rad/pi*12))))
                ini_file.write(_encode4file('LectureSeule=1'))
                ini_file.write(_encode4file('Categorie=OpenWeather'))
                ini_file.close()
                try_file = open(file_name + '.try', "wb")
            if try_file is not None:
                try_file.write(_encode4file(location_id+"{:4d}{:4d}{:4d}{:4d}   E{:3d}{:3d}{:2d}{:2d}{:2d}{:4d}   E     E{:6.2f}{:7.2f}".format(int(round(temperature_tenth[i])), int(round(irradiations_global_horizontal[i]*3600/1000)), int(round(
                    irradiations['diffuse'][i]*3600/1000)), int(round(irradiations_sun_direction[i]*3600/1000)), int(humidities[i]), int(wind_speeds[i]), month, day, hour+1, int(wind_directions_in_deg[i]), self.sun_altitudes_deg[i], self.sun_azimuths_deg[i])))
        try:
            try_file.close()
        except:
            pass

    def plot_heliodor(self, year: int, name:str='', new_figure:bool=True):
        """Plot heliodor at current location.

        :param year: year to be displayed in figure
        :type year: int
        :param name: file_name to be displayed in figure, default to ''
        :type name: str
        """
        stringdates: list[str] = ['21/%i/%i' % (i, year) for i in range(1, 13, 1)]
        legends: list[str] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        hours_altitudes = dict()
        hours_azimuths = dict()

        if new_figure:
            figure, axis = plt.subplots(figsize=plot_size)
        else:
            figure = plt.gcf()
            axis = plt.gca()
        for day_index, stringdate in enumerate(stringdates):
            altitudes_in_deg, azimuths_in_deg = [], []
            for hour_in_day in range(0, 24, 1):
                for minutes in range(0, 60, 1):  # 10
                    altitude_rad, azimuth_rad, day_in_year = self.compute_altitude_azimuth_rad_day_in_year(timemg.stringdate_to_datetime(stringdate + ' %i:%i:0' % (hour_in_day, minutes)))
                    altitude_deg = altitude_rad/pi*180
                    azimuth_deg = azimuth_rad/pi*180
                    if altitude_deg > 0:
                        altitudes_in_deg.append(altitude_deg)
                        azimuths_in_deg.append(azimuth_deg)
                        if minutes == 0 and hour_in_day not in hours_altitudes:
                            hours_altitudes[hour_in_day] = [altitude_deg]
                            hours_azimuths[hour_in_day] = [azimuth_deg]
                        elif minutes == 0:
                            hours_altitudes[hour_in_day].append(altitude_deg)
                            hours_azimuths[hour_in_day].append(azimuth_deg)
                axis.plot(azimuths_in_deg, altitudes_in_deg)
            i_position = (day_index % 6)*len(altitudes_in_deg)//6
            axis.annotate(legends[day_index], (azimuths_in_deg[i_position], altitudes_in_deg[i_position]))
        axis.set_title('heliodor %s (21th of each month)' % name)
        for hour_in_day in hours_altitudes:
            axis.plot(hours_azimuths[hour_in_day], hours_altitudes[hour_in_day], ':k')
            axis.annotate(hour_in_day, (hours_azimuths[hour_in_day][len(hours_azimuths[hour_in_day])//2], hours_altitudes[hour_in_day][len(hours_azimuths[hour_in_day])//2]))
        axis.axis('tight')
        axis.grid()
        return axis

    def plot_angles(self):
        """Plot solar angles for the dates corresponding to dates in site_weather_data."""
        plt.figure()
        plt.plot(self.datetimes, self.sun_altitudes_deg, self.datetimes, self.sun_azimuths_deg)
        plt.legend(('altitude in deg', 'azimuth in deg'))
        plt.axis('tight')
        plt.grid()

    def plot_solar_cardinal_irradiations(self) -> None:
        """Plot total solar irradiation on all cardinal direction and an horizontal one, for the dates corresponding to dates in site_weather_data."""
        plt.figure()
        directions: tuple[str] = ('SOUTH', 'EAST', 'WEST', 'NORTH')
        phis = self.irradiances(exposure_deg=DIRECTION['SOUTH'], slope_deg=SLOPE['HORIZONTAL'])['total']
        print('energy HORIZONTAL:', sum(phis)/1000, 'kWh/m2')
        plt.plot(self.datetimes, phis)
        for d in range(4):
            phis: list[float] = self.irradiances(exposure_deg=DIRECTION[directions[d]], slope_deg=SLOPE['VERTICAL'])['total']
            print('energy', directions[d], ':', sum(phis)/1000, 'kWh/m2')
            plt.plot(self.datetimes, phis)
        plt.legend(('HORIZONTAL', 'SOUTH', 'EAST', 'WEST', 'NORTH'))
        plt.ylabel('Watt')
        plt.axis('tight')
        plt.grid()


def regular_angle_to_decimal_angle_converter(decimals, minutes, seconds):
    """Convert decimals, minutes, seconds to float value.

    :param decimals: number of degrees as an integer
    :type decimals: int
    :param minutes: number of minutes
    :type minutes: int
    :param seconds: number of seconds
    :type seconds: int
    :return: angle in decimal format
    :rtype: float
    """
    return decimals + minutes/60 + seconds/3600


class SolarSystem:
    """A class used to obtain the solar gains through the windows of a building."""

    def __init__(self, solar_model: SolarModel):
        """Create a set of solar collectors with masks to estimate the global solar gain.

        :param site_weather_data: weather data
        :type site_weather_data: openweather.SiteWeatherData
        :param solar_mask: distant solar mask used for the whole building. None means no global solar masks
        :type solar_mask: ComplexZone
        """
        self.datetimes: list[datetime.datetime] = solar_model.site_weather_data.get('datetime')
        self.stringdates: list[str] = solar_model.site_weather_data.get('stringdate')
        self.temperatures: list[float] = solar_model.site_weather_data.get('temperature')
        self.nebulosities_in_percent: list[float] = solar_model.site_weather_data.get('cloudiness')
        self.humidities: list[float] = solar_model.site_weather_data.get('humidity')
        self.pollution: float = solar_model.site_weather_data.pollution
        self.albedo: float = solar_model.site_weather_data.albedo
        self.skyline_mask: Mask = solar_model.skyline_mask
        self.collector_masks: dict[str, Mask] = dict()

        self.phi_collectors_total: dict[str, list[float]] = dict()
        self.phi_collectors_direct: dict[str, list[float]] = dict()
        self.phi_collectors_diffuse: dict[str, list[float]] = dict()
        self.phi_collectors_reflected: dict[str, list[float]] = dict()
        self.collector_names = list()
        self.solar_model: SolarModel = solar_model

    def add_collector(self, name: str, surface: float, exposure_in_deg: float, slope_in_deg: float, solar_factor: float = 0.4, collector_mask: Mask = None, single_side: bool = True) -> None:
        """Add a window to the building.

        :param name: name of the window
        :type name: str
        :param surface: surface of the glass
        :type surface: float
        :param exposure_in_deg: clockwise angle in degrees of the surface with the north. O means north oriented, -90 means West, 90 East and 180 South oriented
        :type exposure_in_deg: float
        :param slope_in_deg: clockwise angle in degrees between the ground and the exposed surface. 0 means horizontal directed to the sky zenith and 90 means vertical
        :type slope_in_deg: float
        :param solar_factor: solar factor between 0 (opaque) and 100 (fully transparent)
        :type solar_factor: float
        :param window_mask: mask only for this collector
        :type window_mask: WindowMask
        """
        self.collector_names.append(name)
        if single_side:
            mask = OrMask(InvertedMask(RectangularMask((-90+exposure_in_deg, 90+exposure_in_deg), (0-slope_in_deg, 180-slope_in_deg))), self.skyline_mask, collector_mask)
        else:
            mask = OrMask(self.skyline_mask, collector_mask)
        
        self.collector_masks[name] = mask
        
        phi_collectors: dict[str, list[float]] = self.solar_model.irradiances(exposure_in_deg, slope_in_deg, surface * solar_factor, mask)
        
        # day_in_year: int, elevation_m, altitude_rad: float, azimuth_rad: float, slope_deg: float, exposure_deg: float, nebulosity_percent: float, temperature: float, humidity: float, ground_atmospheric_pressure: float, pollution: float, albedo: float, attenuation_factor: float = 1, mask: Mask = None
         
        self.phi_collectors_total[name] = phi_collectors['total']
        self.phi_collectors_direct[name] = phi_collectors['direct']
        self.phi_collectors_diffuse[name] = phi_collectors['diffuse']
        self.phi_collectors_reflected[name] = phi_collectors['reflected']
        
    def global_mask(self):
        mask = self.skyline_mask
        for collector_name in self.collector_masks:
            mask = OrMask(mask, self.collector_masks[collector_name])
        return mask

    def solar_gains_in_Wh(self, direct_only: bool = False) -> tuple[list[float], dict[str, list[datetime.datetime]]]:
        """Return hourly solar gains coming through the windows.

        :return: list of total solar gains powers in Watt, and a dictionary of the detailed solar gains in Watts coming through each window
        :rtype: tuple[list[float], dict[str,list[float]]]
        """
        total_gains_in_kWh: list[float] = [0 for _ in range(len(self.datetimes))]
        collector_gains_in_kWh: dict[str, list[datetime.datetime]] = dict()
        for name in self.collector_names:
            collector_gains_in_kWh[name] = self.phi_collectors_total[name]
            for k in range(len(self.datetimes)):
                if direct_only:
                    total_gains_in_kWh[k] += self.phi_collectors_direct[name][k]
                else:
                    total_gains_in_kWh[k] += self.phi_collectors_total[name][k]
        return total_gains_in_kWh, collector_gains_in_kWh

    def day_averager(self, vector, average=True):
        """Compute the average or integration hour-time data to get day data.

        :param average: True to compute an average, False for integration
        :type average: bool
        :param vector: vector of values to be down-sampled
        :return: list of floating numbers
        """
        current_day = self.datetimes[0].day
        day_integrated_vector = list()
        values = list()
        for k in range(len(self.datetimes)):
            if current_day == self.datetimes[k].day:
                values.append(vector[k])
            else:
                average_value = sum(values)
                if average:
                    average_value = average_value/len(values)
                day_integrated_vector.append(average_value)
                values = list()
            current_day = self.datetimes[k].day
        return day_integrated_vector

    def __len__(self):
        """Return the number of hours in the weather data.

        :return: number of hours in the weather data
        :rtype: int
        """
        return len(self.stringdates)

    def generate_xls(self, file_name='calculations', heat_temperature_reference=18,  cool_temperature_reference=26):
        """Save day degrees and solar gains per window for each day in an xls file.

        :param file_name: file name without extension, default to 'calculation'
        :type file_name: str
        :param temperature_reference: reference temperature for heating day degrees
        :type heat_temperature_reference: float
        :param cool_temperature_reference: reference temperature for cooling day degrees
        :type cool_temperature_reference: float
        """
        print('Heating day degrees')
        stringdate_days, average_temperature_days, min_temperature_days, max_temperature_days, dju_heat_days = self.solar_model.site_weather_data.day_degrees(temperature_reference=heat_temperature_reference, heat=True)
        print('Cooling day degrees')
        _, _, _, _, dju_cool_days = self.solar_model.site_weather_data.day_degrees(temperature_reference=cool_temperature_reference, heat=False)

        data: list[list[str]] = [['date'], ['Tout'], ['Tout_min'], ['Tout_max'], ['dju_heat'], ['dju_cool']]
        data[0].extend(stringdate_days)
        data[1].extend(average_temperature_days)
        data[2].extend(min_temperature_days)
        data[3].extend(max_temperature_days)
        data[4].extend(dju_heat_days)
        data[5].extend(dju_cool_days)

        total_gains_in_kWh, collectors_solar_gains_in_kWh = self.solar_gains_in_Wh()
        i = 6
        for window_name in self.collector_names:
            data.append([window_name+'(Wh)'])
            data[i].extend(self.day_averager(collectors_solar_gains_in_kWh[window_name], average=False))
            i += 1

        excel_workbook = pyexcelerate.Workbook()
        excel_workbook.new_sheet(file_name, data=list(map(list, zip(*data))))
        excel_workbook.save(file_name + '.xlsx')


class Mask(ABC):
    """Abstract class standing for a single zone building. It can be used once specialized."""

    _azimuth_min_max_in_rad: tuple = (-pi, pi)
    _altitude_min_max_in_rad: tuple = (0, pi / 2)

    @abstractmethod
    def __contains__(self, solar_position: SolarPosition) -> bool:
        """Determine whether a solar position of the sun in the sky defined by azimuth and altitude angles is passing through the mask (True) or not (False).

        :param solar_position: solar solar_position in the sky
        :type solar_position: SolarPosition
        :return: True if the solar position is blocked by the mask, False otherwise
        :rtype: bool
        """
        raise NotImplementedError

    def plot(self, name: str = '', axis=None, resolution: int=40):
        """Plot the mask according to the specified max_plot_resolution and print a description of the zone.

        :param name: file_name of the plot, default to ''
        :return: the zone
        """
        azimuths_in_rad: list[float] = [Mask._azimuth_min_max_in_rad[0] + i * (Mask._azimuth_min_max_in_rad[1] - Mask._azimuth_min_max_in_rad[0]) / (resolution - 1) for i in range(resolution)]
        altitudes_in_rad = [-Mask._altitude_min_max_in_rad[0] + i *
                            (Mask._altitude_min_max_in_rad[1] - Mask._altitude_min_max_in_rad[0]) / (resolution - 1) for i in range(resolution)]
        if axis is None:
            figure, axis = plt.subplots(figsize=plot_size)
            axis.set_xlim((180 / pi * Mask._azimuth_min_max_in_rad[0], 180 / pi * Mask._azimuth_min_max_in_rad[1]))
            axis.set_ylim((-180 / pi * Mask._altitude_min_max_in_rad[0], 180 / pi * Mask._altitude_min_max_in_rad[1]))
        else:
            figure = plt.gcf()
            axis = plt.gca()
        for azimuth_in_rad in azimuths_in_rad:
            for altitude_in_rad in altitudes_in_rad:
                if SolarPosition(azimuth_in_rad * 180 / pi, altitude_in_rad * 180 / pi) not in self:
                    axis.scatter(180 / pi * azimuth_in_rad, 180 / pi * altitude_in_rad, c='grey', marker='.')
        axis.set_xlabel('Azimuth in degrees (0° = South)')
        axis.set_ylabel('Altitude in degrees')
        axis.set_title(name)


class RectangularMask(Mask):

    def __init__(self, minmax_azimuths_deg: tuple(float, float) = None, minmax_altitudes_deg: tuple(float, float) = None) -> None:
        super().__init__()

        if minmax_azimuths_deg is None:
            self.minmax_azimuth_angles = None
        else:
            self.minmax_azimuth_angles: float = (Angle(minmax_azimuths_deg[0]), Angle(minmax_azimuths_deg[1]))

        if minmax_altitudes_deg is None:
            self.minmax_altitude_angles = None
        else:
            self.minmax_altitude_angles: float | Angle = (Angle(minmax_altitudes_deg[0]), Angle(minmax_altitudes_deg[1]))

    def __contains__(self, solar_position: SolarPosition) -> bool:
        contained: bool = True
        if self.minmax_azimuth_angles is not None:
            contained = (self.minmax_azimuth_angles[0] < solar_position.azimuth_angle < self.minmax_azimuth_angles[1])
        if self.minmax_altitude_angles is not None:
            contained = contained and (self.minmax_altitude_angles[0] < solar_position.altitude_angle < self.minmax_altitude_angles[1])
        return contained


class EllipsoidalMask(Mask):

    def __init__(self, center_azimuth_altitude_in_deg1: tuple(float | Angle, float | Angle), center_azimuth_altitude_in_deg2: tuple(float | Angle, float | Angle), perimeter_azimuth_altitude_in_deg: tuple(float | Angle, float | Angle)) -> None:
        super().__init__()
        self.center_solar_position1: SolarPosition = SolarPosition(center_azimuth_altitude_in_deg1[0], center_azimuth_altitude_in_deg1[1])
        self.center_solar_position2: SolarPosition = SolarPosition(center_azimuth_altitude_in_deg2[0], center_azimuth_altitude_in_deg2[1])
        self.perimeter_solar_position: SolarPosition = SolarPosition(perimeter_azimuth_altitude_in_deg[0], perimeter_azimuth_altitude_in_deg[1])
        self.length: float | Angle = self._three_positions_length(self.perimeter_solar_position)

    def _three_positions_length(self, solar_position: SolarPosition) -> float | Angle:
        return self.center_solar_position1.distance(self.center_solar_position2) + self.center_solar_position2.distance(solar_position) + solar_position.distance(self.center_solar_position1)

    def __contains__(self, solar_position: SolarPosition) -> bool:
        return self.length > self._three_positions_length(solar_position)


class SkyLineMask(Mask):

    def __init__(self, *azimuths_altitudes_in_deg: tuple[tuple(float | Angle, float | Angle)]) -> None:
        super().__init__()
        azimuths_altitudes_in_deg = list(azimuths_altitudes_in_deg)
        if azimuths_altitudes_in_deg[0][0] != -180:
            azimuths_altitudes_in_deg.insert(0, (-180, 0))
        if azimuths_altitudes_in_deg[-1][0] != 180:
            azimuths_altitudes_in_deg.append((180, azimuths_altitudes_in_deg[0][1]))
        for i in range(1, len(azimuths_altitudes_in_deg)):
            if azimuths_altitudes_in_deg[i-1][0] > azimuths_altitudes_in_deg[i][0]:
                raise ValueError('Skyline is not increasing in azimuth at index %i' % i)
        self.solar_positions: list[SolarPosition] = [SolarPosition(Angle(azimuth_in_deg), Angle(altitude_in_deg)) for azimuth_in_deg, altitude_in_deg in azimuths_altitudes_in_deg]

    def __contains__(self, solar_position: SolarPosition) -> bool:
        index: int = None
        for i in range(1, len(self.solar_positions)):
            index = i - 1
            if self.solar_positions[i-1].azimuth_angle < solar_position.azimuth_angle < self.solar_positions[i].azimuth_angle:
                break
        azimuth_angle0, azimuth_angle1 = self.solar_positions[index].azimuth_angle, self.solar_positions[index+1].azimuth_angle
        altitude_angle0, altitude_angle1 = self.solar_positions[index].altitude_angle, self.solar_positions[index+1].altitude_angle
        if azimuth_angle0 == azimuth_angle1:
            return solar_position.altitude_angle < Angle(max(altitude_angle0.value_deg, altitude_angle1.value_deg))
        altitude_segment: float | Angle = Angle((altitude_angle1.value_deg-altitude_angle0.value_deg)/(azimuth_angle1.value_deg-azimuth_angle0.value_deg) * solar_position.azimuth_angle.value_deg + (
            altitude_angle0.value_deg*azimuth_angle1.value_deg-altitude_angle1.value_deg*azimuth_angle0.value_deg)/(azimuth_angle1.value_deg-azimuth_angle0.value_deg))
        if solar_position.altitude_angle < altitude_segment:
            logging.info('Sun beam is blocked by skyline at solar position: AZ:%f°, AL:%f°' %
                         (solar_position.azimuth_angle._value_rad/pi*180, solar_position.altitude_angle._value_rad/pi*180))
        return solar_position.altitude_angle < altitude_segment


class AndMask(Mask):

    def __init__(self, *masks: list[Mask]) -> None:
        super().__init__()
        self.masks: list[Mask] = list()
        for mask in masks:
            self.masks.append(mask)

    def add_mask(self, mask: Mask):
        if mask is not None:
            self.masks.append(mask)

    def __contains__(self, solar_position: SolarPosition) -> bool:
        if len(mask) == 0:
            return True
        for mask in self.masks:
            if not mask.__contains__(solar_position):
                return False
        return True


class OrMask(Mask):

    def __init__(self, *masks: list[Mask]) -> None:
        super().__init__()
        self.masks: List[Mask] = list()
        for mask in masks:
            if mask is not None:
                self.masks.append(mask)

    def add_mask(self, mask: Mask):
        if mask is not None:
            self.masks.append(mask)

    def __contains__(self, solar_position: SolarPosition) -> bool:
        if len(self.masks) == 0:
            return True
        for mask in self.masks:
            if mask.__contains__(solar_position):
                return True
        return False


class InvertedMask:
    def __init__(self, mask: Mask) -> None:
        super().__init__()
        self.mask: Mask = mask

    def __contains__(self, solar_position: SolarPosition) -> bool:
        if self.mask is None:
            return True
        return not self.mask.__contains__(solar_position)


class MOUNT_TYPE(enum.Enum):
    FLAT = 0
    SAW = 1
    ARROW = 2


class PVplantProperties:
    
    def __init__(self):
        self.surfacePV_in_m2: float = None
        self.array_width: float = None
        self.panel_height_in_m: float = None
        self.efficiency: float = None
        self.exposure_in_deg: float = None
        self.slope_in_deg: float = None
        self.distance_between_arrays_in_m = None
        self.mount_type = None
        self.skyline: tuple[tuple[float, float]] = ((-180, 0), (180, 0))
        self.solar_mask = None
        self.number_of_cell_rows: int = 10
        self.temperature_coefficient = 0.0035
        
    def check(self) -> None:
        if self.surfacePV_in_m2 is None or self.surfacePV_in_m2 < 0:
            raise ValueError('Incorrect value for surfacePV_in_m2')
        if self.array_width is None or self.array_width < 0 or 0.1 * self.array_width > self.surfacePV_in_m2:
            raise ValueError('Incorrect value for array_width')
        if self.panel_height_in_m is None or self.panel_height_in_m <= 0:
            raise ValueError('Incorrect value for panel_height_in_m')
        if self.efficiency is None or self.efficiency <= 0 or self.efficiency > 1:
            raise ValueError('Incorrect value for efficiency')
        if self.exposure_in_deg is None or self.exposure_in_deg < -180 or self.exposure_in_deg >  180:
            raise ValueError('Incorrect value for exposure_in_deg')
        if self.slope_in_deg is None or self.slope_in_deg < 0 or self.slope_in_deg > 180:
            raise ValueError('Incorrect value for slope_in_deg')
        if self.distance_between_arrays_in_m is None or self.distance_between_arrays_in_m < 0.1:
            raise ValueError('Incorrect value for distance_between_arrays_in_m')
        if self.mount_type is None or self.mount_type not in MOUNT_TYPE:
            raise ValueError('Incorrect value for mount_type')
        if self.solar_mask is not None and not isinstance(self.solar_mask, Mask):
            raise ValueError('Incorrect value for solar_mask')
        if self.number_of_cell_rows < 0:
            raise ValueError('Incorrect value for number_of_cell_rows')
        if self.temperature_coefficient < 0 or self.temperature_coefficient > 1:
            raise ValueError('Incorrect value for temperature_coefficient')
        

class PVsystem:

    def __init__(self, solar_model: SolarModel, surfacePV_in_m2: float, array_width: float, panel_height_in_m: float, efficiency: float, solar_mask: Mask=None, number_of_cell_rows: float = 10, temperature_coefficient: float = 0.0035) -> None:
        self.solar_model: SolarModel = solar_model
        self.ground_surface_in_m2: float = surfacePV_in_m2
        self.array_width: float = array_width
        self.panel_height_in_m: float = panel_height_in_m
        self.array_surface_in_m2: float = array_width * panel_height_in_m
        self.efficiency: float = efficiency
        self.skyline_azimuths_altitudes_in_deg: tuple[tuple[float, float]] = solar_model.site_weather_data.skyline
        self.solar_mask: Mask = solar_mask
        self.temperature_coefficient: float = temperature_coefficient
        self.number_of_cell_rows: float | Angle = number_of_cell_rows
        self.datetimes: list[datetime.datetime] = self.solar_model.site_weather_data.get('datetime')
        
    def plot_mask(self, **args):
        mask = SkyLineMask(*self.skyline_azimuths_altitudes_in_deg)
        if self.solar_mask is not None:
            mask = OrMask(mask, self.solar_mask)
        mask.plot(**args)

    def __solar_irradiance_to_photovoltaic_power(self, outdoor_temperature: float, solar_irradiance: float) -> float:
        TaNOCT: float = 46  # in °Celsius
        cell_temperature: float = outdoor_temperature + solar_irradiance / self.array_surface_in_m2 / 800 * (TaNOCT - 20)
        return (1 - self.temperature_coefficient * max(cell_temperature - 25, 0)) * solar_irradiance

    def array_production_in_kWh(self, exposure_in_deg: float, panel_slope_in_deg: float,  distance_between_arrays_in_m: float = None, shadowed: bool = False) -> buildingenergy.solar.SolarSystem:
        solar_system = SolarSystem(self.solar_model)
        outdoor_temperatures: list[float] = self.solar_model.temperatures

        if shadowed:
            cell_row_surface_in_m2: float = self.array_surface_in_m2 / self.number_of_cell_rows
            logging.info('Shadowed PV array surface %fm2, distance between arrays: %fm, slope: %f°, exposure: %f°' %
                         (self.array_surface_in_m2, distance_between_arrays_in_m, panel_slope_in_deg, exposure_in_deg))
            for i in range(self.number_of_cell_rows):
                minimum_sun_visible_altitude_in_deg: float = 180 * atan2(sin(panel_slope_in_deg / 180 * pi), self.number_of_cell_rows *
                                                                         distance_between_arrays_in_m / (self.number_of_cell_rows - i) / self.panel_height_in_m - cos(panel_slope_in_deg/180 * pi)) / pi
                front_side_mask: solar_system.RectangularMask = InvertedMask(RectangularMask(minmax_altitudes_deg=(minimum_sun_visible_altitude_in_deg, 180)))
                if self.solar_mask is not None:
                    front_side_mask = OrMask(self.solar_mask, front_side_mask)
                logging.info('row %i > alt.: %f°,' % (i, minimum_sun_visible_altitude_in_deg))
                solar_system.add_collector('shadowed array: cell row %i' % i, surface=cell_row_surface_in_m2, exposure_in_deg=exposure_in_deg,
                                           slope_in_deg=panel_slope_in_deg, solar_factor=self.efficiency / 1000, collector_mask=front_side_mask)

            direct_only_array_solar_collection, _collectors_production_in_kWh = solar_system.solar_gains_in_Wh(direct_only=True)
            total_array_solar_collection, _ = solar_system.solar_gains_in_Wh(direct_only=False)
            ratio: float = min(1, distance_between_arrays_in_m / self.panel_height_in_m)

            _array_production_in_kWh = [(1 - ratio) * direct_only_array_solar_collection[i] + ratio * total_array_solar_collection[i] for i in range(len(total_array_solar_collection))]
            _array_production_in_kWh = total_array_solar_collection
            for collector_name in _collectors_production_in_kWh:
                logging.info('> collector %s is producing %fkWh' % (collector_name, sum(_collectors_production_in_kWh[collector_name])))
        else:  # front array
            logging.info('Front PV array surface %fm2, slope: %f°, exposure: %f°' % (self.array_surface_in_m2, panel_slope_in_deg, exposure_in_deg))
            solar_system.add_collector('front array', surface=self.array_surface_in_m2, exposure_in_deg=exposure_in_deg,
                                       slope_in_deg=panel_slope_in_deg, solar_factor=self.efficiency/1000, collector_mask=self.solar_mask)
            _array_production_in_kWh, _collectors_production_in_kWh = solar_system.solar_gains_in_Wh(direct_only=False)
        return [self.__solar_irradiance_to_photovoltaic_power(outdoor_temperatures[i], _array_production_in_kWh[i]) for i in range(len(_array_production_in_kWh))]
    
    def number_of_arrays(self, distance_between_arrays_in_m: float, mount_type: MOUNT_TYPE) -> int:
        theoretical_number_of_arrays = round(self.ground_surface_in_m2 / self.array_width / distance_between_arrays_in_m)
        if mount_type == MOUNT_TYPE.FLAT:
            if distance_between_arrays_in_m < self.panel_height_in_m:
                return 0
            else:
                return theoretical_number_of_arrays
        elif mount_type == MOUNT_TYPE.SAW:
            if distance_between_arrays_in_m < .1:
                return 0
            else:
                return theoretical_number_of_arrays
        elif mount_type == MOUNT_TYPE.ARROW:
            if distance_between_arrays_in_m < .1:
                return 0
            elif theoretical_number_of_arrays % 2 == 0:
                return theoretical_number_of_arrays
            else:
                return theoretical_number_of_arrays - 1
        else:
            raise ValueError('Unknown mount type')

    def productions_in_kWh(self, exposure_in_deg: float, slope_in_deg: float, distance_between_arrays_in_m: float, mount_type: MOUNT_TYPE) -> list[float]:
        _number_of_arrays: int = self.number_of_arrays(distance_between_arrays_in_m, mount_type)
        if _number_of_arrays < 1:
            print('not enough place to install PV panels')
            return [0 for _ in self.datetimes]
        clear_head_array_productions_in_kWh: float = self.array_production_in_kWh(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, shadowed=False)
        logging.info('Front array electricity production: %fkWh' % sum(clear_head_array_productions_in_kWh))
        
        if mount_type == MOUNT_TYPE.FLAT:
            productions_in_kWh: list[float] = [prod * _number_of_arrays for prod in clear_head_array_productions_in_kWh]
        elif mount_type == MOUNT_TYPE.ARROW:
            number_of_clear_head_arrays: int = 1
            number_of_shadowed_head_arrays: int = floor((_number_of_arrays - 1)/2)
            number_of_shadowed_tail_arrays: int = floor((_number_of_arrays - 1)/2)
            number_of_clear_tail_arrays: int = (_number_of_arrays-1) % 2
            logging.info('%i arrays: %i clear head, %i  shadowed heads and tails and %i clear tail' % (number_of_clear_head_arrays+number_of_shadowed_head_arrays +
                            number_of_shadowed_tail_arrays+number_of_clear_tail_arrays, number_of_clear_head_arrays, number_of_shadowed_head_arrays, number_of_clear_tail_arrays))
            shadowed_head_array_productions_in_kWh: float = self.array_production_in_kWh(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, shadowed=True)
            logging.info('Shadowed head array electricity production: %fkWh' % sum(shadowed_head_array_productions_in_kWh))
            shadowed_tail_array_productions_in_kWh: float = self.array_production_in_kWh(exposure_in_deg-180, slope_in_deg, distance_between_arrays_in_m, shadowed=True)
            logging.info('Shadowed tail array electricity production: %fkWh' % sum(shadowed_tail_array_productions_in_kWh))
            clear_tail_array_productions_in_kWh: float = self.array_production_in_kWh(exposure_in_deg-180, slope_in_deg, distance_between_arrays_in_m, shadowed=False)
            logging.info('Rear array electricity production: %fkWh' % sum(clear_tail_array_productions_in_kWh))
            productions_in_kWh = [clear_head_array_productions_in_kWh[i] + number_of_shadowed_head_arrays * shadowed_head_array_productions_in_kWh[i] + number_of_shadowed_tail_arrays * shadowed_tail_array_productions_in_kWh[i] + number_of_clear_tail_arrays * clear_tail_array_productions_in_kWh[i] for i in range(len(clear_head_array_productions_in_kWh))]
        elif mount_type == MOUNT_TYPE.SAW:
            shadowed_array_productions_in_kWh = self.array_production_in_kWh(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, shadowed=True)
            logging.info('Shadowed array electricity production: %fkWh' % sum(shadowed_array_productions_in_kWh))
            productions_in_kWh = [clear_head_array_productions_in_kWh[i] + (_number_of_arrays - 1) * shadowed_array_productions_in_kWh[i] for i in range(len(clear_head_array_productions_in_kWh))]
        else:
            raise ValueError('Unknown mount type')
        return productions_in_kWh

    def pv_surface_in_m2(self, distance_between_arrays_in_m: float) -> float:
        return self.array_surface_in_m2 * round(self.ground_surface_in_m2 / self.array_width / distance_between_arrays_in_m)

    def __str__(self) -> str:
        string = 'PV system with array widths equal to %.2fm composed of panels with a height of %.2fm' % (self.array_width, self.panel_height_in_m)
        string += ' and a ground surface of %.2fm2' % (self.ground_surface_in_m2)
        return string

    def print_month_hour_productions(self, exposure_in_deg: float, slope_in_deg: float, distance_between_arrays_in_m: float, mount_type: MOUNT_TYPE, productions_in_kWh: list[float]):
        print('mount type: %s, exposure: %.0f°, slope: %.0f°, distance: %.2fm' % (mount_type, exposure_in_deg, slope_in_deg, distance_between_arrays_in_m))
        print('total electricity production: %.0fkWh' % sum(productions_in_kWh))

        month_hour_occurrences: dict[int, dict[int, int]] = [[0 for j in range(24)] for i in range(12)]
        month_hour_productions_in_kWh: dict[int, dict[int, float]] =[[0 for j in range(24)] for i in range(12)]
        table = prettytable.PrettyTable()
        table.set_style(prettytable.MSWORD_FRIENDLY)
        labels: list[str] = ["month#", "total"]
        labels.extend(['%i:00' % i for i in range(24)])
        table.field_names = labels
        for i, dt in enumerate(self.datetimes):
            month_hour_occurrences[dt.month-1][dt.hour] = month_hour_occurrences[dt.month-1][dt.hour] + 1
            month_hour_productions_in_kWh[dt.month-1][dt.hour] = month_hour_productions_in_kWh[dt.month-1][dt.hour] + productions_in_kWh[i]
        for month in range(12):
            number_of_month_occurrences: int = sum(month_hour_occurrences[month-1])
            if number_of_month_occurrences != 0:
                total: str = '%.fkWh' % (sum(month_hour_productions_in_kWh[month-1]))
            else:
                total: str = '0.'
            month_row = [month, total]
            for hour in range(24):
                if month_hour_occurrences[month][hour] != 0:
                    month_row.append('%.fW' % (1000 * month_hour_productions_in_kWh[month][hour] / month_hour_occurrences[month][hour]))
                else:
                    month_row.append('0.')
            table.add_row(month_row)
        print('Following PV productions are in Wh:')
        print(table)


class PVplant(PVsystem):
    
    def __init__(self, solar_model: SolarModel, pv_plant_properties: PVplantProperties):
        pv_plant_properties.check()
        super().__init__(solar_model, surfacePV_in_m2=pv_plant_properties.surfacePV_in_m2, array_width=pv_plant_properties.array_width, panel_height_in_m=pv_plant_properties.panel_height_in_m, efficiency=pv_plant_properties.efficiency, solar_mask = pv_plant_properties.solar_mask, number_of_cell_rows=pv_plant_properties.number_of_cell_rows, temperature_coefficient=pv_plant_properties.temperature_coefficient)
        self.pv_plant_properties: PVplantProperties = pv_plant_properties
        
    def productions_in_kWh(self) -> list[float]:
        return super().productions_in_kWh(self.pv_plant_properties.exposure_in_deg, self.pv_plant_properties.slope_in_deg, self.pv_plant_properties.distance_between_arrays_in_m, self.pv_plant_properties.mount_type)

def pv_productions_angles(pv_system: PVplant, exposures_in_deg: list[float], panel_slopes_in_deg: list[float], distance_between_arrays_in_m: float, mount_type: buildingenergy.solar.MOUNT_TYPE) -> list[float]:
    pv_productions_kWh: list[float] = numpy.zeros((len(panel_slopes_in_deg), len(exposures_in_deg)))
    productions_in_kWh_per_pv_surf: numpy.array = numpy.zeros((len(panel_slopes_in_deg), len(exposures_in_deg)))
    for i, slope_in_deg in enumerate(panel_slopes_in_deg):
        for j, exposure_in_deg in enumerate(exposures_in_deg):
            production = sum(pv_system.productions_in_kWh(exposure_in_deg, slope_in_deg, distance_between_arrays_in_m, mount_type))
            pv_productions_kWh[i, j] = production
            productions_in_kWh_per_pv_surf[i, j] = production / pv_system.pv_surface_in_m2(distance_between_arrays_in_m)
    return pv_productions_kWh, productions_in_kWh_per_pv_surf


def pv_productions_distances_slopes(pv_system: PVplant, exposure_in_deg: float, panel_slopes_in_deg: list[float], distances_between_arrays_in_m: list[float], mount_type: buildingenergy.solar.MOUNT_TYPE) -> tuple[list[list[float]], list[list[float]]]:
    productions_in_kWh: numpy.array = numpy.zeros((len(distances_between_arrays_in_m), len(panel_slopes_in_deg)))
    productions_in_kWh_per_pv_surf: numpy.array = numpy.zeros((len(distances_between_arrays_in_m), len(panel_slopes_in_deg)))
    for i, distance_between_arrays_in_m in enumerate(distances_between_arrays_in_m):
        for j, panel_slope_in_deg in enumerate(panel_slopes_in_deg):
            if panel_slope_in_deg > 10 or (distance_between_arrays_in_m > pv_system.panel_height_in_m + 0.1):
                production: float = sum(pv_system.productions_in_kWh(exposure_in_deg, panel_slope_in_deg, distance_between_arrays_in_m, mount_type))
                productions_in_kWh[i, j] = production
                productions_in_kWh_per_pv_surf[i, j] = production / pv_system.pv_surface_in_m2(distance_between_arrays_in_m)
            else:
                productions_in_kWh[i, j] = 0
                productions_in_kWh_per_pv_surf[i, j] = 0
    return productions_in_kWh, productions_in_kWh_per_pv_surf


if __name__ == '__main__':
    RectangularMask(minmax_azimuths_deg=(-80, 120), minmax_altitudes_deg=(30, 60)).plot()
    plt.show()
