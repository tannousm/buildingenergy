"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations
import numpy
import sites.model_h358
import buildingenergy.model
import time


class Runner:
    
    def __init__(self, model: buildingenergy.model.VaryingStateModel):
        self.model = model
        self._regular_input_values: dict[str, list[float]] = model.regular_input_values()
        
    @property
    def parameters(self) -> buildingenergy.parameters.ParameterSet:
        return self.model.parameters
    
    @property
    def data(self) -> buildingenergy.data.DataSet:
        return self.model.data
    
    def reset_data(self, data: buildingenergy.data.DataSet) -> None:
        self.model._set_data(data)
        
    def run(self) -> dict[str, list[float]]:
        self._airflows_values: dict[str, list[float]] = self.model.airflows_name_values()
        _variables_in_fingerprint_values = self.model.get_variables_in_fingerprint_values()
        self._output_values: dict[str, list[float]] = {name: list() for name in self.model.regular_output_names}
        
        state: numpy.matrix = None
        for k in range(len(self.model.data)):
            regular_input_values: dict[str, float] = {name: values[k] for name, values in self._regular_input_values.items()}
            state_model = self.model.state_model(_variables_in_fingerprint_values, self._airflows_values, k)
            if state is None:
                state = state_model.initialize(**regular_input_values)
            else:
                state_model.set_state(state)
            output_values: numpy.matrix = state_model.output(**regular_input_values)
            for i, name in enumerate(self.model.regular_output_names):
                self._output_values[name].append(output_values[i])
            state = state_model.step(**regular_input_values)
        return self._output_values
            
    @property
    def regular_input_values(self) -> dict[str, list[float]]:
        return self._regular_input_values
    
    @property
    def airflows_values(self) -> dict[str, list[float]]:
        return self._airflows_values
    
    @property
    def output_values(self) -> dict[str, list[float]]:
        return self._output_values
    
    def plot(self):
        for output_name in self.output_values:
            self.model.data.add_external_variable(output_name, self.output_values[output_name])
        self.model.data.plot()
    
if __name__ == '__main__':
    print('- Loading data')
    start = time.time()
    data: sites.data_h358.H358Data = sites.data_h358.H358Data('15/02/2015', '15/02/2016', number_of_levels=3)
    print('%i secs' % (time.time() - start))
    print('- Model generation')
    start = time.time()
    model = sites.model_h358.Model(order=None, data=data, parameters_levels=(5, 8, 2, 8, 8, 4, 4, 5, 0, 7, 4, 2, 6, 3))
    print('%i secs' % (time.time() - start))
    runner = Runner(model)
    print('- Simulation')
    simulation_start = time.time()
    runner.run()
    print('%i secs' % (time.time() - start))
    runner.plot()
    
