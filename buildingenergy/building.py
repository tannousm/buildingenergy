"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

This module is for the easy design of multi-zone buildings.
It models both the thermal behavior and the evolution of the CO2 concentration.
The thermal part is using thermal module
"""
from __future__ import annotations
from enum import Enum
from math import pi, sqrt
import abc
import numpy
import numpy.linalg
import copy
import networkx
import buildingenergy.data
import buildingenergy.physics
import buildingenergy.thermal
import buildingenergy.parameters
import buildingenergy.statemodel
import matplotlib.pyplot as plt


class SideType(Enum):
    """
    TYpes of interface between 2 zones where negative value means horizontal, positive means vertical.
    """
    HEATING_FLOOR = -6
    CANOPY = -5
    VELUX = -4
    GROUND = -3
    ROOF = -2
    SLAB = -1
    BRIDGE = 0
    WALL = 1
    DOOR = 2
    GLAZING = 3
    CUPBOARD = 4
    JOINERIES = 5


class Kind(Enum):
    """
    Location for a zone (wall or floor)
    """
    SIMULATED = 0
    OUTDOOR = 1
    INFINITE = 2  # temperature is known but indoor surface coefficients apply


class Direction(Enum):
    """
    Direction for a interface (wall or floor)
    """
    HORIZONTAL = 1
    VERTICAL = 2


def name_layers(left_name: str, right_name: str, number_of_layers: int, prefix: str = '') -> tuple[list[str], list[tuple[str, str]]]:
    """generate series of suffixes to name a given number of layers: name are provided for each layer but also for the 2 borders delimiting a layer

    :param left_name: left hand side existing layer name
    :type left_name: str
    :param right_name: right hand side existing layer name
    :type right_name: str
    :param number_of_layers: number of layers to be named
    :type number_of_layers: int
    :param prefix: prefix added to each name (and inside name by composition of names), defaults to ''
    :type prefix: str, optional
    :return: first a list of names for each layer and second, a list of pairs of variable names representing both sides of a layer. Of course, the left hand side name of a layer n is equal to the right hand side name of the layer n-1
    :rtype: tuple[list[str], list[tuple[str, str]]]
    """
    layer_border_names = list()
    layer_names = list()
    for i in range(number_of_layers):
        left_border_name: str = left_name + '_' + right_name + '_' + str(i - 1) if i > 0 else left_name
        right_border_name: str = left_name + '_' + right_name + '_' + str(i) if i < number_of_layers-1 else right_name
        layer_border_names.append((prefix+left_border_name, prefix+right_border_name))
        layer_names.append(prefix+left_name + '_' + right_name + '_' + str(i) + 'm')
    return layer_names, layer_border_names


class Zone:

    def __init__(self, name: str, kind: Kind = Kind.INFINITE):
        """
        Create a zone

        :param name: name of the zone (it will be trimmed and space will be replaced by '_'
        :type name: str
        :param kind: kind of zone, defaults to Kind.INFINITE, corresponding to a bounding zone where temperature and CO2 concentration are known. Outdoor is a special INFINITE zone, whereas INDOOR kind represents a room that must be simulated. The kind should not be provided: it is deduced by the system from the description of the building
        :type kind: Kind, optional
        """
        self.name: str = name.strip().replace(" ", "_")
        self.air_temperature_name: str = 'TZ' + self.name
        self.power_gain_name: str = 'PZ' + self.name
        self.air_capacitance_name: str = 'CZ' + self.name
        self.CO2_concentration_name: str = 'CCO2_' + self.name
        self.CO2_production_name: str = 'PCO2_' + self.name
        self.connected_zones: Zone = list()
        self.connected_airflows: list[Airflow] = list()
        self.volume = None
        self.kind: Kind = kind

    def _airflow(self, zone: Zone) -> Airflow:
        """Utility method giving the airflow joining the current zone to the specified one. None is returned if there's no airflow between the 2 zones

        :param zone: the zone that be connected to the current one
        :type zone: Zone
        :return: the connecting airflow or if it doesn't exist
        :rtype: Airflow
        """
        if zone not in self.connected_zones:
            return None
        i = self.connected_zones.index(zone)
        return self.connected_airflows[i]

    def set_volume(self, volume: float) -> None:
        """Set the volume of the zone and define it as a zone to be simulated

        :param volume: volume of the zone
        :type volume: float
        """
        self.volume = volume
        self.kind = Kind.SIMULATED

    @property
    def simulated(self) -> bool:
        """
        True is the current zone total incoming (or outgoing) air flow has to be simulated: it gets this status by setting a volume to the current zone.

        :return: True if it has to be simulated, False elsewhere;
        :rtype: bool
        """
        return self.volume is not None

    def __str__(self) -> str:
        string: str = '* ' if self.simulated else '* '
        string += '%s "%s" with temperature "%s", CO2 concentration "%s"' % (self.kind.name, self.name, self.air_temperature_name, self.CO2_concentration_name)
        if self.simulated:
            string += ', power gain "%s" and CO2 production "%s"' % (self.power_gain_name, self.CO2_production_name)
        string += ' with connected air flows:\n- ' + '\n- '.join([airflow.name for airflow in self.connected_airflows])
        return string + '\n'

    def __lt__(self, other_zone: Zone):
        return self.name < other_zone.name


class WallSide(abc.ABC):

    def __init__(self, zone1: Zone, zone2: Zone, side_type: SideType):
        """Initialize an abstract wall side i.e. a part of a wall. It can be a layered side or a block side.

        :param zone1: the 1st zone
        :type zone1: Zone
        :param zone2: the second zone
        :type zone2: Zone
        :param side_type: type of side (see SideType)
        :type side_type: SideType
        """
        self.zone1, self.zone2 = zone1, zone2
        self.side_type: SideType = side_type

    @property
    @abc.abstractmethod
    def Rs1_2(self) -> list[float]:
        """return a list of resistances corresponding to each layer, in the direction 1 to 2

        :raises NotImplementedError: abstract method
        :return: list of thermal resistances' values
        :rtype: list[float]
        """
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def Cs1_2(self) -> list[float]:
        """return a list of capacitances corresponding to each layer, in the direction 1 to 2

        :raises NotImplementedError: abstract method
        :return: list of thermal capacitances' values
        :rtype: list[float]
        """
        raise NotImplementedError

    @property
    def global_USs1_2(self):
        return 1 / sum([R if not None else 0 for R in self.Rs1_2])

    @property
    def global_Cs1_2(self):
        return sum([C if C is not None else 0 for C in self.Cs1_2])


class LayeredWallSide(WallSide):
    """
    A layered wall side is a part of wall composed by layers, where both extreme layers cannot be air layers. Additionally, 2 air layers cannot be stacked consecutively.
    """

    def __init__(self, zone1: Zone, zone2: Zone, side_type: SideType, surface: float):
        """
        Initialize a layered interface

        :param zone1: zone next to side 1
        :type zone1: Zone
        :param zone2: zone next to side 2
        :type zone2: Zone
        :param interface_type: type interface corresponding to layered interface
        :type interface_type: Interface
        :param surface: surface of the interface
        :type surface: float
        """
        super().__init__(zone1, zone2, side_type)
        self.surface = surface

        if self.side_type.value > 0:
            self.direction: Direction = Direction.VERTICAL
        else:
            self.direction = Direction.HORIZONTAL
        self.layers1_2: list[dict[str, float]] = list()

    def add_layer(self, material: str, thickness: float):
        """
        add a layer to the wall side starting from side 1

        :param material: short name of the thermal
        :type material: str
        :param thickness: _description_
        :type thickness: float
        """
        if material in buildingenergy.physics.library:
            self.layers1_2.append({'material': material, 'thickness': thickness})
        else:
            raise ValueError('"%s" not loaded in library' % material)

    @property
    def Rs1_2(self) -> list[float]:
        """
        Return the list of resistances, one for each layer of an interface, plus 2 surface resistances, one at each extremity

        :return: list of resistances
        :rtype: list[float]
        """
        resistances = list()
        if self.side_type.value == SideType.BRIDGE:
            raise ValueError('A bridge cannot appear in an wall side')
        if self.layers1_2[0]['material'] == 'air' or self.layers1_2[-1]['material'] == 'air':
            raise ValueError('An air layer cannot appear in an external layer.')
        for i in range(1, len(self.layers1_2)-1):
            if self.layers1_2[i]['material'] == 'air' and self.layers1_2[i+1]['material'] == 'air':
                raise ValueError('Consecutive air layer are prohibited: gather air layers.')
        if self.zone1.kind != Kind.OUTDOOR:
            resistances.append(buildingenergy.physics.library.indoor_surface_resistance(self.layers1_2[0]['material'], self.direction) / self.surface)
        else:
            resistances.append(buildingenergy.physics.library.outdoor_surface_resistance(self.layers1_2[0]['material'], self.direction) / self.surface)
        for i in range(len(self.layers1_2)):
            if self.layers1_2[i]['material'] != 'air':
                resistances.append(buildingenergy.physics.library.conduction_resistance(self.layers1_2[i]['material'], self.layers1_2[i]['thickness']) / self.surface)
            else:
                resistances.append(buildingenergy.physics.library.thermal_air_gap_resistance(self.layers1_2[i-1]['material'], self.layers1_2[i+1]['material'], self.layers1_2[i]['thickness'], self.direction) / self.surface)
        if self.zone2.kind != Kind.OUTDOOR:
            resistances.append(buildingenergy.physics.library.indoor_surface_resistance(self.layers1_2[-1]['material'], self.direction) / self.surface)
        else:
            resistances.append(buildingenergy.physics.library.outdoor_surface_resistance(self.layers1_2[-1]['material'], self.direction) / self.surface)
        return resistances

    @property
    def Cs1_2(self) -> list[float]:
        """
        Return the list of capacitances, one for each layer of an interface

        :return: list of capacitances, one for each layer of an interface
        :rtype: list[float]
        """
        capacitances = [None]
        for layer1_2 in self.layers1_2:
            if layer1_2['material'] == 'air':
                capacitances.append(None)
            else:
                density: float = buildingenergy.physics.library.get(layer1_2['material'])['density']
                Cp: float = buildingenergy.physics.library.get(layer1_2['material'])['Cp']
                capacitances.append(layer1_2['thickness'] * self.surface*density*Cp)
        capacitances.append(None)
        return capacitances

    def __str__(self) -> str:
        """
        :return: string depicting the layered interface
        :rtype: str
        """
        string = 'Layered wall side (%s, %s) type: %s surface: %.3fm2 with thermal losses at %.2fW/K and capacitance %.0fkJ/K composed of\n' % (self.zone1.name, self.zone2.name, str(self.side_type).split('.')[1], self.surface, 1/sum([R if R is not None else 0 for R in self.Rs1_2]), sum([C/1000 if C is not None else 0 for C in self.Cs1_2]))
        for i in range(len(self.Rs1_2)):
            if (i == 0) or (i == len(self.Rs1_2) - 1):
                string += '\t* %s %s > %.2fW/K)\n' % ('air', 'surface', 1/self.Rs1_2[i])
            else:
                string += '\t* %s, %.3fm > %.2fW/K, %.fkJ/K\n' % (self.layers1_2[i-1]['material'], self.layers1_2[i-1]['thickness'], 1/self.Rs1_2[i], self.Cs1_2[i]/1000 if self.Cs1_2[i] is not None else 0)
        return string


class BlockWallSide(WallSide):
    """
    A component interface is a wall side depicted by a global heat transmission coefficient without inertia
    """

    def __init__(self, zone1: Zone, zone2: Zone, side_type: SideType, total_US: float, total_capacitance: float = None):
        """
        Initialize a component interface

        :param zone1: zone next to side 1
        :type zone1: Zone
        :param zone2: zone next to side 2
        :type zone2: Zone
        :param interface_type: type interface corresponding to layered interface
        :type interface_type: Interface
        :param heat_transmission_coefficient: heat transmission coefficient in W/m2/K or in W/K if the surface is 1
        :type heat_transmission_coefficient: float
        :param surface: surface of the interface, default to 1
        :type surface: float, optional
        """
        super().__init__(zone1, zone2, side_type)
        self.total_US: float = total_US
        self.total_capacitance = total_capacitance

    @property
    def Rs1_2(self) -> list[float]:
        """
        global thermal loss (U) of the block side

        :return: thermal loss in W/K
        :rtype: float
        """
        return [1/self.total_US]

    @property
    def Cs1_2(self) -> list[float]:
        """
        global thermal capacitance (C) of the block side

        :return: thermal capacitance in J/K
        :rtype: float
        """
        return [None]

    def __str__(self) -> str:
        """
        :return: string depicting the component interface
        :rtype: str
        """
        return 'Block wall side (%s, %s) type: %s with losses at %fW/K and capacitance %.0fkJ/K\n' % (self.zone1.name, self.zone2.name, str(self.side_type).split('.')[1], 1/self.Rs1_2[0], self.Cs1_2[0]/1000 if self.Cs1_2[0] is not None else 0)


class Airflow:

    def __init__(self, zone1: Zone, zone2: Zone, nominal_value: float):  # , name: str=None
        """
        Create a bi-directional air flow between 2 zones. It is named as Q(name of zone1, name of zone2)

        :param zone1: first zone
        :type zone1: Zone
        :param zone2: second zone
        :type zone2: Zone
        :param nominal_value: the nominal value in m3/s used if not overloaded
        :type nominal_value: float
        """
        if zone1 == zone2:
            raise ValueError("Can't connect zone %s with itself" % zone1.name)
        if zone2 < zone1:
            zone1, zone2 = zone2, zone1
        self.connected_zones: list[Zone] = [zone1, zone2]
        self.name: str = 'Q(%s,%s)' % (zone1.name, zone2.name)
        self.nominal_value = nominal_value
        zone1.connected_airflows.append(self)
        zone2.connected_airflows.append(self)
        zone1.connected_zones.append(zone2)
        zone2.connected_zones.append(zone1)

    def __eq__(self, other_airflow) -> bool:
        return (other_airflow.zone1 in self.connected_zones) and (other_airflow.zone2 in self.connected_zones)

    def __str__(self) -> str:
        return 'airflow named "%s" connecting zone "%s" and zone "%s"' % (self.name, self.connected_zones[0].name, self.connected_zones[1].name)


class _Building(abc.ABC):
    """
    This class represents the thermal part of a building with its zones, its layered and block wall sides separating 2 zones.
    It contains a superset of the class building, which is the thermal part of the API exposed to the designer of building.
    """

    def __init__(self, *zone_names: str, periodic_depth_seconds: float, state_model_order_max: int, data_provider: buildingenergy.data.DataProvider):
        """
        Initialize a site

        :param zone_names: names of the zones except for 'outdoor', which is automatically created
        :type zone_names: Tuple[str]
        :param args: set the order of the resulting reduced order thermal state model with order=integer value. If the value is set to None, there won't be order reduction of the thermal state model, default to None
        :type args: Dict[str, float], optional
        """
        self.sample_time_seconds: int = data_provider.sample_time_in_secs
        self.data_provider: buildingenergy.DataProvider = data_provider
        self.state_model_order_max: int = state_model_order_max
        self.name_zones: dict[str, Zone] = dict()
        self.zone_names = zone_names
        for zone_name in zone_names:
            if zone_name in self.name_zones:
                raise ValueError('Zone %s is duplicated' % zone_name)
            self.name_zones[zone_name] = Zone(zone_name)
        self.layered_wall_sides: list[WallSide] = list()
        self.block_wall_sides: list[WallSide] = list()
        self.thermal_state_model_order = None
        self.name_zones['outdoor'] = Zone('outdoor', kind=Kind.OUTDOOR)
        self.thermal_network = None
        self.periodic_depth_seconds: float = periodic_depth_seconds

    @property
    def wall_sides(self) -> list[WallSide]:
        _wall_sides: list[WallSide] = self.layered_wall_sides.copy()
        _wall_sides.extend(self.block_wall_sides)
        return _wall_sides

    @property
    def simulated_zone_names(self) -> list[str]:
        simulated_zone_names: list[str] = list()
        for zone in self.zone_names:
            if self.name_zones[zone].volume is not None:
                simulated_zone_names.append(self.name_zones[zone])
        return simulated_zone_names

    def simulate_zone(self, zone_name: str) -> None:
        """
        define a zone as for being simulated regarding CO2 concentration

        :param zone_name: name of the zone
        :type zone_name: str
        :param zone_volume: volume of the zone
        :type zone_volume: float
        """
        if zone_name not in self.name_zones:
            raise ValueError("Can't simulate the zone %s because it has not been created before" % zone_name)
        self.name_zones[zone_name].set_volume(self.data_provider('office:volume'))

    def layered_wall_side(self, zone1_name: str, zone2_name: str, side_type: SideType, surface: float) -> LayeredWallSide:
        """
        add a layered wall side between 2 zones

        :param zone1_name: first zone name
        :type zone1_name: str
        :param zone2_name: second zone name
        :type zone2_name: str
        :param side_type: type of wall side
        :type side_type: SideType
        :param surface: surface of the interface in m2
        :type surface: float
        :return: the layered wall side where the layers can be added from zone1 to zone2
        :rtype: LayeredWallSide
        """
        layered_side = LayeredWallSide(self.name_zones[zone1_name], self.name_zones[zone2_name], side_type, surface)
        self.layered_wall_sides.append(layered_side)
        return layered_side

    def block_wall_side(self, zone1_name: str, zone2_name: str, side_type: SideType, total_US: float, total_capacitance: float = None) -> BlockWallSide:
        """add a block wall side with only one layer and potentially no capacitance

        :param zone1_name: first zone name
        :type zone1_name: str
        :param zone2_name: seconde zone name
        :type zone2_name: str
        :param side_type: type of side
        :type side_type: SideType
        :param total_US: total transmission coefficient for the whole block side
        :type total_US: float
        :param total_capacitance: total capacitance for the whole block side, defaults to None
        :type total_capacitance: float, optional
        :return: the created block side
        :rtype: BlockSide
        """
        self.block_wall_sides.append(BlockWallSide(self.name_zones[zone1_name], self.name_zones[zone2_name], side_type, total_US, total_capacitance))

    def __make_thermal_network(self) -> buildingenergy.thermal.ThermalNetwork:
        """
        Utility private method that transforms the geometrical description of a building, taking into account adjustment factors for model calibration, into a thermal network, from which a state model can be deduced. This method is called by the method 'make_state_model()' in Building.
        It has to be called anytime an adjustment factor is changed to regenerate a thermal network.

        :param side_Rfactor: adjustment multiplicative factors applying to the resulting resistances for the referred wall sides. It's a dictionary {(zone1, zone2): Rfactor1_2, (zone1, zone5): Rfactor1_5, ...}
        :type side_Rfactor: dict[tuple[str,str], float]
        :param side_Cfactor: adjustment multiplicative factors applying to the resulting capacitances for the referred wall sides. It's a dictionary {(zone1, zone2): Cfactor1_2, (zone1, zone5): Cfactor1_5, ...}
        :type side_Cfactor: dict[tuple[str,str], float]
        :param Vfactor: adjustment multiplicative factors applying to the specified zone. It's a dictionary {zone1: Vfactor1, zone2: Vfactor2, ...}
        :type Vfactor: dict[str,float]
        :return: the thermal network
        :rtype: buildingenergy.thermal.ThermalNetwork
        """
        self.thermal_network = buildingenergy.thermal.ThermalNetwork()
        air_properties: dict[str, float] = buildingenergy.physics.library.get('air')
        rhoCp_air = air_properties['density'] * air_properties['Cp']

        for zone_name in self.name_zones:
            zone = self.name_zones[zone_name]
            if zone.kind == Kind.SIMULATED:
                self.thermal_network.T(zone.air_temperature_name, buildingenergy.thermal.CAUSALITY.OUT)
                self.thermal_network.HEAT(T=zone.air_temperature_name, name=zone.power_gain_name)
                Cair: float = rhoCp_air * self.data_provider('%s:volume' % zone_name)
                self.thermal_network.C(toT=zone.air_temperature_name, name=zone.air_capacitance_name, value=Cair)
            else:
                self.thermal_network.T(zone.air_temperature_name, buildingenergy.thermal.CAUSALITY.IN)

        for wall_side in self.wall_sides:
            if ('%s-%s:Rfactor' % (wall_side.zone1.name, wall_side.zone2.name)) in self.data_provider:
                Rfactor = self.data_provider('%s-%s:Rfactor' % (wall_side.zone1.name, wall_side.zone2.name))
            else:
                Rfactor = 1
            if ('%s-%s:Cfactor' % (wall_side.zone1.name, wall_side.zone2.name)) in self.data_provider:
                Cfactor = self.data_provider('%s-%s:Cfactor' % (wall_side.zone1.name, wall_side.zone2.name))
            else:
                Cfactor = 1
            number_of_layers = len(wall_side.Rs1_2)
            layer_names, layer_border_names = name_layers(wall_side.zone1.air_temperature_name, wall_side.zone2.air_temperature_name, number_of_layers)
            for layer_index in range(number_of_layers):
                layer_left_temperature_name = layer_border_names[layer_index][0]
                middle_layer_temperature_name = layer_names[layer_index]
                layer_right_temperature_name = layer_border_names[layer_index][1]
                R = Rfactor * wall_side.Rs1_2[layer_index] / 2
                if wall_side.Cs1_2[layer_index] is None:
                    self.thermal_network.R(fromT=layer_left_temperature_name, toT=layer_right_temperature_name, value=2*R)
                else:
                    C = Cfactor * wall_side.Cs1_2[layer_index]
                    n_sublayers = 1
                    try:
                        n_sublayers = round(sqrt(pi * R * C / self.periodic_depth_seconds))
                    except:  # noqa
                        n_sublayers = 1
                    if n_sublayers <= 1:
                        self.thermal_network.R(fromT=layer_left_temperature_name, toT=middle_layer_temperature_name, value=R)
                        self.thermal_network.R(fromT=middle_layer_temperature_name, toT=layer_right_temperature_name, value=R)
                        self.thermal_network.C(toT=middle_layer_temperature_name, value=C)
                    else:
                        sublayer_names, sublayer_border_names = name_layers(layer_left_temperature_name, layer_right_temperature_name, n_sublayers)
                        R = Rfactor * wall_side.Rs1_2[layer_index] / 2 / n_sublayers
                        C = Cfactor * wall_side.Cs1_2[layer_index] / n_sublayers
                        for sublayer_index in range(n_sublayers):
                            sublayer_left_name = sublayer_border_names[sublayer_index][0]
                            sublayer_middle_name = sublayer_names[sublayer_index]
                            sublayer_right_name = sublayer_border_names[sublayer_index][1]
                            self.thermal_network.R(fromT=sublayer_left_name, toT=sublayer_middle_name, value=R)
                            self.thermal_network.R(fromT=sublayer_middle_name, toT=sublayer_right_name, value=R)
                            self.thermal_network.C(toT=sublayer_middle_name, value=C)
        return self.thermal_network

    def __str__(self) -> str:
        string = 'Zones are:\n'
        for zone in self.name_zones:
            string += str(self.name_zones[zone])
        string += 'Wall sides are:\n'
        for wall_side in self.wall_sides:
            string += str(wall_side)
        return string


class Building(_Building):
    """
    This class complements the class _Building: it contains the API dedicated to the processing into a state model.
    """
    def __init__(self, *zone_names: str, data_provider: buildingenergy.data.DataProvider, periodic_depth_seconds: float = 60*60, state_model_order_max: int = None):
        """
        Initialize a building and help to create it. It generates as state model afterwards thanks to the method 'make_state_model()', that must be called anytime an adjustment multiplicative factor is modified

        :param zone_names: names of the zones except for 'outdoor', which is automatically created
        :type zone_names: tuple[str]
        :param periodic_depth_seconds: the target periodic depth penetration is a wave length, which is defining the decomposition in sublayers, each sublayer is decomposed such that its thickness is attenuating a temperature of this wave length: the smallest, the more precise but also the more computation time.
        :type periodic_depth_seconds:
        :param state_model_order_max: set a maximum order for the final state model. If the value is set to None, there won't be order reduction, default to None
        :type state_model_order_max: int, optional
        :param sample_time_in_secs: sample time for future version (only the default 3600s has been tested), default is 3600s, don't change it
        :type sample_time_in_secs: int, optional
        """
        super().__init__(*zone_names, periodic_depth_seconds=periodic_depth_seconds, state_model_order_max=state_model_order_max, data_provider=data_provider)
        self.airflow_network: networkx.Graph = networkx.Graph()
        for zone_name in self.name_zones:
            self.airflow_network.add_node(zone_name)
        self.V_nominal_reduction_matrix: numpy.matrix = None
        self.W_nominal_reduction_matrix: numpy.matrix = None
        self.nominal_fingerprint: int = None
        self.airflows = list()
        self.CO2_connected_zones = list()

    def connect_airflow(self, zone1_name: str, zone2_name: str, nominal_value: float):
        """
        create an airflow exchange between 2 zones

        :param zone1_name: zone name of the origin of the air flow
        :type zone1_name: str
        :param zone2_name: zone name of the destination of the air flow
        :type zone2_name: str
        :param nominal_value: nominal value for air exchange, used if not overloaded
        :type nominal_value: float
        """
        self.airflow_network.add_edge(zone1_name, zone2_name)
        airflow = Airflow(self.name_zones[zone1_name], self.name_zones[zone2_name], nominal_value)
        self.airflows.append(airflow)
        if self.name_zones[zone1_name] not in self.CO2_connected_zones:
            self.CO2_connected_zones.append(self.name_zones[zone1_name])
        if self.name_zones[zone2_name] not in self.CO2_connected_zones:
            self.CO2_connected_zones.append(self.name_zones[zone2_name])

    def make_state_model(self, current_airflow_values: dict[str, float] = dict(), reset_reduction: bool = False, fingerprint: int = None) -> buildingenergy.statemodel.StateModel:
        if reset_reduction:
            self.V_nominal_reduction_matrix = None
            self.W_nominal_reduction_matrix = None
        if len(self.airflows) == 0:
            self.global_state_model = self.__make_thermal_network().state_model()  # 
            self.V_nominal_reduction_matrix, self.W_nominal_reduction_matrix = self.global_state_model.reduce(self.thermal_state_model_order, self.V_nominal_reduction_matrix, self.W_nominal_reduction_matrix)
            return self.global_state_model

        if self.V_nominal_reduction_matrix is None:
            nominal_airflow_values = {airflow.name: airflow.nominal_value for airflow in self.airflows}
            self.global_state_model = self.make_full_order_state_mode(nominal_airflow_values)
            self.V_nominal_reduction_matrix, self.W_nominal_reduction_matrix = self.global_state_model.reduce(self.thermal_state_model_order, self.V_nominal_reduction_matrix, self.W_nominal_reduction_matrix)
        self.global_state_model = self.make_full_order_state_model(current_airflow_values)
        self.V_nominal_reduction_matrix, self.W_nominal_reduction_matrix = self.global_state_model.reduce(self.thermal_state_model_order, self.V_nominal_reduction_matrix, self.W_nominal_reduction_matrix)
        return self.global_state_model

    def make_full_order_state_model(self, airflow_values: dict[str, float] = dict()) -> buildingenergy.statemodel.StateModel:
        self.thermal_network: buildingenergy.thermal.ThermalNetwork = self.__make_thermal_network()
        air_properties: dict[str, float] = buildingenergy.physics.library.get('air')
        rhoCp_air: float = air_properties['density'] * air_properties['Cp']
        for airflow in self.airflows:
            zone1, zone2 = airflow.connected_zones
            self.thermal_network.R(fromT=zone1.air_temperature_name, toT=zone2.air_temperature_name, name='Rv%s_%s' % (zone1.name, zone2.name), value=rhoCp_air * airflow_values[airflow.name] if airflow.name in airflow_values else airflow.nominal_value)

        full_order_state_model = self.__make_thermal_network().state_model()
        CO2state_matrices = self.__make_CO2_state_model(airflow_values)
        A_CO2 = CO2state_matrices['A']
        B_CO2 = numpy.hstack((CO2state_matrices['B_CO2'], CO2state_matrices['B_prod']))
        C_CO2 = CO2state_matrices['C']
        D_CO2 = numpy.hstack((CO2state_matrices['D_CO2'], CO2state_matrices['D_prod']))
        input_names = CO2state_matrices['U_CO2']
        input_names.extend(CO2state_matrices['U_prod'])
        output_names = CO2state_matrices['Y']
        full_order_state_model.extend('CO2', (A_CO2, B_CO2, C_CO2, D_CO2), input_names, output_names)
        return full_order_state_model

    def __make_CO2_state_model(self, airflow_values: dict[str, float]) -> dict:
        """
        Generate the state model representing the CO2 evolution

        :param airflow_values: connecting airflows values as a dictionary with the airflow names as keys and the values as airflow values
        :type airflow_values: dict[str, float]
        :param zone_Vfactors: multiplicative adjustment factors for the zone air volumes as a dictionary with zone names as keys and corrective factor as value, default to empty dictionary
        :type zone_Vfactors: dict[str, float], optional
        :return: State space model for the CO2
        :rtype: STATE_MODEL
        """
        simulated_zones = list()
        input_zones = list()
        state_variable_names: list[str] = list()
        input_variable_names: list[str] = list()
        production_names: list[str] = list()

        for zone_name in self.name_zones:
            zone = self.name_zones[zone_name]
            if zone.simulated:
                simulated_zones.append(zone)
                state_variable_names.append(zone.CO2_concentration_name)
                production_names.append(zone.CO2_production_name)
            elif len(zone.connected_zones) > 0:
                input_zones.append(zone)
                input_variable_names.append(zone.CO2_concentration_name)

        A_CO2 = numpy.zeros((len(state_variable_names), len(state_variable_names)))
        B_CO2 = numpy.zeros((len(state_variable_names), len(input_variable_names)))
        B_prod = numpy.zeros((len(state_variable_names), len(production_names)))
        C_CO2 = numpy.eye(len(state_variable_names))
        D_CO2 = numpy.zeros((len(state_variable_names), len(input_variable_names)))
        D_prod = numpy.zeros((len(state_variable_names), len(production_names)))

        for i, zone in enumerate(simulated_zones):
            A_CO2[i, i] = 0
            for connecting_airflow in zone.connected_airflows:
                if connecting_airflow.name in airflow_values:
                    A_CO2[i, i] += -airflow_values[connecting_airflow.name] / (zone.volume) 
                else:
                    A_CO2[i, i] += -connecting_airflow.nominal_value / (zone.volume)
            B_prod[i, 0] = 1/zone.volume
            for connected_zone in zone.connected_zones:
                if connected_zone.simulated:
                    connecting_airflow = zone._airflow(connected_zone)
                    j: int = state_variable_names.index(connected_zone.CO2_concentration_name)
                    A_CO2[i, j] = airflow_values[connecting_airflow.name] / zone.volume if connecting_airflow.name in airflow_values else connecting_airflow.nominal_value / zone.volume
                else:
                    connecting_airflow = zone._airflow(connected_zone)
                    j = input_variable_names.index(connected_zone.CO2_concentration_name)
                    B_CO2[i, j] = airflow_values[connecting_airflow.name] / zone.volume if connecting_airflow.name in airflow_values else connecting_airflow.nominal_value / zone.volume
        self.CO2_state_model = buildingenergy.statemodel.StateModel((A_CO2, B_CO2, C_CO2, D_CO2), input_variable_names, state_variable_names, self.sample_time_seconds)
        return {'A': A_CO2, 'B_CO2': B_CO2, 'B_prod': B_prod, 'C': C_CO2, 'D_CO2': D_CO2, 'D_prod': D_prod, 'Y': state_variable_names, 'X': state_variable_names, 'U_CO2': input_variable_names, 'U_prod': production_names, 'type': 'differential'}

    def plot_thermal_net(self):
        """
        draw digraph of the thermal network (use matplotlib.show() to display)
        """
        self.thermal_network.draw()

    def plot_airflow_net(self):
        """
        draw digraph of the airflow network (use matplotlib.show() to display)
        """

        pos = networkx.shell_layout(self.airflow_network)
        node_colors = list()
        for zone_name in self.zones:
            if self.zones[zone_name].is_known:
                node_colors.append('blue')
            elif self.zones[zone_name].simulated:
                node_colors.append('pink')
            else:
                node_colors.append('yellow')
        labels = dict()
        for node in self.airflow_network.nodes:
            label = '\n' + str(self.zones[node]._propagated_airflow)
            labels[node] = label
        plt.figure()
        networkx.draw(self.airflow_network, pos, with_labels=True, edge_color='black', width=1, linewidths=1, node_size=500, font_size='medium', node_color=node_colors, alpha=1)
        networkx.drawing.draw_networkx_labels(self, pos,  font_size='x-small', verticalalignment='top', labels=labels)

    def __str__(self) -> str:
        """
        :return: string depicting the site
        :rtype: str
        """
        string = str(super().__str__())
        string += 'Connected zones:\n'
        for airflow in self.airflows:
            string += '* %s with a nominal value of %.2fm3/h\n' % (str(airflow), 3600 * airflow.nominal_value)
        return string
