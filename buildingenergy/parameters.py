"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

This module deals with parameters that can intervene in data (ParameterizedData) or in the models.
"""
from __future__ import annotations
import os
import json
import configparser
import prettytable
import re


config = configparser.ConfigParser()
config.read('setup.ini')


class ParameterSet:
    """
    A parameter is referring to a value that may change from one hour simulation step to another, but more commonly, from a simulation to another. A specific type of parameters is the nonlinear input variable, which is connected to the model (in practice, some air flows) and is a cause of bilinear nonlinearity. They are not considered as adjustable because they are not concerned by parameter adjustment process.
    """

    def __init__(self) -> None:
        """
        Initialize a parameter set with given resolution, used to store in a cache already computed state models in order to reduce computations but also to estimate the parameters' values. If resolution = n, then the parameter intervals will be decomposed in n and all the state models belonging to the same hypercube are considered as equal. 

        :param resolution: Resolution corresponds to the number of value levels a nonlinear input is approximated to be replaced by a cached close state model. The resolution has to be at least equal to 2. The higher, the more precise but also the slower, and conversely;
        :type resolution: int
        """
        self.initial_parameter_values: dict[str, float] = dict()
        self._parameter_names: list[str] = list()
        self.known_parameter_values: dict[str, float] = dict()
        self.adjustable_parameter_names: list[str] = list()
        self.adjustable_resolutions: dict[str, float] = dict()
        self.adjustable_parameter_levels: dict[str, int] = dict()
        self.adjustable_level_bounds: dict[str, tuple[int, int]] = dict()
        self.adjustable_parameter_bounds: dict[str, tuple[float, float]] = dict()
        self.zone_parameters: dict[str, list[str]] = dict()
        self.connected_zones_parameters: dict[tuple[str, str], list[str]] = dict()

    def __contains__(self, parameter_name: str) -> bool:
        if re.match('^\\w+-\\w+:\\w+$', parameter_name):
            zone1, zone2, simple_parameter_name = re.findall('^(\\w+)-(\\w+):(\\w+)$', parameter_name)[0]
            if zone2 < zone1:
                zone1, zone2 = zone2, zone1
            parameter_name = '%s-%s:%s' % (zone1, zone2, simple_parameter_name)
        return parameter_name in self._parameter_names

    def __call__(self, parameter_name: str, parameter_value: float = None, bounds_resolution: tuple[float, float, float] = None) -> float:
        if re.match('^\\w+:\\w+$', parameter_name):
            zone, simple_parameter_name = re.findall('^(\\w+):(\\w+)$', parameter_name)[0]
            if zone in self.zone_parameters and simple_parameter_name not in self.zone_parameters[zone]:
                self.zone_parameters[zone].append(simple_parameter_name)
            else:
                self.zone_parameters[zone] = [simple_parameter_name]
        elif re.match('^\\w+-\\w+:\\w+$', parameter_name):
            zone1, zone2, simple_parameter_name = re.findall('^(\\w+)-(\\w+):(\\w+)$', parameter_name)[0]
            if zone2 < zone1:
                zone1, zone2 = zone2, zone1
            if (zone1, zone2) in self.connected_zones_parameters and simple_parameter_name not in self.connected_zones_parameters[(zone1, zone2)]:
                self.connected_zones_parameters[(zone1, zone2)].append(simple_parameter_name)
            else:
                self.connected_zones_parameters[(zone1, zone2)] = [simple_parameter_name]
            parameter_name = '%s-%s:%s' % (zone1, zone2, simple_parameter_name)
        elif not re.match('^\\w+$', parameter_name):
            raise ValueError("Invalid parameter name: %s" % parameter_name)

        if parameter_value is None:  # get a value
            if parameter_name in self.known_parameter_values:
                return self.known_parameter_values[parameter_name]
            elif parameter_name in self.adjustable_parameter_names:
                return self._level_to_value(parameter_name, self.adjustable_parameter_levels[parameter_name])
            else:
                raise ValueError('Unknown parameter "%s"' % parameter_name)
        elif type(parameter_value) is float or type(parameter_value) is int:  # set a simple float/int parameter value, possibly adjustable
            if parameter_name not in self._parameter_names:
                self._parameter_names.append(parameter_name)
            if parameter_name not in self.initial_parameter_values:
                self.initial_parameter_values[parameter_name] = parameter_value
            if bounds_resolution is not None:
                if bounds_resolution[0] < bounds_resolution[1]:
                    if parameter_value < bounds_resolution[0] or parameter_value > bounds_resolution[1]:
                        raise ValueError('Parameter name "%s" is initialized out of is range' % parameter_name)
                    self.adjustable_resolutions[parameter_name] = bounds_resolution[2]
                    self.adjustable_parameter_bounds[parameter_name] = (bounds_resolution[0], bounds_resolution[1])
                    self.adjustable_level_bounds[parameter_name] = (0, (bounds_resolution[1] - bounds_resolution[0]) // bounds_resolution[2])
                    self.adjustable_parameter_names.append(parameter_name)
                else:
                    raise ValueError('Recheck bounds and resolution for parameter "%s"' % parameter_name)
                self.adjustable_parameter_levels[parameter_name] = self._value_to_level(parameter_name, self.initial_parameter_values[parameter_name])
            elif parameter_name in self.adjustable_parameter_names:
                # parameter_index = self.adjustable_parameter_names.index(parameter_name)
                # # self._adjustable_values[parameter_index] = parameter_value
                print(self.adjustable_parameter_levels[parameter_name])
                print(self.levels())
                self.adjustable_parameter_levels[parameter_name] = self._value_to_level(parameter_name, parameter_value)
                print(self.adjustable_parameter_levels[parameter_name])
                print(self.levels())
            else:
                self.known_parameter_values[parameter_name] = parameter_value
        else:
            raise ValueError('Incompatible value type for parameter value "%s"' % parameter_name)

    def get_zone_parameter_names(self, zone_name: str) -> list[str]:
        return ['%s:%s' % (zone_name, parameter_name) for parameter_name in self.zone_parameters[zone_name]]

    def get_connected_zones_parameter_names(self, zone1_name: str, zone2_name: str) -> list[str]:
        if zone2_name < zone1_name:
            zone1_name, zone2_name = zone2_name, zone1_name
        return ['%s-%s:%s' % (zone1_name, zone2_name, parameter_name) for parameter_name in self.connected_zones_parameters[(zone1_name, zone2_name)]]

    def set_adjustable_levels(self, levels: list[int]) -> None:
        for i, adjustable_parameter in enumerate(self.adjustable_parameter_names):
            self.adjustable_parameter_levels[adjustable_parameter] = levels[i]

    @property
    def values(self) -> dict[str, float]:
        return {parameter_name: self(parameter_name) for parameter_name in self._parameter_names}

    def _value_to_level(self, parameter_name: str, parameter_value: float = None) -> int:
        return int((parameter_value - self.adjustable_parameter_bounds[parameter_name][0]) // self.adjustable_resolutions[parameter_name])

    def _level_to_value(self, parameter_name: str, level: int) -> float:
        return self.adjustable_parameter_bounds[parameter_name][0] + level * self.adjustable_resolutions[parameter_name]

    def reset(self):
        initial_levels = list()
        for i in range(self.number_of_ajustables):
            initial_levels[self.adjustable_parameter_names[i]] = self._value_to_level(self.adjustable_parameter_names[i], self.initial_parameter_values[self.adjustable_parameter_names[i]])
        self.adjustable_parameter_levels = initial_levels

    def levels(self, parameter_names: str = None) -> int | tuple[int]:
        if parameter_names is None:
            parameter_names = self.adjustable_parameter_names
        if type(parameter_names) is not list:
            parameter_names = list(parameter_names)
        return [self.adjustable_parameter_levels[parameter_name] for parameter_name in parameter_names]

    @property
    def number_of_ajustables(self) -> int:
        return len(self.adjustable_parameter_names)

    @property
    def adjustable_values(self) -> list[float]:
        return [self._level_to_value(parameter_name, self.adjustable_parameter_levels[parameter_name]) for i, parameter_name in enumerate(self.adjustable_parameter_names)]

    def save(self, file_name):
        saved: dict = dict()
        saved['initial_parameter_values'] = self.initial_parameter_values
        saved['parameter_names'] = self._parameter_names
        saved['known_parameter_values'] = self.known_parameter_values
        saved['adjustable_parameter_names'] = self.adjustable_parameter_names
        saved['adjustable_resolutions'] = self.adjustable_resolutions
        saved['adjustable_parameter_levels'] = self.adjustable_parameter_levels
        saved['adjustable_level_bounds'] = self.adjustable_level_bounds
        saved['adjustable_parameter_bounds'] = self.adjustable_parameter_bounds

        with open(config['folders']['results'] + file_name + '.json', "w") as outfile:
            outfile.write(json.dumps(saved))

    def load(self, parameters_filename: str = "best_parameters"):
        if os.path.exists(config['folders']['data'] + parameters_filename + '.json'):
            with open(config['folders']['data'] + parameters_filename, 'r') as file:
                saved: dict = json.load(file)
            self.initial_parameter_values = saved['initial_parameter_values']
            self._parameter_names = saved['parameter_names']
            self.known_parameter_values = saved['known_parameter_values']
            self.adjustable_parameter_names = saved['adjustable_parameter_names']
            self.adjustable_resolutions = saved['adjustable_resolutions']
            self.adjustable_parameter_levels = saved['adjustable_parameter_levels']
            self.adjustable_level_bounds = saved['adjustable_level_bounds']
            self.adjustable_parameter_bounds = saved['adjustable_parameter_bounds']

    def __len__(self) -> int:
        return len(self._parameter_names)

    def __str__(self) -> str:
        """
        Return a string representation of a parameter set

        :return: a string representation of a parameter set
        :rtype: str
        """
        pretty_table = prettytable.PrettyTable(header=True)
        # pretty_table.set_style(prettytable.MSWORD_FRIENDLY)
        pretty_table.add_column('name', self.adjustable_parameter_names)
        pretty_table.add_column('value', ['%.3g' % v for v in self.adjustable_values])
        pretty_table.add_column('v. bounds', ['(%.3g, %.3g)' % self.adjustable_parameter_bounds[pname] for pname in self.adjustable_parameter_names])
        pretty_table.add_column('level', ['%i' % self.adjustable_parameter_levels[parameter_name] for parameter_name in self.adjustable_parameter_levels])
        pretty_table.add_column('l. bounds', ['(%i, %i)' % self.adjustable_level_bounds[pname] for pname in self.adjustable_parameter_names])

        return pretty_table.__str__()
