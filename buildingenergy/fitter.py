"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0

This module is a top level module in the way it connects measurement data, parameters and models.
"""
from __future__ import annotations
import numpy
import numpy.linalg
import time
import prettytable
import SALib.sample.morris
import SALib.analyze.morris
import plotly.express
from random import randint
import buildingenergy.data
import buildingenergy.model
import buildingenergy.parameters
import buildingenergy.statemodel
import buildingenergy.runner
import configparser

def setup(*references):
    _setup = configparser.ConfigParser()
    _setup.read('setup.ini')
    configparser_obj = _setup
    for ref in references:
        configparser_obj = configparser_obj[ref]
    return configparser_obj
    

class ModelFitter:
    """
    Runner is the main class for simulating a site model: it connects measurement data, parameters and models.
    """
    
    def __init__(self, varying_state_model: buildingenergy.model.VaryingStateModel, verbose: bool=False) -> None:
        """
        Initialize a runner i.e. a runnable simulator with the design of the following objects:
        - a parameter set, containing the names og nonlinear inputs (some airflows) with their bounds, to approximate the state model
        - a (state) model, with identified nonlinear input variables 
        - a data set, which contains all the data bounded to yield the state
        :param model_data_bindings: connect a model variable to one or several data. If several data are provided, they are summed up. Instead of data, parameterized data can be provided.
        :type model_data_bindings: tuple[str, str | list[str]])
        """
        self.varying_state_model: buildingenergy.model.Model = varying_state_model
        self.training_data_provider: buildingenergy.data.DataProvider = varying_state_model.dp
        self.parameters: buildingenergy.parameters.ParameterSet = self.training_data_provider.parameter_set
        self.verbose = verbose
        self.varying_state_model.verbose = verbose
        
        self.adjustable_parameter_level_bounds: dict[str, tuple[int, int]] = self.parameters.adjustable_level_bounds
        self.adjustable_parameter_levels: list[int] = self.parameters.adjustable_parameter_levels
        
    def run(self) -> dict[str, list[float]]:
        self.parameters.set_adjustable_levels(self.parameters.levels())
        return self.varying_state_model.simulate()
    
    def error(self, output_values: dict[str, list[float]]) -> float:
        total_error: float = 0
        for output_name in output_values:
            # output_data_name = self.training_data_provider.model_data_bindings.data_name(output_model_name)
            data_bounds: tuple[float, float] = self.training_data_provider.bounds(output_name)
            output_error = sum([abs(output_values[output_name][k] - self.training_data_provider(output_data_name, k)) for  k in range(len(self.training_data_provider))])
            total_error += output_error / len(self.training_data_provider) / abs(data_bounds[1] - data_bounds[0])
        return total_error
                
    def fit(self, n_iterations: int, validation_data_provider: buildingenergy.data.DataProvider) -> tuple[dict[str, list[float]], float]:
        iteration: int = 0
        best_learning_error: float = None
        best_learning_outputs: dict[str, list[float]] = None
        parameters_tabu_list: list[tuple[int]] = list()
        number_of_adjustable_parameters: int = len(self.parameters.adjustable_parameter_names)
        adjustable_parameter_levels: tuple[int] = self.parameters.adjustable_parameter_levels
        no_progress_counter: int = 0
        
        while iteration < n_iterations and no_progress_counter < 2 * number_of_adjustable_parameters:
            if self.verbose:
                print('levels: ' + ','.join([str(l) for l in adjustable_parameter_levels]))
            candidate_outputs: dict[str, list[float]] = self.run()
            candidate_error: float = self.error(candidate_outputs)
            if self.verbose:
                print('-> candidate error:', candidate_error)
                print('* Iteration %i/%i' % (iteration, n_iterations-1))  # time analysis
            parameters_tabu_list.append(adjustable_parameter_levels)
            if best_learning_error is None or candidate_error < best_learning_error:
                if best_learning_error is None:
                    initial_error: float = candidate_error
                    initial_levels: list[int] = tuple(adjustable_parameter_levels)
                best_parameter_levels: list[int] = tuple(adjustable_parameter_levels)
                best_learning_outputs = candidate_outputs
                best_learning_error = candidate_error
                print('Best error: %f' % best_learning_error, 'with', ','.join([str(int(p)) for p in best_parameter_levels]))
                no_progress_counter = 0
            else:
                no_progress_counter += 1
            candidate_found: bool = False
            counter: int = 0
            new_parameter_levels = None
            while not candidate_found and counter < 2 * number_of_adjustable_parameters:
                new_parameter_levels: list[int] = list(best_parameter_levels)
                parameter_to_change = randint(0, number_of_adjustable_parameters-1)
                change = randint(0,1) * 2 - 1
                if new_parameter_levels[parameter_to_change] + change < 0 or new_parameter_levels[parameter_to_change] + change > number_of_adjustable_parameters - 1:
                    change = - change
                new_parameter_levels[parameter_to_change] = new_parameter_levels[parameter_to_change] + change
                candidate_found = new_parameter_levels not in parameters_tabu_list
                counter += 1
            if counter >= 2 * number_of_adjustable_parameters:
                iteration = n_iterations
            else:
                adjustable_parameter_levels: tuple[int] = tuple(new_parameter_levels)
                self.training_data_provider.parameter_set.set_adjustable_levels(new_parameter_levels)
                iteration += 1
        
        self.training_data_provider.parameter_set.set_adjustable_levels(best_parameter_levels)
        self.training_data_provider.parameter_set.save('parameters%i' % time.time())
        
        contact = []
        adjustables_bounds_str: list[str] = ['(%.5f,%.5f)' % self.training_data_provider.parameter_set.adjustable_parameter_bounds[name] for name in self.training_data_provider.parameter_set.adjustable_parameter_names]
        for parameter_name in self.parameters.adjustable_parameter_names:
            if abs(self.training_data_provider.parameter_set(parameter_name)-self.training_data_provider.parameter_set.adjustable_parameter_bounds[parameter_name][0]) < 1e-2 * abs(self.training_data_provider.parameter_set.adjustable_parameter_bounds[parameter_name][0]):
                contact.append('<')
            elif abs(self.training_data_provider.parameter_set(parameter_name)-self.training_data_provider.parameter_set.adjustable_parameter_bounds[parameter_name][1]) < 1e-2 * abs(self.training_data_provider.parameter_set.adjustable_parameter_bounds[parameter_name][1]):
                contact.append('>')
            else:
                contact.append('-')
        
        pretty_table = prettytable.PrettyTable(header = True)
        pretty_table.add_column('name',self.training_data_provider.parameter_set.adjustable_parameter_names)
        pretty_table.add_column('initial level', initial_levels)
        pretty_table.add_column('final level', best_parameter_levels)
        pretty_table.add_column('final values', self.training_data_provider.parameter_set.adjustable_values)
        pretty_table.add_column('bounds', adjustables_bounds_str)
        pretty_table.add_column('contact', contact)
        pretty_table.float_format['final values'] = ".4"
            
        print(pretty_table)
        print('Learning error from %f to %f ' % (initial_error, best_learning_error))
        
        training_data: buildingenergy.data.DataProvider = self.training_data_provider
        self.varying_state_model.set_data(validation_data_provider)
        best_validation_outputs: dict[str, list[float]] = self.varying_state_model.simulate()
        best_validation_error: float = self.error(best_validation_outputs)
        print('Validation error: %f ' % (best_validation_error))
        self.varying_state_model.set_data(training_data)
        return best_parameter_levels, best_validation_outputs, best_validation_error, best_learning_outputs, best_learning_error

    def save(self, file_name: str = 'results.csv', selected_variables: list[str] = None):
        """
        save the selected data in a csv file

        :param file_name: name of the csv file, defaults to 'results.csv' saved in the 'results' folder specified in the setup.ini file
        :type file_name: str, optional
        :param selected_variables: list of the variable names to be saved (None for all), defaults to None
        :type selected_variables: list[str], optional
        """
        self.data.save(file_name, selected_variables)
        
    def sensitivity(self, number_of_trajectories: int, number_of_levels: int=4) -> dict:
        """Perform a Morris sensitivity analysis for average simulation error both for indoor temperature and CO2 concentration. It returns 2 plots related to each output variable. mu_star axis deals with the simulation variation bias, and sigma for standard deviation of the simulation variations wrt to each parameter.

        :param number_of_trajectories: [description], defaults to 100
        :type number_of_trajectories: int, optional
        :param number_of_levels: [description], defaults to 4
        :type number_of_levels: int, optional
        :return: a dictionary with the output variables as key and another dictionary as values. It admits 'names', 'mu', 'mu_star', 'sigma', 'mu_star_conf' as keys and corresponding values as lists
        :rtype: dict[str,dict[str,list[float|str]]]
        """
        
        print('number of levels:', number_of_levels)
        print('number of trajectories:', number_of_trajectories)
        problem: dict[str,float] = dict()
        adjustable_parameters: list[str] = self.parameters.adjustable_parameter_names
        problem['num_vars'] = len(adjustable_parameters)
        problem['names'] = []
        problem['bounds'] = []
        for parameter_name in adjustable_parameters:
            problem['names'].append(parameter_name)
            problem['bounds'].append((0, self.parameters.adjustable_level_bounds[parameter_name][1]))
        
        parameter_value_sets = SALib.sample.morris.sample(problem, number_of_trajectories, num_levels=number_of_levels)
        
        errors = list()
        for i, parameter_value_set in enumerate(parameter_value_sets):
            parameter_value_set = [round(p) for p in parameter_value_set]
            self.training_data_provider.parameter_set.set_adjustable_levels(parameter_value_set)
            print('simulation %i/%i>' % (i+1, len(parameter_value_sets)), '\t', parameter_value_set)
            simulated_ouput_data: dict[str, list[float]]= self.run()
            output_error = self.error(simulated_ouput_data)
            errors.append(output_error)
        print()
        print('Analyzing simulation results')
        print('\n* estimation errors')
        results: dict = SALib.analyze.morris.analyze(problem, parameter_value_sets, numpy.array(errors, dtype=float), conf_level=0.95, print_to_console=True, num_levels=number_of_levels)
        fig = plotly.express.scatter(results, x='mu_star', y='sigma', text=adjustable_parameters, title='estimation errors')
        fig.show()
        return results
  
# if __name__ == '__main__':
#     from sites.model_h358 import *

#     print('Loading data')
#     parameter_set = 
#     data_provider = DataProvider(H358ParameterSet(), H358Data(starting_stringdate='15/02/2015', ending_stringdate='15/02/2016', number_of_levels=3, verbose=True), h358_model_data_bindings)
#     H358ParameterizedDataSet(data_provider)
#     training_data_provider = sites.data_h358.make_h358_data_provider('1/03/2015', '1/05/2015', number_of_levels=3)
#     validation_data_provider = sites.data_h358.make_h358_data_provider('1/03/2015', '1/05/2015', number_of_levels=3)  # '15/02/2015', '15/02/2016'

#     print('Model generation')
#     order = None
#     start: float = time.time()
#     model_maker = VaryingStateModel(training_data_provider, order, sample_time_in_seconds=3600)  # , parameters_levels=None
#     state_model = model_maker.state_model(k=0)
#     print('model generation duration: %f secondes' % (time.time() - start))

#     fitter: ModelFitter = ModelFitter(model_maker)
#     print('- Model fitting')
#     start: float = time.time()
#     best_parameters, best_validation_outputs, best_validation_error, best_learning_outputs, best_learning_error = fitter.fit(10, validation_data_provider=validation_data_provider)
#     print(best_parameters)
#     print('levels=(', ', '.join(['%i' % level for level in validation_data_provider.parameters.adjustable_parameter_levels]), ')')
#     for simulated_output_name in best_validation_outputs:
#         validation_data_provider.add_external_variable(simulated_output_name+'_simulation', best_validation_outputs[simulated_output_name])
#     validation_data_provider.plot()
#     # fitter.sensitivity(2)
#     print('%i secs' % (time.time() - start))
    