"""
This code has been written by stephane.ploix@grenoble-inp.fr
It is protected under GNU General Public License v3.0
"""
from __future__ import annotations

import buildingenergy.parameters
import buildingenergy.data
import buildingenergy.statemodel
import datetime
import abc
import numpy
import time

class DataBagGenerator:
    
    class DataBag:
    
        def __init__(self, model: buildingenergy.model.Model) :
            self.regular_input_names: list[str] = model.regular_input_names
            self.singular_input_names: list[str] = model.singular_input_names
            self.airflows_names: list[str] = model.airflow_names
            self.regular_output_names: list[str] = model.regular_output_names
        
            self.airflows_values: list[dict[str, float]] = model.airflows_values
            self.variable_in_fingerprint_values: list[dict[str, float]] = model.variables_in_finge

            self.data: buildingenergy.data.VariableSet = model.data
            self.parameters: buildingenergy.parameters.ParameterSet = model.parameters
            self.parameterized_data: buildingenergy.data.ParameterizedData = model.parameterized_data
            
            self.fingerprint_databag: dict[list[float], dict[str, list[float]]] = {self.parameters.levels: dict()}
        
    # def series(self):
    #     for variable in 
        


class Controller(abc.ABC):

    def ___init__(self, model: buildingenergy.model.Model, model_data_bindings: list[tuple[str, str]]) -> None:
        """Initialize a controller hook to adapt different kinds of controller like fast PID, rule-based control and predictive control

        :param datetimes: datetimes related to the timed data
        :type datetimes: list[datetime.datetime]
        """
        self.model = model
        self.model_data_bindings = model_data_bindings
        self._setpoints: dict[str, list[float]] = self.generate_timed_setpoints()
        
        self.data: dict[str, list[float]] = {variable: self.data(variable) for variable in dataset.variable_names}
        self.control_variables: dict[str, list[float]] = dict()
        self.controlled_variables: list[str] = list()
        
        self.controlled_variables_to_setpoints: dict[str,str] = dict()
        self.variable_limited_value_domains: dict[str,tuple[float, float]] = dict()
        #self.invariant_data: dict[str, list[float]] = {variable: data(variable) for variable in data}
        
        # for variable in setpoints:
        #     self.invariant_data[variable] = setpoints[variable]
        
    def setpoints(self) -> dict[str, list[float]]:
        return self._setpoints
        
    @abc.abstractclassmethod
    def generate_timed_setpoints(self) -> dict[str, list[float]]:
        """An abstract class to generate timed setpoints that will be available as context_data for the computation of the control port value domains and
        of control algorithm. These series of data can be equivalently provided to the ControlHook initializer or provided here. It's an abstract method 
        that must be implemented. 

        :param datetimes: list of datetimes of the timed data series
        :type datetimes: list[datetime.datetime]
        :param context_timed_data: contain the existing time data, collected by the initializer
        :type context_timed_data: dict[str,list[float]]
        :raises NotImplementedError: raised when sub-class has not been implemented
        :return: timed setpoints
        :rtype: dict[str, list[float]]
        """
        raise NotImplementedError
    
    # def create_control(self, variable_name: str, dependent_variable_name: str=None, dependent_variable_limited_value_domain: tuple[float, float]=None) -> None:
    #     """Create a control for a variable possibly depending on another variable with a possible limitation in the value domain

    #     :param variable_name: a control port name, i.e. an output of the controller, and an input of the system to be controlled
    #     :type variable_name: str
    #     :param dependent_variable_name: a dependent output port can be associated to the port when there is a local controller adjusting the value of the dependent port in order to set the control port, defaults to None i.e. no local controller
    #     :type dependent_variable_name: str, optional
    #     :param dependent_variable_limited_value_domain: if a dependent control port is specified, a interval representing the extreme possible values must be defined, defaults to None
    #     :type dependent_variable_limited_value_domain: tuple[float, float], optional
    #     :raises ValueError: _description_
    #     """
    #     self.controlled_variables.append(variable_name)
    #     if dependent_variable_name is not None:
    #         self.setpoints_to_controlled_variables[variable_name] = dependent_variable_name
    #         self.controlled_variables_to_setpoints[dependent_variable_name] = variable_name
    #         if dependent_variable_limited_value_domain is not None:
    #             self.variable_limited_value_domains[dependent_variable_name] = dependent_variable_limited_value_domain
            # else:
            #     raise ValueError('A value domain interval must be specified for the dependent port "%s" bounded to "%s"' % (dependent_variable_name, variable_name))
    
    #hour_control_variables = controller.compute_hour_control(k, state_model)
    def controls(self, k: int, **actual_data: dict[str, float]) -> dict[str, float]:
        """Return the control values for all the control port, including the dependent control ports, using the control_algorithm abstract method. The control values are adjusted to correspond to the related value domain. If it does not match, the control values are replaced by the closest discrete value of the control port value domain. The control is using either the timed context data or the actual values possibly provided as arguments

        :param k: current time step
        :type k: int
        :param actual_data: forced values of context data or of controls
        :type actual_data: dict[str, float]
        :return: the current control values for the control ports
        :rtype: dict[str, float]
        """
        context_data: dict[str, float] = {data_name: self.context_timed_data[data_name][k] for data_name in self.context_timed_data}
        for data_name in actual_data:
            context_data[data_name] = actual_data[data_name]
            
        value_domains: dict[str,list[float]] = self.control_value_domains(k, context_data)
        port_value_domains: dict[str,list[float]] = dict()
        
        controls: dict[str, float] = dict()  # check the size of the value domain and deduce control if possible
        
        for port_name in self.controlled_variables:  # check
            if port_name not in value_domains or len(value_domains[port_name]) == 0:
                controls[port_name] = 0  # if a port name is not appearing in the value domains, its value is set to 0
            elif len(value_domains[port_name]) == 1: # if a port name has only one value in the corresponding value domain, this value is set for the control port name
                controls[port_name] = value_domains[port_name][0]
            else:  # if several values are possible for the port name, the control algorithm will select one value for the control port name
                port_value_domains[port_name] = value_domains[port_name]
        for control_port_name in controls:  # add control values that could be resolved into the context data
            context_data[control_port_name] = controls[control_port_name]
       
        if len(port_value_domains) > 0: # if there are control port value domains with several possible values, call the control algorithm to solve it
            computed_controls: dict[str, float] = self.control_algorithm(k, port_value_domains, context_data, self.setpoints_to_controlled_variables)  ########### call of control algorithm
            for computed_control in computed_controls:  # merge control values obtained from the control algorithms with the pre-determined one
                if computed_control not in controls:
                    controls[computed_control] = computed_controls[computed_control]
      
        for control_name in self.controlled_variables:  # adjust control values to match value domains
            if control_name in value_domains and controls[control_name] not in value_domains[control_name]: # if control is not in value domain, take the closest permitted value from value domain
                control_name: float = controls[control_name]
                value_domain: list[float] = value_domains[control_name]
                val_dist: list[float] = [abs(value_domain[i] - control_name) for i in range(len(value_domain))]
                controls[control_name] = value_domain[val_dist.index(min(val_dist))]
            
            direct_controls: dict[str, float] = dict()
            indirect_controls: dict[str, float] = dict()
            for control_name in controls:
                if control_name in self.setpoints_to_controlled_variables:
                    indirect_controls[control_name] = controls[control_name]
                else:
                    direct_controls[control_name] = controls[control_name]
        return direct_controls, indirect_controls
    
    def adjust_dependent_controls(self, dependent_controls: dict[str, float]) -> dict[str, float]:
        """Return adjusted values for dependent continuous control variable values. Values are thresholded according to the predefined value domain interval
        and possibly set to None in case of missing dependent controls.

        :param dependent_controls: name of a dependent control associated to its current value
        :type dependent_controls: dict[str, float]
        :return: the adjusted dependent control
        :rtype: dict[str, float]
        """
        for dependent_control_name in self.controlled_variables_to_setpoints:
            if dependent_control_name in dependent_controls:  # cut the extreme values for the dependent control port
                dependent_controls[dependent_control_name] = min(max(dependent_controls[dependent_control_name], self.control_port_names_intervals[dependent_control_name][0]), self.control_port_names_intervals[dependent_control_name][1])
            else:
                dependent_controls[dependent_control_name] = None
        return dependent_controls
    
    @abc.abstractclassmethod
    def control_algorithm(self, k: int, value_domains: dict[str, list[float]], context_data: dict[str, float], dependent_control_port_names: dict[str, str]) -> dict[str, float]:
        """ Compute the controls that could not be solved at time step k. The possible values are given by the value domains. All the pre-defined values are available in context dat

        :param k: current time step
        :type k: int
        :param value_domains: all the possible values for each control to be computed
        :type value_domains: dict[str, list[float]]
        :param context_data: all the pre-defined values including set-points, context data and solved control values
        :type context_data: dict[str, float]
        :param dependent_control_port_names: dependency between control ports
        :type dependent_control_port_names: dict[str, str]
        :raises NotImplementedError: method must be instantiated
        :return: a value for each control port
        :rtype: dict[str, float]
        """
        raise NotImplementedError
    
    abc.abstractmethod
    def control_value_domains(self, k: int, context_data: dict[str, float]) -> dict[str,list[float]]:
        """Return the discrete value domains (list of named values) of the direct and indirect control ports at time step k (but not the dependent continuous value domains, which is an interval). The value domain specified for a port will always be satisfied by the port control value: if the control value does not match the value domain, the closest possible value will be selected. It's an abstract method that must be implemented. 

        :param k: current time step
        :type k: int
        :param context_data: available data to build the port value domains at time step k
        :type context_data: dict[str, float]
        :raises NotImplementedError: raised when sub-class has not been implemented
        :return: value domains i.e. the set of possible values for each direct or indirect control port.
        :rtype: dict[str,list[float]]
        """
        raise NotImplementedError
    
    @property
    def typed_controls(self) -> dict[str, str]:
        """list of the control ports organized accordingly to the control port type: 'direct', 'indirect' and 'dependent'

        :return: control port names organized by type 
        :rtype: dict[str, str]
        """
        _typed_control_ports: list[str] = {'direct':[], 'indirect':[], 'dependent': []}
        for port in self.controlled_variables:
            if port in self.setpoints_to_controlled_variables:
                _typed_control_ports['indirect'].append(port)
            else:
                _typed_control_ports['direct'].append(port)
        for port in self.controlled_variables_to_setpoints:
            _typed_control_ports['dependent'].append(port)
        return _typed_control_ports
        
    def __str__(self) -> str:
        """Return a description of the controller hook for printing its characteristics

        :return: a descriptive string
        :rtype: str
        """
        _str: str = 'Controller hook with:\n* direct controls:\n - '
        _str += '\n - '.join(self.controlled_variables)
        _str += '\n* setpoints:\n - '
        _str += '\n - '.join([v for v in self.setpoints()])
        # _str += '\n* contextual inputs:\n - '
        # _str += '\n - '.join([n for n in self.context_timed_data])
        return _str    

class ControlRunner:
    """Simulator dedicated to high level control like rule based or optimized anticipative control. It is using a model and defines control points i.e.
    a limited set of possible control values for each control or setpoint.
    """

    def __init__(self, model: buildingenergy.model.AbstractModel, controller: Controller=None, suffix: str='ctrl') -> None:
        """Initialize a simulation with control points of different natures

        :param model: model of the 
        :type model: buildingenergy.model.AbstractModel
        :param controlpoint_bindings: _description_
        :type controlpoint_bindings: dict[str, ControlType]
        :param control_type: _description_, defaults to SIMULATION
        :type control_type: int, optional
        """
        self.model: buildingenergy.model.AbstractModel = model
        self.parameters: buildingenergy.parameters.ParameterSet = self.model.parameters
        self.data: buildingenergy.data.VariableSet = self.model.data
        self.controller: Controller = controller
        self.suffix: str = suffix

        self.setpoints: dict[str, list[float]] = controller.setpoints
        self.regular_input_names: list[str] = model.regular_input_names
        self.airflows_names: list[str] = model.airflow_names
        self.regular_output_names: list[str] = model.regular_output_names
        self.variables_in_fingerprint_names: list[str] = model.variables_in_fingerprints_names
        self.regular_outputs_values: list[dict[str, float]] = model.regular_outputs_values
        self.regular_inputs_values: list[dict[str, float]] = model.regular_inputs_values
        self.airflows_values: list[dict[str, float]] = model.airflows_values
        self.variable_in_fingerprint_values: list[dict[str, float]] = model.variables_in_fingerprint_values
        #self._regular_outputs_values: dict[str,list[float]] = {regular_output_name: list() for regular_output_name in self.regular_output_names}
        
        setpoints: dict[str, list[float]] = self.controller.setpoints()
        for k in range(len(self.data)):
            for regular_input_name in self.regular_input_names:
                if regular_input_name in setpoints and setpoints[regular_input_name][k] is not None:
                    self.regular_inputs_values[k][regular_input_name] = setpoints[regular_input_name][k]
            for airflow_name in self.airflows_names:
                if airflow_name in setpoints and setpoints[airflow_name][k] is not None:
                    self.airflows_values[k][airflow_name] = setpoints[airflow_name][k]
            for variable_in_fingerprint_name in self.variables_in_fingerprint_names:
                if variable_in_fingerprint_name in setpoints and setpoints[variable_in_fingerprint_name][k] is not None:
                    self.variable_in_fingerprint_values[k][variable_in_fingerprint_name] = setpoints[variable_in_fingerprint_name][k]
                    
            
        
    def run(self, suffix:str='ctrl') -> dict[str, list[float]]:
        regular_outputs_values: dict[str,list[float]] = {regular_output_name + '_' + suffix: list() for regular_output_name in self.regular_output_names}
        regular_inputs_values: dict[str,list[float]] = {regular_input_name + '_' + suffix: list() for regular_input_name in self.regular_input_names}
        airflows_values: dict[str,list[float]] = {airflow_name + '_' + suffix: list() for airflow_name in self.airflows_names}
        variables_in_fingerprint_values: dict[str,list[float]] = {variables_in_fingerprint_name + '_' + suffix: list() for variables_in_fingerprint_name in self.variables_in_fingerprint_names}
        day_indices: list[float] = self.model.data('day_index')
        #self._output_values: dict[str, list[float]] = {name: list() for name in self.model.regular_output_names}
        
        current_day_index = day_indices[0] - 1
        state: numpy.matrix = None
        for k in range(len(self.model.data)):
            if day_indices[k] != current_day_index:
                current_day_index: float = day_indices[k]
                start_day_hour: int = k
            
            # controls = self.controller.controls(k)
            k_variable_in_fingerprint_values: dict[str, float] = self.variable_in_fingerprint_values[k]  
            k_airflows_values: dict[str, float] = self.airflows_values[k]
            # k_state_model: buildingenergy.statemodel = self.model.generate_state_model(k_variable_in_fingerprint_values, k_airflows_values)
            self.model.compute_on_data_change()
            k_state_model: buildingenergy.statemodel = self.model.generate_state_model(dict(), k_airflows_values)
            k_regular_input_values: dict[str, float] = self.regular_inputs_values[k]
            if state is None:
                state = k_state_model.initialize(**k_regular_input_values)
            else:
                k_state_model.set_state(state)
            output_values: numpy.matrix = k_state_model.output(**k_regular_input_values)
            for i, regular_output_name in enumerate(self.regular_output_names):
                regular_outputs_values[regular_output_name + '_' + suffix].append(output_values[i, 0])
            for regular_input_name in self.regular_input_names:
                regular_inputs_values[regular_input_name + '_' + suffix].append(k_regular_input_values[regular_input_name])
            for airflow_name in self.airflows_names:
                airflows_values[airflow_name + '_' + suffix].append(k_airflows_values[airflow_name])
            for variable_in_fingerprint_name in self.variables_in_fingerprint_names:
                variables_in_fingerprint_values[variable_in_fingerprint_name + '_' + suffix].append(k_variable_in_fingerprint_values[variable_in_fingerprint_name])
                
                
            # controls: dict[str, float] = dict()
            # for controlled_input_var in self.controller.setpoints_to_controlled_variables:
            #     output_setpoint_val: float|None = self.controller.paired_inout_control_port_values[controlled_input_var][k]
            #     if output_setpoint_val is not None:
            #         output_setpoint_var: str = self.controller.setpoints_to_controlled_variables[controlled_input_var]
            #         j, i =  self.controller.step_output_control_indices[controlled_input_var]
            #         CA = state_model.C * state_model.A
            #         CB = state_model.C * state_model.B
            #         if CB[i,j] == 0 or state_model.D.any():
            #             raise ValueError('A step-controller cannot be created to adjust the control "%s" in order to get the setpoint output value on "%s"' % (controlled_input_var, output_setpoint_var))
            #         state = state_model.state 
                    
            #         regular_input_values[controlled_input_var] = 0
            #         input_vec: numpy.matrix = numpy.matrix([[regular_input_values[input_name]] for input_name in self.regular_input_names])
            #         Uk_control = (output_setpoint_val - CA[i,:] * state - CB[i,:]  * input_vec) / CB[i,j]
            #         Uk_control = self.controller.control_options_bindings[controlled_input_var].cap(Uk_control[0,0])
            #         regular_input_values[controlled_input_var] = Uk_control
            #         controls[controlled_input_var] = Uk_control
            # output_values: numpy.matrix = state_model.output(**regular_input_values)
            # for i, name in enumerate(self.model.regular_output_names):
            #     self._output_values[name].append(output_values[i][0,0])
            # for control_input in controls:
            #     if control_input in self._output_values:
            #         self._output_values[control_input].append(controls[control_input])
            #     else:
            #         self._output_values[control_input] = [controls[control_input]]
            state = k_state_model.step(**k_regular_input_values)
        
        return regular_outputs_values, regular_inputs_values, airflows_values, variables_in_fingerprint_values


class SignalGenerator:
    """
    This class is a setpoint generator. It first generates a constant setpoint and proposes different 
    methods to intersect with the original signal. The value None means no setpoint i.e. control
    is off. 
    """

    def __init__(self, base_value: float, datetimes: list[datetime.datetime]):
        """
        Initialize the setpoint generator.

        :param reference_setpoint: the constant maximum setpoint value
        :type reference_setpoint: float
        :param datetimes: the list of dates (hours) corresponding to samples (that should be identical to
        to a Data object for integration)
        :type datetimes: list[datetime]
        """
        self.base_value: float = base_value
        self.datetimes: list[datetime.datetime] = datetimes
        self._values: list[float] = [base_value for _ in range(len(datetimes))]

    def __call__(self) -> list[float]:
        """
        Return the setpoint values

        :return: setpoint values
        :rtype: list[float]
        """
        return self._values

    def _minimum(self, values: list[float]) -> list[float]:
        """
        Internal method used to intersect (take the minimum of the current setpoints and the provided ones)
        another list of setpoints

        :param setpoints: setpoints that will be compared to the current setpoint values: the minimum is
        computed for each sample and replace the current setpoint value. None is the minimum of all the possible values
        :type setpoints: list[float]
        """
        for i in range(len(self.datetimes)):
            if values[i] is not None:
                self._values[i] = min(self._values[i], values[i])

    def heater_on(self, summer_period_start: tuple[int,int] = (15,3), summer_period_end: tuple[int,int] = (15,10)) -> None:
        """
        Generate setpoints corresponding to seasonal start and stop of the HVAC system for instance.

        :param summer_period_start: starting date for the summer period (signal starts to be off), defaults to '15/03'
        :type summer_period_start: str, optional
        :param summer_period_end: ending date for the summer period (end of the off values), defaults to '15/10'
        :type summer_period_end: str, optional
        """
        values = list()
        for datetime in self.datetimes:
            start_summer_day, end_summer_day = summer_period_start[0], summer_period_end[0]
            start_summer_month, end_summer_month = summer_period_start[1], summer_period_end[1]

            summer = None
            if start_summer_month < datetime.month < end_summer_month:
                summer: bool = True
            elif (start_summer_month == datetime.month and datetime.day >= start_summer_day) and (end_summer_month == datetime.month and datetime.day < end_summer_day):
                summer = True
            else:
                summer = False
            if summer:
                values.append(0)
            else:
                values.append(None)
        self._minimum(values)

    def daily(self, weekdays: list[int], low_value: float, hour_triggers: list[int], initial_on_state: bool = True):
        """
        Generate a setpoint corresponding to daily hours for selected days of the week.

        :param weekdays: list of the days of the week (0: Monday,... 6: Sunday) concerned by the setpoints
        :type weekdays: list[int]
        :param low_setpoint: low setpoint values used for computing the intersection with the current signal
        :type low_setpoint: float
        :param triggers: hours where the signal will switch from reference setpoint value to the low setpoint
        value and conversely, defaults to list[int]
        :type triggers: list[int], optional
        :param initial_on_state: initial setpoint value is low setpoint if False and reference setpoint value if True, defaults to False
        :type initial_on_state: bool, optional

        """
        current_state: bool = initial_on_state
        hour_triggers: list[int] = sorted(hour_triggers)
        on_triggers: dict[int, bool] = dict()
        for trigger in hour_triggers:
            on_triggers[trigger] = not current_state
            current_state = not current_state

        if type(weekdays) is int:
            weekdays: list[int] = [weekdays]
        setpoints: list[float] = list()
        profile: list[float] = list()
        on_state: bool = False
        for trigger_index in range(hour_triggers[0]):
            profile.append(self.base_value if not hour_triggers[0] else low_value)
        for trigger_index in on_triggers:
            while len(profile) < trigger_index:
                if on_state:
                    profile.append(self.base_value)
                else:
                    profile.append(low_value)
            on_state = on_triggers[trigger_index]
        for _ in range(trigger_index, 24):
            profile.append(self.base_value if on_triggers[trigger_index] else low_value)

        for datetime in self.datetimes:
            if datetime.weekday() in weekdays:
                setpoints.append(profile[datetime.hour])
            else:
                setpoints.append(None)
        self._minimum(setpoints)

    def long_absence(self, number_of_days: int, presence: list[float]):
        """
        Detect long absences for setting the setpoints to off.

        :param long_absence_setpoint: setpoint value in case of long absence detected
        :type long_absence_setpoint: float
        :param number_of_days: number of days over which a long absence is detected
        :type number_of_days: int
        :param presence: list of hours with presence (>0) and absence (=0)
        :type presence: list[float]
        """
        long_absence_start: int = None  # starting index for long absence
        long_absence_counter: int = 0
        values: list = list()
        for i in range(len(self.datetimes)):
            if presence[i] > 0:  # presence
                if long_absence_start is not None:  # long absence detected and ongoing
                    if long_absence_start + long_absence_counter > number_of_days * 24: # long absence detected but is over (presence detected)
                        for i in range(long_absence_start, long_absence_counter):  # add long absence.endswith() setpoints
                            values.append(None)  # long_absence_value
                    else:  # long absence has not been detected
                        for i in range(long_absence_start, long_absence_counter):
                            values.append(0)
                    values.append(0)
                long_absence_counter = 0  # reinitialize the long absence counter
            else: #absence
                if long_absence_start is None: # first absence detection
                    long_absence_counter = 1
                    long_absence_start = i
                else:  # new absence detection
                    long_absence_counter += 1
        for i in range(len(values), len(self.datetimes)):
            values.append(None)
        self._minimum(values)

    def capping(self, capping_values: float, threshold: float, thresholding_values: list[float]=None, opposite:bool=False):
        """
        Modify setpoint values on the occurrence of setpoint (window opening ratio for instance) values of an extra-signal passing a threshold.

        :param opening_setpoint: setpoint value to be used in case of the extra signal pass a threshold
        :type opening_setpoint: list[float]
        :param opening_threshold: threshold over which the opening threshold value will be applied.
        :type opening_threshold: float
        :param openings: extra-signal whose values will trigger the right setpoint
        :type openings: list[float]
        """
        values: list = list()
        if thresholding_values is None:
            thresholding_values: list[float] = capping_values
        for i in range(len(self.datetimes)):
            if not opposite:
                values.append(capping_values if thresholding_values[i] >= threshold else self.base_value)
            else:
                values.append(None if thresholding_values[i] >= threshold else capping_values)
        self._minimum(values)
