Download the buildingenergy project and unzip it, or clone the project with:
    
# Getting the code

git clone https://gricad-gitlab.univ-grenoble-alpes.fr/ploixs/buildingenergy.git 

# Getting the required libraries

Launch a terminal in "Visual Studio code", the "Powershell" (Windows) or the "Terminal" (MacOS, Linux) application on your Operating System

Go inside the buildingenergy folder:

cd <PATH_TO_buildingenergy>

pip install -r requirements.txt

# some libraries might to install properly using the `requirements.txt` file

# Getting data

- If you are at ENSE3, modify the `setup.ini` file at code root folder by commenting (with a ';' in front) as follows:

[folders]
;data = ./../data/
data = //FILE-V04.e3.local/Pedagogie/buildingenergy/data/
results = ./results/
linreg = linreg/
sliding = sliding/
figures = figures/ 
[sizes]
width = 8
height = 8
[databases]
irise = irise38.sqlite3

- Otherwise, keep it as it is:

[folders]
data = ./../data/
;data = //FILE-V04.e3.local/Pedagogie/buildingenergy/data/
results = ./results/
linreg = linreg/
sliding = sliding/
figures = figures/ 
[sizes]
width = 8
height = 8
[databases]
irise = irise38.sqlite3

but download the data from https://cloud.univ-grenoble-alpes.fr/s/zdqMR7bxWzmx5CE

Unzip and move the data at the same level than the root folder for code

- wherever you are, your file system should look like:

/your_containing_folder
    /data
    /buildingenergy
        /buildingenergy
        /doc
        /ecommunity
        /img
        /results
        /sites
        /slides
        notebook...
        ...
        requirements.txt
        setup.ini