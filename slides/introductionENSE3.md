---
marp: false
theme: default
paginate: true
---
![width:200px](../img1/ense3.png)
# Energy Management for Buildings
[stephane.ploix@grenoble-inp.fr](stephane.ploix@grenoble-inp.fr), 
[estefania.alvarez-del-castillo-cardoso@grenoble-inp.fr](estefania.alvarez-del-castillo-cardoso@grenoble-inp.fr)

---
## General content

- energy management in buildings (40h, Stéphane Ploix & Estefania Alvarez...)
  - modeling for building design and comparison with measurements
  - energy management strategies with flexibility
  - discovery of a fine simulation tool: Pleiades
- building control: IDA-ICE (8h, Benoit Delinchant)
- modeling HVAC systems (12h, Yann Bultel)

---
## Objective

Increase your experience about
- understanding the different between tools for pre-design and tools for the advanced design stage
-  the capabilities and limitations of __different kinds of models__ for energy management in buildings : data models, regressive models, simplified knowledge models and detailed knowledge models 
- the __different kinds of algorithms__ useful for the design and the maintenance of buildings: dynamic simulation, sensitivity analysis, finite element method, optimization,...
- estimation of potentialities at __different stages__: pre-design stage, decision aiding at design stage, model calibration at audit stage,...
  
__Real cases__ are going to be investigated to offer an overview of activities in building sector, with a special focus on still open issues.

---
## Pedagogy

Each session of the lecture is compulsory. It corresponds to a Jupyter notebook. All the problems you have to solve are in the notebook: each question is numbered for you to be able to use your favorite tool for writing your report. It's recommended not to reply directly in the notebook because regular updates will probably occur.
Scientific material is generally inside the notebook, but can also be on Chamilo. Keep all your replies: you will have to return all of them after the last session.

---
## Materials

1. notebook01_context: pre-design & bioclimatism, solar radiation, heating systems,... (8h)
2. notebook02_data: measurements, indicators (4h)
3. notebook03_static: static modeling of buildings, thermics and air quality (6h)
4. notebook04_dynamic: dynamic modeling: inertia, comparison with reality (6h)
5. notebook05_fitting: parameter estimation of knowledge models vs standard regressor (6h)
6. notebook06_management: energy management strategies based on heuristics vs optimization (4h)
7. notebook07_photovoltaic: model, shadow impact, best direction (4h)
8. notebook08_community: energy communities, flexibilities, reinforcement (4h)
9. notebook09_ski_resort: performance of a human-centred system: a ski-resort (4h)
10. notebook10_pleiades: detailed modeling (8/12h)

58h for 40h in classroom!

---
## Running a tutorial

You need:
- Microsoft Visual Studio with Python and Jupyter extensions 
- a Python version ≥ 3.9 (3.11 still better)
- the teaching material from __Chamilo__ or from [https://gricad-gitlab.univ-grenoble-alpes.fr/ploixs/buildingenergy](https://gricad-gitlab.univ-grenoble-alpes.fr/ploixs/buildingenergy)
- open the root folder
- open the relevant tutorial (ending with .ipynb) and start
- read the 'README.txt' file to finalize the installation: note that data and code are not at the same location.

__NB: All the materials related to this course is open-source and can be used freely.__

---
# Prerequisites

- basic knowledge about modeling thermal phenomena
- knowledge about state space modeling
- basic skills in the Python language
- __interest in energy management in buildings__

---
# to go further…

![width:400px](../img/book1.png)

---
# to go further…

![width:400px](../img/book2.png)

---
# to go further…

![width:400px](../img/book3.png)

---
# It's time to start the first tutorial

