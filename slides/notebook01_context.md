---
marp: true
theme: default
paginate: true
---
# notebook01: context 

I. Territories and standards [60min]

II. Bioclimatism [60min]

III. Solar gain and mask [60min]

IV. Heat/Cool Day Degrees [30min]

V. Heating systems [30min]

---
# 0. What energy systems are... (keep this is mind)

- Energy is used to provide services to people

- Apart from gain in efficiency, reducing energy consumption and adding flexibility have an impact on __qualities of services__.
  - efficiency (easy)
  - flexibility (tough)
  - sobriety (crazy)
  
- Energy systems use to be socio-technical
  - Human actors are part of the system.
- Is automation a global solution? How would look a more involving solution?
  - scientific presqu'île and GreEN-ER, regional nature parks (ZAN,...), ski resorts, energy communities, national energy policy

---
# I. Territories and standards: Usual perspective 

Territories are diversely composed of:
- renewable or non renewable energy sources
  - adjustable or not (PV, river-hydro, windmills,...)
- storage means (STEPs, EVs?,...)
- (flexible) consumers
  - direct
  - indirect
- grids and micro-grids with control mechanisms
- a governance/economical mechanisms with rules
---
![](figs/governance.jpg)

---
# I. Territories and standards: proposal of Decision Aiding System

1. Service → Quality of service → User satisfaction $\sigma_i \in [0,100\%]$
2. Model of the relationship between energy needs and resulting services
3. Identification of possible actions
4. Humans - Collaborative Decision Aiding (DA) tool
   1. own knowledge of the DA tool about the system$^1$
   2. own knowledge of the humans, about the system 
   3. synchronization (interactions) of human DA tool knowledge and human knowledge
   4. proposals of possibly interesting strategy
$^1$ system = physical part + people + DA system

---
## I. Territories and standards [60min]

Let's investigate about energy around us.

### I.1 Presqu'île Scientifique [30min]

### I.2 RE2020 (E+C-) at home

## I.3 GreEn-ER/PREDIS HVAC system [30min]
---
## II. Bioclimatism [60min] __AVOID RUNNING THE CODE AND KEEP THE EXISTING RESULTS, OTHERWISE IT'S GONNA RUN A LONG TIME!__

### II.1 Characteristics of the lambda house [15min]

### II.2 Analysis of the lambda house [45min]

### II.3> Propose a couple of design directions for the building that should be designed in Forges, knowing that, like for all the new buildings, accordingly to the 2020 environmental regulation (Réglementation Environnementale 2020), it has to be neutral, and possibly positive in yearly energy. 
---
# III. Solar gain and mask [75min]

### III.1 Understanding the solar model [30min]

### III.2 Understanding the heliodor [15min]

### III.3 Impact of solar gain [30min]

---
## IV. Heat/Cool Day Degrees [30min]

### IV.1 Fast estimation of the heating and cooling needs

---
## V. Heating system [15min]

### V.1 Depict the structure of the code below (you will need it later)

### V.2 Propose your analyses 


